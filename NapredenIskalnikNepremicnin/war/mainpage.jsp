<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.napreden.iskalnik.entity.*"%>    
<%@ page import="com.napreden.iskalnik.*"%>
<%@ page import="com.cron.jobs.*"%>

<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %> 

<%  String[] vrstaPonudbe = {"Prodam", "Oddam", "Podarim"};
	String[] letoIzgradnjeSeznam = {"do 1949", "od 1950 do 1959", "od 1960 do 1969", "od 1970 do 1979", "od 1980 do 1989", "od 1990 do 1999", "od 2000 do 2009", "2010 in več"};
	String[] letoIzgradnjeValue = {"0-1949", "1950-1959", "1960-1969", "1970-1979", "1980-1989", "1990-1999", "2000-2009", "2010-2019"};

	String[] oddaljenost = {"0.5", "1.0", "1.5", "2.0", "2.5", "3.0"};	
	
	
	
	
	
	
	
%>       
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Iskalnik nepremičnin</title>
	
	<link rel="stylesheet" type="text/css" href="/stylesheets/resultsPage.css"/>
	<link rel="stylesheet" type="text/css" href="/stylesheets/overlayWindow.css"/>
	<link rel="stylesheet" type="text/css" href="/stylesheets/googleMap.css"/>
	<link rel="stylesheet" type="text/css" href="/stylesheets/infobox.css"/>
	
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>	
	<!--  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=true"></script> -->    
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js"></script>
   	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&sensor=true"></script>
    <script type="text/javascript">
		var globalneSpremenljivke = {	
			directionsDisplay: new google.maps.DirectionsRenderer(),
			directionsService: new google.maps.DirectionsService(),
			map: "",
			imeUporabnika: "",
			latLngMesta: "",
			infowindow: "",
			infowindowData: "",
			nepremicnina: "",
			currentMarker: "",
			nepremicnine: [],
			markerjiNepremicnin: [],
			priljubljene: [],
			slikeNepremicnine: []
		};
	
	
		// prikaže zemljevid
		google.maps.event.addDomListener(window, 'load', initialize);
		
		
		function initialize() {
			globalneSpremenljivke.imeUporabnika = document.getElementById("userdata").innerHTML.split(" ")[0];
			
			// info okno za nepremičnine
	  		globalneSpremenljivke.infowindow = new google.maps.InfoWindow();	  			  		
	  		// info okno za podatke o bližnih objektih
	  		globalneSpremenljivke.infowindowData = new google.maps.InfoWindow();
	  		
			// izpiše število zadetkov
			document.getElementById("steviloZadetkovStevilka").innerHTML = "0".bold();
				
			// lastnosti zemljevida
			var mapOptions = {
				zoom: 8,
				center: new google.maps.LatLng(46.151241, 14.995463),
		        // tip zemljevida
		    	mapTypeId: google.maps.MapTypeId.ROADMAP
		    	
		    };					
			// kreira zemljevid			
		  	globalneSpremenljivke.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
			 // nastavi lastnosti za prkaz poti
	  		globalneSpremenljivke.directionsDisplay.setMap(globalneSpremenljivke.map);
	  		globalneSpremenljivke.directionsDisplay.setPanel(document.getElementById("directions-panel"));
	  		
	    	globalneSpremenljivke.nepremicnine = new Array();
	    	globalneSpremenljivke.markerjiNepremicnin = new Array();
	    	globalneSpremenljivke.priljubljene = new Array(); 
	    	
			// pridobi priljubljene, jih doda v dropdown in prikaže na zemljevidu
			jQuery.ajax({
		    	url: 'https://spreadsheets.google.com/feeds/list/1jdF6_HqXe7gDJUv4pypW0KL_K-NtTQtB7pReuBI9he8/2051305767/public/values?alt=json&sq=uporabnik="'+ globalneSpremenljivke.imeUporabnika + '"',
		    	dataType: 'json',
		    	success: function(json) {
		    		if (!jQuery.isEmptyObject(json.feed.entry)) {
		    			for (var i = 0; i < json.feed.entry.length; i++) {
			    			createFavouriteMarker(json.feed.entry[i].gsx$idnepremicnine.$t, json.feed.entry[i].gsx$latitude.$t, json.feed.entry[i].gsx$longitude.$t);	
			    		}
		    		}
		        },
			    async:false
			});
	  		
	  		
		}		
		
		// prikaže pot med naslovoma
		function calcRoute() {
		  	// začetni naslov
		  	var start = document.getElementById("start").value;
		  	var end = document.getElementById("end").value;
		  	// način prikaza poti (avto, peš,...)
		  	var selectedMode = document.getElementById("mode").value;
		  	var request = {
		    	origin: start,
		    	destination: end,
		    	travelMode: google.maps.TravelMode[selectedMode]
		  	};
		  	// prikaz poti
		  	globalneSpremenljivke.directionsService.route(request, function(response, status) {
		    	if (status == google.maps.DirectionsStatus.OK) {
		    		globalneSpremenljivke.directionsDisplay.setDirections(response);
		    	}
		  	});
		}
		
		// odstrani prikaz poti
		function clearRoute() {
			// z zemljevida izbriše prikazano pot
			globalneSpremenljivke.directionsDisplay.set("directions", null);
		  	// pobriše polje s pisnim opisom poti 
		  	document.getElementById("directions-panel").innerHTML = "";	
		  	// pobriše vrednosti v poljih začetnega in končnega naslova
		  	document.getElementById("start").value = "";
		  	document.getElementById("end").value = "";
		  	// da dropdown na začetno vrednost
		  	document.getElementById("mode").selectedIndex = 0;
		}
		
		// izdela marker za priljubljeno nepremičnino
		function createFavouriteMarker(idNepremicnine, latitude, longitude) {
			jQuery.ajax({
		    	url: 'https://spreadsheets.google.com/feeds/list/15BtuJFX8MAYQ0t7Gl-ILFF3tpE2pewhfA7mfx9Sfs9M/995105425/public/values?alt=json&sq=id="'+ idNepremicnine + '"',
		    	dataType: 'json',
		    	success: function(json) {
		    		var marker;
		    		if (!jQuery.isEmptyObject(json.feed.entry)) {
		    			var n = {
	    					id: json.feed.entry[0].gsx$id.$t,
							tip: json.feed.entry[0].gsx$tip.$t,
							podtip: json.feed.entry[0].gsx$podtip.$t,
							vrstaPonudbe: json.feed.entry[0].gsx$vrstaponudbe.$t,
							url: json.feed.entry[0].gsx$url.$t,
							naziv: json.feed.entry[0].gsx$naziv.$t,
							cena: json.feed.entry[0].gsx$cena.$t,
							regija: json.feed.entry[0].gsx$regija.$t,
							kraj: json.feed.entry[0].gsx$kraj.$t,
							predel: json.feed.entry[0].gsx$predel.$t,
							velikost: json.feed.entry[0].gsx$velikost.$t,
							letoIzgradnje: json.feed.entry[0].gsx$letoizgradnje.$t,
							naslov: json.feed.entry[0].gsx$naslov.$t,
							opis: json.feed.entry[0].gsx$opis.$t,
							pasovnaSirina: json.feed.entry[0].gsx$latitude.$t,
							pasovnaDolzina: json.feed.entry[0].gsx$longitude.$t,
							naslovnaSlika: json.feed.entry[0].gsx$naslovnaslika.$t,
							dodano: json.feed.entry[0].gsx$dodano.$t,
							zadnjicPregledano: json.feed.entry[0].gsx$zadnjicpregledano.$t	
		    			}
		    									
		    			marker = new google.maps.Marker({
							position: new google.maps.LatLng(json.feed.entry[0].gsx$latitude.$t, json.feed.entry[0].gsx$longitude.$t),
							map: globalneSpremenljivke.map,
							icon: 'map_icons/realestatePriljubljene.png',
							idNepremicnine: json.feed.entry[0].gsx$id.$t,
							id: "",
							stevilka: "",
							slika: json.feed.entry[0].gsx$naslovnaslika.$t
					  	}); 
						
						
						var cena = "/";
						if (json.feed.entry[0].gsx$cena.$t != "") {
							cena = json.feed.entry[0].gsx$cena.$t + " €";
						}
						var podtip = "/";
						if (json.feed.entry[0].gsx$podtip.$t != "") {
							podtip = json.feed.entry[0].gsx$podtip.$t; 
						}
						var contentNew = '<div id="infobox" style="width:360px;height:230px">' +
											'<div id="nazivNep_infobox">' + json.feed.entry[0].gsx$naziv.$t + '</div>' +
											'<div id="picture-panel"><img id="nepPicture" alt="" src=' + json.feed.entry[0].gsx$naslovnaslika.$t + ' width="130" height="120"/></div>' + 		
											'<div id="properties-detail">' +
												'<table>' +
													'<tr><td id="nepNaslov"><b>Naslov:</b> ' + json.feed.entry[0].gsx$naslov.$t + ' ' + json.feed.entry[0].gsx$kraj.$t + '</td></tr>' +
													'<tr><td id="nepCena"><b>Cena:</b> ' + cena + '</td></tr>' +
													'<tr><td id="nepTip"><b>Tip:</b> ' + json.feed.entry[0].gsx$tip.$t + '</td></tr>' +
													'<tr><td id="nepPodtip"><b>Podtip:</b> ' + podtip + '</td></tr>' +
													'<tr><td id="nepVelikost"><b>Velikost:</b> ' + json.feed.entry[0].gsx$velikost.$t + ' m<sup>2</sup></td></tr>' +
												'</table>' +								
											'</div>' +
											'<div id="infobox-datumi">' +
												'<b>Dodano v iskalnik</b>: ' + json.feed.entry[0].gsx$dodano.$t + ', <b>zadnji pregled</b>: ' + json.feed.entry[0].gsx$zadnjicpregledano.$t +
											'</div>' +
											'<div id="infobox-buttons">' +
											'<input type="button" value="Podrobnosti" onclick="overlay()"> <input type="button" id="removeFavourite" value="Odstrani" onclick="removeFromFavourites()"> ' + 
											'</div>' +
										 '</div>';
					  	
						// omogoči klik na marker
					  	google.maps.event.addListener(marker, 'click', function() { 	  			
					  		// prikaže okno
							globalneSpremenljivke.infowindow.open(globalneSpremenljivke.map, marker);
					  		// nastavi vsebino okna
							globalneSpremenljivke.infowindow.setContent(contentNew);
							// začetni naslov za iskanje poti vpiše v začetno polje
					  		document.getElementById('start').value = json.feed.entry[0].gsx$naslov.$t + " " + json.feed.entry[0].gsx$kraj.$t;
							// zapomni si nepremičnino za overlay window
					  		globalneSpremenljivke.nepremicnina = n;
					  		// id markerja
					  		globalneSpremenljivke.currentMarker = marker.id;
					  	});
					  
						
						// doda nepremičnino v dropdown Priljubljene
						var opt = document.createElement("option");
				        document.getElementById("realestateFavourites").options.add(opt);
				        opt.text = json.feed.entry[0].gsx$naziv.$t;
				        opt.value = json.feed.entry[0].gsx$naziv.$t;
						
						// shrani marker v seznam priljubljenih			
						var priljubljen = {
							id: json.feed.entry[0].gsx$id.$t,	
							naziv: json.feed.entry[0].gsx$naziv.$t,
							markerNepremicnine: marker,
							idMarkerja: "" 
						}
						globalneSpremenljivke.priljubljene.push(priljubljen);
						
					 	// da dropdown priljubljene na default vrednost
				  		google.maps.event.addListener(globalneSpremenljivke.infowindow, 'closeclick', function() {
				  			document.getElementById("realestateFavourites").value = 0;
						});
		    		}
		    		else {
		    			// 
		    			marker = new google.maps.Marker({
							position: new google.maps.LatLng(latitude, longitude),
							map: globalneSpremenljivke.map,
							icon: 'map_icons/realestatePriljubljene1.png',
					  	});
		    			
		    			// omogoči klik na marker
					  	google.maps.event.addListener(marker, 'click', function() { 	  			
					  		var odziv = confirm("Nepremičnina ne obstaja več. Ali jo želite izbrisati iz priljubljenih?");
					  		if (odziv) {
					  			// izbriše marker iz zemljevida
					  			marker.setMap(null);
					  			var value = "action=odstraniPriljubljene&imeUporabnika=" + globalneSpremenljivke.imeUporabnika + "&idPriljubljene=" + idNepremicnine;
								// odstrani nepremicnine iz tabele priljubljene
								jQuery.ajax({
								     url: "napredeniskalniknepremicnin",
								     //type: "post",
								     data: value,
								     cache: false,
								     success: function(data) {
								     
								     }
							    });	
					  			
					  		}
					  	});
		    		}
		        },
			    async:false
			});
		}
		
		var podtipHisa = [{value: "Atrijska", text: "Atrijska"},
                          {value: "Dvojček", text: "Dvojček"},
                          {value: "Dvostanovanjska", text: "Dvostanovanjska"},
                          {value: "Montažna", text: "Montažna"},
                          {value: "Samostojna", text: "Samostojna"},
                          {value: "Trojček", text: "Trojček"},
                          {value: "Vrstna", text: "Vrstna"},
                          {value: "Ostalo", text: "Ostalo"}
                          ];
		var podtipPocitniskiObjekt = [{value: "Apartma", text: "Apartma"},
		                              {value: "Hiša", text: "Hiša"},
		                              {value: "Koča", text: "Koča"},
		                              {value: "Počitniška prikolica", text: "Počitniška prikolica"},
		                              {value: "Vikend", text: "Vikend"},
		                              {value: "Ostalo", text: "Ostalo"}
		                              ];
		var podtipPosest = [{value: "Kmetija", text: "Kmetija"},
	                        {value: "Kmetijsko zemljišče", text: "Kmetijsko zemljišče"},
	                        {value: "Nezazidljivo", text: "Nezazidljivo"},
	                        {value: "Zazidljivo", text: "Zazidljivo"},
	                        {value: "Za investicijo", text: "Za investicijo"},
	                        {value: "Ostalo", text: "Ostalo"}
	                        ];
		var podtipPoslovniProstor = [{value: "Delavnica", text: "Delavnica"},
		                             {value: "Gostinski lokal", text: "Gostinski lokal"},
		                             {value: "Kombiniran prostor", text: "Kombiniran prostor"},
		                             {value: "Ordinacija", text: "Ordinacija"},
		                             {value: "Pisarna", text: "Pisarna"},
		                             {value: "Poslovni objekt", text: "Poslovni objekt"},
		                             {value: "Prostor za storitve", text: "Prostor za storitve"},
		                             {value: "Skladišče", text: "Skladišče"},
		                             {value: "Trgovina", text: "Trgovina"},
		                             {value: "Ostalo", text: "Ostalo"}
		                             ];
		var podtipStanovanje = [{value: "1-sobno", text: "1-sobno"},
		                        {value: "1.5-sobno", text: "1.5-sobno"},
		                        {value: "2-sobno", text: "2-sobno"},
		                        {value: "2.5-sobno", text: "2.5-sobno"},
		                        {value: "3-sobno", text: "3-sobno"},
		                        {value: "3.5-sobno", text: "3.5-sobno"},
		                        {value: "4-sobno", text: "4-sobno"},
		                        {value: "4.5-sobno", text: "4.5-sobno"},
		                        {value: "5 in večsobno", text: "5 in večsobno"},
		                        {value: "Apartma", text: "Apartma"},
		                        {value: "Garsonjera", text: "Garsonjera"},
		                        {value: "Soba", text: "Soba"},
		                        {value: "Ostalo", text: "Ostalo"}
		                        ];
		var velikostGarazeSeznam = [{value: "0-5", text: "do 5"},
		                            {value: "6-10", text: "od 6 do 10"},
		                            {value: "11-15", text: "od 11 do 15"},
		                            {value: "16-20", text: "od 16 do 20"},
		                            {value: "21-25", text: "od 21 do 25"},
		                            {value: "26-30", text: "od 26 do 30"},
		                            {value: "31-35", text: "od 31 do 35"},
		                            {value: "36-40", text: "od 36 do 40"},
		                            {value: "40-10000", text: "41 in več"},
		                            ];
		var velikostPosestSeznam = [{value: "0-500", text: "do 500"},
		                            {value: "501-1000", text: "od 501 do 1000"},
		                            {value: "1001-2000", text: "od 1001 do 2000"},
		                            {value: "2001-3000", text: "od 2001 do 3000"},
		                            {value: "3001-4000", text: "od 3001 do 4000"},
		                            {value: "4001-6000", text: "od 4001 do 6000"},
		                            {value: "6001-8000", text: "od 6001 do 8000"},
		                            {value: "8001-10000", text: "od 8001 do 10000"},
		                            {value: "10001-100000000", text: "10001 in več"},
		                            ];
		var velikostOstaloSeznam = [{value: "0-50", text: "do 50"},
		                            {value: "51-100", text: "od 51 do 100"},
		                            {value: "101-150", text: "od 101 do 150"},
		                            {value: "151-200", text: "od 151 do 200"},
		                            {value: "201-250", text: "od 201 do 250"},
		                            {value: "251-300", text: "od 251 do 300"},
		                            {value: "301-350", text: "od 301 do 350"},
		                            {value: "351-400", text: "od 351 do 400"},
		                            {value: "401-10000000", text: "401 in več"},
		                            ];
		
		var letoIzgradnjeSeznam = [{value: "0-1949", text: "do 1949"},
		                            {value: "1950-1959", text: "od 1950 do 1959"},
		                            {value: "1960-1969", text: "od 1960 do 1969"},
		                            {value: "1970-1979", text: "od 1970 do 1979"},
		                            {value: "1980-1989", text: "od 1980 do 1989"},
		                            {value: "1990-1999", text: "od 1990 do 1999"},
		                            {value: "2000-2009", text: "od 2000 do 2009"},
		                            {value: "2010-2019", text: "2010 in več"},
		                            ];
		
		// glede na izbran tip nepremičnine doda opcije ostalih parametrov
		function dodajOpcijeParametrov() {
			var tip = document.getElementById('idTip').value;
			if (tip == "Stanovanja") {
				// omogoči dropdown podtip
				document.getElementById("idPodtip").disabled = false;
				// doda opcije v dropdown podtip
				dodajOptions("idPodtip", podtipStanovanje);
				// omogoči dropdown velikost
				document.getElementById("idVelikost").disabled = false;
				// doda opcije v dropdown velikost
				dodajOptions("idVelikost", velikostOstaloSeznam);
				// omogoči dropdown leto izgradnje
				document.getElementById("idLetoIzgradnje").disabled = false;
			}
			else if (tip == "Hiše") {
				// omogoči dropdown podtip
				document.getElementById("idPodtip").disabled = false;
				// doda opcije v dropdown podtip
				dodajOptions("idPodtip", podtipHisa);
				// omogoči dropdown velikost
				document.getElementById("idVelikost").disabled = false;
				// doda opcije v dropdown velikost
				dodajOptions("idVelikost", velikostOstaloSeznam);
				// omogoči dropdown leto izgradnje
				document.getElementById("idLetoIzgradnje").disabled = false;				
			}
			else if (tip == "Počitniški objekt") {
				// omogoči dropdown podtip
				document.getElementById("idPodtip").disabled = false;
				// doda opcije v dropdown podtip
				dodajOptions("idPodtip", podtipPocitniskiObjekt);
				// omogoči dropdown velikost
				document.getElementById("idVelikost").disabled = false;
				// doda opcije v dropdown velikost
				dodajOptions("idVelikost", velikostOstaloSeznam);
				// omogoči dropdown leto izgradnje
				document.getElementById("idLetoIzgradnje").disabled = false;
			}
			else if (tip == "Posesti") {
				// omogoči dropdown podtip
				document.getElementById("idPodtip").disabled = false;
				// doda opcije v dropdown podtip
				dodajOptions("idPodtip", podtipPosest);
				// omogoči dropdown velikost
				document.getElementById("idVelikost").disabled = false;
				// doda opcije v dropdown velikost
				dodajOptions("idVelikost", velikostPosestSeznam);
				// onemogoči dropdown leto izgradnje
				document.getElementById("idLetoIzgradnje").disabled = true;
				// da dropdown leto izgradnje na default opcijo
				document.getElementById("idLetoIzgradnje").value = "0";
			}
			else if (tip == "Garaže") {
				// onemogoči dropdown podtip
				document.getElementById("idPodtip").disabled = true;
				// da dropdown podtip na default opcijo
				document.getElementById("idPodtip").value = "0";
				// omogoči dropdown velikost
				document.getElementById("idVelikost").disabled = false;
				// doda opcije v dropdown velikost
				dodajOptions("idVelikost", velikostGarazeSeznam);
				// omogoči dropdown leto izgradnje
				document.getElementById("idLetoIzgradnje").disabled = false;
			}
			else if (tip == "Poslovni prostori") {
				// omogoči dropdown podtip
				document.getElementById("idPodtip").disabled = false;
				// doda opcije v dropdown podtip
				dodajOptions("idPodtip", podtipPoslovniProstor);
				// omogoči dropdown velikost
				document.getElementById("idVelikost").disabled = false;
				// doda opcije v dropdown velikost
				dodajOptions("idVelikost", velikostOstaloSeznam);
				// omogoči dropdown leto izgradnje
				document.getElementById("idLetoIzgradnje").disabled = false;
			}
			else {
				// onemogoči dropdown podtip
				document.getElementById("idPodtip").disabled = true;
				// da dropdown podtip na default opcijo
				document.getElementById("idPodtip").value = "0";
				// onemogoči dropdown velikost
				document.getElementById("idVelikost").disabled = true;
				// da dropdown velikost na default opcijo
				document.getElementById("idVelikost").value = "0";
			}
		}
		
		// doda opcije v seznam
		function dodajOptions(idDropdown, seznamOpcij) {
			// pobriše vse trenutne opcije
			document.getElementById(idDropdown).innerHTML = "";				
			// doda osnovno opcijo
			var opt = document.createElement("option");
	        document.getElementById(idDropdown).options.add(opt);			        
	        opt.text = "-izberi-";
	        opt.value = "0";
			// doda opcije v seznam
			for (var i = 0; i < seznamOpcij.length; i++) {
				var opt = document.createElement("option");
		        document.getElementById(idDropdown).options.add(opt);			        
		        opt.text = seznamOpcij[i].text;
		        opt.value = seznamOpcij[i].value;
			}
		}
		
		
		var nepremicnineBrezNaslovaMarkerStil = [{
			textColor: '#FFFFFF',
			textSize: 15,
			url: 'map_icons/realestate1.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 15,	
       		url: 'map_icons/realestate1.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/realestate1.png',
       	    width: 35,
       	    height: 32,
       	}]
		var markersNepremicnineBrezNaslova = [];
		var markerNepremicnineBrezNaslovaCluster = null;
		
		var nepremicnineZNaslovomMarkerStil = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/realestate.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 14,	
       		url: 'map_icons/realestate.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/realestate.png',
       	    width: 35,
       	    height: 32,
       	}]
		var markerNepremicnineZNaslovomCluster = null;
		
		// filtrira in prikaže nepremičnine na zemljevidu
		function filtrirajNepremicnine() {
			// zbriše trenutno grupiranje markerjev
			if (globalneSpremenljivke.markerjiNepremicnin.length > 0) {
				markerNepremicnineZNaslovomCluster.clearMarkers();
			}
			if (markersNepremicnineBrezNaslova.length > 0) {
				markerNepremicnineBrezNaslovaCluster.clearMarkers();
			} 
			// izbriše markerje iz zemljevida
			for (var i = 0; i < globalneSpremenljivke.markerjiNepremicnin.length; i++) {
				globalneSpremenljivke.markerjiNepremicnin[i].setMap(null);
			}
			for (var i = 0; i < markersNepremicnineBrezNaslova.length; i++) {
				markersNepremicnineBrezNaslova[i].setMap(null);
			}
			// izprazne sezname markerjev
			globalneSpremenljivke.nepremicnine = [];
			globalneSpremenljivke.markerjiNepremicnin = [];
			markersNepremicnineBrezNaslova = [];
			
			// zapre okno 
			globalneSpremenljivke.infowindow.close();
			// prikaže priljubljene na zemljevidu
			for (var i = 0; i < globalneSpremenljivke.priljubljene.length; i++) {
				globalneSpremenljivke.priljubljene[i].markerNepremicnine.setMap(globalneSpremenljivke.map);
			}
			
			
			var izbranoMesto = document.getElementById("idMesto").value;
			var izbranTip = document.getElementById("idTip").value;
			// preveri ustreznost obveznih parametrov
			if (izbranoMesto == "0" || izbranTip == "0") {
				document.getElementById('steviloZadetkovStevilka').innerHTML = "0".bold();
				alert("Parametra Mesto in Tip nepremičnine sta obvezna!");
				return;
			}

			// preveri ustreznost parametrov cena od do
			var cenaOd = document.getElementById("idCenaOd").value;
			var cenaOdStevilka = /^\d+$/.test(cenaOd);
			var cenaDo = document.getElementById("idCenaDo").value;
			var cenaDoStevilka = /^\d+$/.test(cenaDo);
			if (cenaOd != "" || cenaDo != "") {
				if ((cenaOd != "" && cenaDo == "") || (cenaOd == "" && cenaDo != "")) {
					document.getElementById('steviloZadetkovStevilka').innerHTML = "0".bold();
					alert("Izpolnjeni morata biti obe vrednosti za ceno!");
				  	return;
				}
				else if (!cenaOdStevilka) {
					alert("Cena od mora biti celo število!");
					document.getElementById('steviloZadetkovStevilka').innerHTML = "0".bold();
				  	return;
				}
				else if (!cenaDoStevilka) {
					document.getElementById('steviloZadetkovStevilka').innerHTML = "0".bold();
					alert("Cena do mora biti celo število!");
				  	return;
				}
				else if (parseInt(cenaOd) > parseInt(cenaDo)) {
					document.getElementById('steviloZadetkovStevilka').innerHTML = "0".bold();
				  	alert("Cena do mora biti večja kot cena od!");
				  	return;
				}
			}
			
			// pasovna širina in dolžina izbranega kraja
		    jQuery.ajax({
		    	url: 'https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/2130016799/public/values?alt=json&sq=nazivobcine="'+ izbranoMesto + '"',
		    	dataType: 'json',
		    	success: function(json) {
		    		centerMesta(json.feed.entry[0].gsx$latitude.$t, json.feed.entry[0].gsx$longitude.$t);
		        },
			    async:false
			});
			
			// doda kraj in tip nepremičnine v poizvedbo
			var poizvedba = 'status=aktiven%20and%20kraj="'+ izbranoMesto + '"%20and%20tip="' + izbranTip + '"'; 
			// doda podtip nepremičnine v poizvedbo
			var izbranPodtip = document.getElementById("idPodtip").value;
			if (izbranPodtip != "0") {
				poizvedba = poizvedba + '%20and%20podtip="' + izbranPodtip + '"';
			}
			// doda vrsto ponudbe v  poizvedbo
			var izbranaVrstaPonudbe = document.getElementById("idPonudba").value;
			if (izbranaVrstaPonudbe != "0") {
				poizvedba = poizvedba + '%20and%20vrstaponudbe="' + izbranaVrstaPonudbe + '"';
			}
			// doda leto izgradnje v poizvedbo
			var izbranoLetoIzgradnje = document.getElementById("idLetoIzgradnje").value;
			if (izbranoLetoIzgradnje != "0") {
				var letoOdDo = izbranoLetoIzgradnje.split("-");
				poizvedba = poizvedba + '%20and%20letoizgradnje>=' + letoOdDo[0] + '%20and%20letoizgradnje<=' + letoOdDo[1];
			}
			
			// pridobi vrtce v kraju
			var izbranaOddaljenostVrtec = document.getElementById("idVrtec").value;
			var vpisanVrtec = document.getElementById("vrtciText").value;
			var vrtciKraja = new Array();
			if (izbranaOddaljenostVrtec != "null") {
				jQuery.ajax({
  			    	url: 'https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/707867289/public/values?alt=json&sq=obcina="' + izbranoMesto + '"',
  			    	dataType: 'json',
  			    	success: function(json) {
  			    		if (!jQuery.isEmptyObject(json.feed.entry)) {
	  			    		for (var i = 0; i < json.feed.entry.length; i++) {
	  			    			if (vpisanVrtec != "") {
	  			    				var regex = new RegExp(vpisanVrtec, "i");
	  			    				if (regex.test(json.feed.entry[i].gsx$nazivvrtca.$t)) {
	  			    					var vrtec = {
  	  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  	  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  	  		  			    			};
  	  		  			    			vrtciKraja.push(vrtec);
	  			    				}
	  			    			}
	  			    			else {
	  			    				var vrtec = {
  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  		  			    			};
  		  			    			vrtciKraja.push(vrtec);
	  			    			}	
	  			    		}
  			    		}
  			        },
  				    async:false
  				});
			}
						
			// pridobi osnovne šole v kraju
			var izbranaOddaljenostOsnovnaSola = document.getElementById("idOsnovnaSola").value;
			var vpisanOsnovnaSola = document.getElementById("osnovnaSolaText").value;
			var osnovneSoleKraja = new Array();
			if (izbranaOddaljenostOsnovnaSola != "null") {
				jQuery.ajax({
  			    	url: 'https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/277039759/public/values?alt=json&sq=obcina="' + izbranoMesto + '"',
  			    	dataType: 'json',
  			    	success: function(json) {
  			    		if (!jQuery.isEmptyObject(json.feed.entry)) {
	  			    		for (var i = 0; i < json.feed.entry.length; i++) {
	  			    			if (vpisanOsnovnaSola != "") {
	  			    				var regex = new RegExp(vpisanOsnovnaSola, "i");
	  			    				if (regex.test(json.feed.entry[i].gsx$nazivsole.$t)) {
	  			    					var osnovnaSola = {
  	  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  	  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  	  		  			    			};
	  			    					osnovneSoleKraja.push(osnovnaSola);
	  			    				}
	  			    			}
	  			    			else {
	  			    				var osnovnaSola = {
  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  		  			    			};
	  			    				osnovneSoleKraja.push(osnovnaSola);
	  			    			}	
	  			    		}
  			    		}
  			        },
  				    async:false
  				});
			}
			
			// pridobi srednje šole v kraju
			var izbranaOddaljenostSrednjaSola = document.getElementById("idSrednjaSola").value;
			var vpisanSrednjaSola = document.getElementById("srednjaSolaText").value;
			var srednjeSoleKraja = new Array();
			if (izbranaOddaljenostSrednjaSola != "null") {
				jQuery.ajax({
  			    	url: 'https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/2057546630/public/values?alt=json&sq=posta="' + izbranoMesto + '"',
  			    	dataType: 'json',
  			    	success: function(json) {
  			    		if (!jQuery.isEmptyObject(json.feed.entry)) {
	  			    		for (var i = 0; i < json.feed.entry.length; i++) {
	  			    			if (vpisanSrednjaSola != "") {
	  			    				var regex = new RegExp(vpisanSrednjaSola, "i");
	  			    				if (regex.test(json.feed.entry[i].gsx$nazivsole.$t)) {
	  			    					var srednjaSola = {
  	  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  	  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  	  		  			    			};
	  			    					srednjeSoleKraja.push(srednjaSola);
	  			    				}
	  			    			}
	  			    			else {
	  			    				var srednjaSola = {
  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  		  			    			};
	  			    				srednjeSoleKraja.push(srednjaSola);
	  			    			}	
	  			    		}
  			    		}
  			        },
  				    async:false
  				});
			}
			
			// pridobi visokošolske zavode v kraju
			var izbranaOddaljenostVisokosolskiZavod = document.getElementById("idVisokosolskiZavod").value;
			var vpisanVisokosolskiZavod = document.getElementById("visokosolskiZavodText").value;
			var visokosolskiZavodiKraja = new Array();
			if (izbranaOddaljenostVisokosolskiZavod != "null") {
				jQuery.ajax({
  			    	url: 'https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/791582045/public/values?alt=json&sq=obcina="' + izbranoMesto + '"',
  			    	dataType: 'json',
  			    	success: function(json) {
  			    		if (!jQuery.isEmptyObject(json.feed.entry)) {
	  			    		for (var i = 0; i < json.feed.entry.length; i++) {
	  			    			if (vpisanVisokosolskiZavod != "") {
	  			    				var regex = new RegExp(vpisanVisokosolskiZavod, "i");
	  			    				if (regex.test(json.feed.entry[i].gsx$nazivsole.$t)) {
	  			    					var visokosolskiZavod = {
  	  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  	  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  	  		  			    			};
	  			    					visokosolskiZavodiKraja.push(visokosolskiZavod);
	  			    				}
	  			    			}
	  			    			else {
	  			    				var visokosolskiZavod = {
  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  		  			    			};
	  			    				visokosolskiZavodiKraja.push(visokosolskiZavod);
	  			    			}	
	  			    		}
  			    		}
  			        },
  				    async:false
  				});
			}
			
			// pridobi zdravstvene ustanove v kraju
			var izbranaOddaljenostZdravstvo = document.getElementById("idZdravstvo").value;
			var vpisanZdravstvenaUstanova = document.getElementById("zdravstvoText").value;
			var zdravstvoKraja = new Array();
			if (izbranaOddaljenostZdravstvo != "null") {
				jQuery.ajax({
  			    	url: 'https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/1289548467/public/values?alt=json&sq=posta="' + izbranoMesto + '"',
  			    	dataType: 'json',
  			    	success: function(json) {
  			    		if (!jQuery.isEmptyObject(json.feed.entry)) {
	  			    		for (var i = 0; i < json.feed.entry.length; i++) {
	  			    			if (vpisanZdravstvenaUstanova != "") {
	  			    				var regex = new RegExp(vpisanZdravstvenaUstanova, "i");
	  			    				if (regex.test(json.feed.entry[i].gsx$naziv.$t)) {
	  			    					var zdravstvo = {
  	  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  	  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  	  		  			    			};
	  			    					zdravstvoKraja.push(zdravstvo);
	  			    				}
	  			    			}
	  			    			else {
	  			    				var zdravstvo = {
  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  		  			    			};
	  			    				zdravstvoKraja.push(zdravstvo);
	  			    			}	
	  			    		}
  			    		}
  			        },
  				    async:false
  				});
			}
			
			// pridobi lekarne v kraju
			var izbranaOddaljenostLekarne = document.getElementById("idLekarna").value;
			var vpisanLekarna = document.getElementById("lekarnaText").value;
			var lekarneKraja = new Array();
			if (izbranaOddaljenostLekarne != "null") {
				jQuery.ajax({
  			    	url: 'https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/1550820942/public/values?alt=json&sq=posta="' + izbranoMesto + '"',
  			    	dataType: 'json',
  			    	success: function(json) {
  			    		if (!jQuery.isEmptyObject(json.feed.entry)) {
	  			    		for (var i = 0; i < json.feed.entry.length; i++) {
	  			    			if (vpisanLekarna != "") {
	  			    				var regex = new RegExp(vpisanLekarna, "i");
	  			    				if (regex.test(json.feed.entry[i].gsx$nazivlekarne.$t)) {
	  			    					var lekarna = {
  	  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  	  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  	  		  			    			};
	  			    					lekarneKraja.push(lekarna);
	  			    				}
	  			    			}
	  			    			else {
	  			    				var lekarna = {
  		  			    			    latitude: json.feed.entry[i].gsx$latitude.$t,
  		  			    			    longitude: json.feed.entry[i].gsx$longitude.$t
  		  			    			};
	  			    				lekarneKraja.push(lekarna);
	  			    			}	
	  			    		}
  			    		}
  			        },
  				    async:false
  				});
			}
			
			// pridobi avtobusne in železniške postaje v kraju
			var izbranaOddaljenostJavniPromet = document.getElementById("idJavniPromet").value;
			var javniPrometKraja = new Array();
			if (izbranaOddaljenostJavniPromet != "null") {
				// poizvedba
			  	var request = {
					location: new google.maps.LatLng(globalneSpremenljivke.latLngMesta.sirina, globalneSpremenljivke.latLngMesta.dolzina),
					radius: 10000,
					types: ['bus_station']
				};				
			  	service = new google.maps.places.PlacesService(globalneSpremenljivke.map);
			  	service.nearbySearch(request, function(results, status){
					if (status == google.maps.places.PlacesServiceStatus.OK) {
				    	for (var i = 0; i < results.length; i++) {
				    		var avtobusnaPostaja = {
		    					latitude: results[i].geometry.location.lat(),
  			    			    longitude: results[i].geometry.location.lng()
  			    			};
				    		javniPrometKraja.push(avtobusnaPostaja);
				    	}	
				  	}
			  	});	
			  	
				// poizvedba
			  	var request = {
					location: new google.maps.LatLng(globalneSpremenljivke.latLngMesta.sirina, globalneSpremenljivke.latLngMesta.dolzina),
					radius: 10000,
					types: ['train_station']
				};				
			  	service = new google.maps.places.PlacesService(globalneSpremenljivke.map);
			  	service.nearbySearch(request, function(results, status){
					if (status == google.maps.places.PlacesServiceStatus.OK) {
				    	for (var i = 0; i < results.length; i++) {
				    		var zelezniskaPostaja = {
		    					latitude: results[i].geometry.location.lat(),
  			    			    longitude: results[i].geometry.location.lng()
  			    			};
				    		javniPrometKraja.push(zelezniskaPostaja);
				    	}	
				  	}
			  	});
			}
			
			// pridobi gasilske postaje v kraju
			var izbranaOddaljenostGasilci = document.getElementById("idGasilci").value;
			var gasilciKraja = new Array();
			if (izbranaOddaljenostGasilci != "null") {
				// poizvedba
				var request = {
			    	location: new google.maps.LatLng(globalneSpremenljivke.latLngMesta.sirina, globalneSpremenljivke.latLngMesta.dolzina), 
			    	radius: '5000',
			    	query: 'gasilsko drustvo'
			  	};
			  	service = new google.maps.places.PlacesService(globalneSpremenljivke.map);
			  	service.textSearch(request, function(results, status){
					if (status == google.maps.places.PlacesServiceStatus.OK) {
				    	for (var i = 0; i < results.length; i++) {
				    		var gasilskaPostaja = {
		    					latitude: results[i].geometry.location.lat(),
  			    			    longitude: results[i].geometry.location.lng()
  			    			};
				    		gasilciKraja.push(gasilskaPostaja);
				    	}	
				  	}
			  	});	
			}
			
			// pridobi poštne poslovalnice v kraju
			var izbranaOddaljenostPosta = document.getElementById("idPosta").value;
			var posteKraja = new Array();
			if (izbranaOddaljenostPosta != "null") {
				// poizvedba
				var request = {
			    	location: new google.maps.LatLng(globalneSpremenljivke.latLngMesta.sirina, globalneSpremenljivke.latLngMesta.dolzina), 
			    	radius: '5000',
			    	query: 'postna poslovalnica'
			  	};
			  	service = new google.maps.places.PlacesService(globalneSpremenljivke.map);
			  	service.textSearch(request, function(results, status){
					if (status == google.maps.places.PlacesServiceStatus.OK) {
				    	for (var i = 0; i < results.length; i++) {
				    		var posta = {
		    					latitude: results[i].geometry.location.lat(),
  			    			    longitude: results[i].geometry.location.lng()
  			    			};
				    		posteKraja.push(posta);
				    	}		
				  	}
			  	});
			}
			
		 	// filtrira in prikaže nepremičnine na zemljevidu
	  		$.getJSON('https://spreadsheets.google.com/feeds/list/15BtuJFX8MAYQ0t7Gl-ILFF3tpE2pewhfA7mfx9Sfs9M/995105425/public/values?alt=json&sq=' + poizvedba, function(json) {
	  			var stevilkaNepremicnine = 0;
	  			if (!jQuery.isEmptyObject(json.feed.entry)) {
					
	  				for (var i = 0; i < json.feed.entry.length; i++) {
	  					// preveri ceno
	  					var cenaOK = true;
						if (cenaOd != "" && cenaDo != "") {
							cenaOK = false;
							var c = json.feed.entry[i].gsx$cena.$t.split(",")[0].replace(".", "");								
							if (parseInt(c) >=  parseInt(cenaOd) &&  parseInt(c) <=  parseInt(cenaDo)) {
								cenaOK = true;
							}
						}
	  					// preveri velikost nepremičnine
	  					var velikostOK = true;
	  					var izbranaVelikost = document.getElementById("idVelikost").value;
						if (izbranaVelikost != "0" && cenaOK) {
							velikostOK = false;
							var velikostOd = izbranaVelikost.split("-")[0];
							var velikostDo = izbranaVelikost.split("-")[1];
							var velikostNeprem = json.feed.entry[i].gsx$velikost.$t.replace(".", "");
							velikostNeprem = velikostNeprem.replace(",", ".");
							if (parseFloat(velikostNeprem) >= parseFloat(velikostOd) && parseFloat(velikostNeprem) <= parseFloat(velikostDo)) {
								velikostOK = true;
							}				
						}
						
						var lokacijaNepremicnine = new google.maps.LatLng(json.feed.entry[i].gsx$latitude.$t, json.feed.entry[i].gsx$longitude.$t);
						// preveri oddaljenost od vrtca
						var vrtecOK = true;
						if (izbranaOddaljenostVrtec != "null" && cenaOK && velikostOK) {
							vrtecOK = false;
							for (var j = 0; j < vrtciKraja.length; j++) {	
								var lokacijaVrtca = new google.maps.LatLng(vrtciKraja[j].latitude, vrtciKraja[j].longitude);
								var razdaljaMetri = google.maps.geometry.spherical.computeDistanceBetween(lokacijaVrtca, lokacijaNepremicnine);									
								var razdaljaKm = parseFloat(razdaljaMetri) / 1000;
								if (razdaljaKm <= parseFloat(izbranaOddaljenostVrtec)) {
									vrtecOK = true;
									break;
								}	
							}
						}
						
						// preveri oddaljenost od osnovne šole
						var osnovnaSolaOK = true;
						if (izbranaOddaljenostOsnovnaSola != "null" && cenaOK && velikostOK && vrtecOK) {
							osnovnaSolaOK = false;
							for (var j = 0; j < osnovneSoleKraja.length; j++) {	
								var lokacijaOsnovneSole = new google.maps.LatLng(osnovneSoleKraja[j].latitude, osnovneSoleKraja[j].longitude);
								var razdaljaMetri = google.maps.geometry.spherical.computeDistanceBetween(lokacijaOsnovneSole, lokacijaNepremicnine);									
								var razdaljaKm = parseFloat(razdaljaMetri) / 1000;
								if (razdaljaKm <= parseFloat(izbranaOddaljenostOsnovnaSola)) {
									osnovnaSolaOK = true;
									break;
								}	
							}
						}
						
						// preveri oddaljenost od srednje šole
						var srednjaSolaOK = true;
						if (izbranaOddaljenostSrednjaSola != "null" && cenaOK && velikostOK && vrtecOK && osnovnaSolaOK) {
							srednjaSolaOK = false;
							for (var j = 0; j < srednjeSoleKraja.length; j++) {	
								var lokacijaOsnovneSole = new google.maps.LatLng(srednjeSoleKraja[j].latitude, srednjeSoleKraja[j].longitude);
								var razdaljaMetri = google.maps.geometry.spherical.computeDistanceBetween(lokacijaOsnovneSole, lokacijaNepremicnine);									
								var razdaljaKm = parseFloat(razdaljaMetri) / 1000;
								if (razdaljaKm <= parseFloat(izbranaOddaljenostSrednjaSola)) {
									srednjaSolaOK = true;
									break;
								}	
							}
						}
						
						// preveri oddaljenost od visokošolskega zavoda
						var visokosolskiZavodOK = true;
						if (izbranaOddaljenostVisokosolskiZavod != "null" && cenaOK && velikostOK && vrtecOK && osnovnaSolaOK && srednjaSolaOK) {
							visokosolskiZavodOK = false;
							for (var j = 0; j < visokosolskiZavodiKraja.length; j++) {	
								var lokacijaVisokosolskegaZavoda = new google.maps.LatLng(visokosolskiZavodiKraja[j].latitude, visokosolskiZavodiKraja[j].longitude);
								var razdaljaMetri = google.maps.geometry.spherical.computeDistanceBetween(lokacijaVisokosolskegaZavoda, lokacijaNepremicnine);									
								var razdaljaKm = parseFloat(razdaljaMetri) / 1000;
								if (razdaljaKm <= parseFloat(izbranaOddaljenostVisokosolskiZavod)) {
									visokosolskiZavodOK = true;
									break;
								}	
							}
						}
						
						// preveri oddaljenost od zdravstvene ustanove
						var zdravstvoOK = true;
						if (izbranaOddaljenostZdravstvo != "null" && cenaOK && velikostOK && vrtecOK && osnovnaSolaOK && srednjaSolaOK && visokosolskiZavodOK) {
							zdravstvoOK = false;
							for (var j = 0; j < zdravstvoKraja.length; j++) {	
								var lokacijaZdravstvo = new google.maps.LatLng(zdravstvoKraja[j].latitude, zdravstvoKraja[j].longitude);
								var razdaljaMetri = google.maps.geometry.spherical.computeDistanceBetween(lokacijaZdravstvo, lokacijaNepremicnine);									
								var razdaljaKm = parseFloat(razdaljaMetri) / 1000;
								if (razdaljaKm <= parseFloat(izbranaOddaljenostZdravstvo)) {
									zdravstvoOK = true;
									break;
								}	
							}
						}
						
						// preveri oddaljenost od zdravstvene ustanove
						var lekarnaoOK = true;
						if (izbranaOddaljenostLekarne != "null" && cenaOK && velikostOK && vrtecOK && osnovnaSolaOK && srednjaSolaOK && visokosolskiZavodOK && zdravstvoOK) {
							lekarnaoOK = false;
							for (var j = 0; j < lekarneKraja.length; j++) {	
								var lokacijaLekarne = new google.maps.LatLng(lekarneKraja[j].latitude, lekarneKraja[j].longitude);
								var razdaljaMetri = google.maps.geometry.spherical.computeDistanceBetween(lokacijaLekarne, lokacijaNepremicnine);									
								var razdaljaKm = parseFloat(razdaljaMetri) / 1000;
								if (razdaljaKm <= parseFloat(izbranaOddaljenostLekarne)) {
									lekarnaoOK = true;
									break;
								}	
							}
						}
						
						// preveri oddaljenost od javnega prometa
						var javniPrometOK = true; 
						if (izbranaOddaljenostJavniPromet != "null" && cenaOK && velikostOK && vrtecOK && osnovnaSolaOK && srednjaSolaOK && visokosolskiZavodOK && zdravstvoOK && lekarnaoOK) {
							javniPrometOK = false;
							for (var j = 0; j < javniPrometKraja.length; j++) {	
								var lokacijaJavniPromet = new google.maps.LatLng(javniPrometKraja[j].latitude, javniPrometKraja[j].longitude);
								var razdaljaMetri = google.maps.geometry.spherical.computeDistanceBetween(lokacijaJavniPromet, lokacijaNepremicnine);									
								var razdaljaKm = parseFloat(razdaljaMetri) / 1000;
								if (razdaljaKm <= parseFloat(izbranaOddaljenostJavniPromet)) {
									javniPrometOK = true;
									break;
								}	
							}
						}
						
						// preveri oddaljenost od gasilske postaje 
						var gasilciOK = true; 
						if (izbranaOddaljenostGasilci != "null" && cenaOK && velikostOK && vrtecOK && osnovnaSolaOK && srednjaSolaOK && visokosolskiZavodOK && zdravstvoOK && lekarnaoOK && javniPrometOK) {
							gasilciOK = false;
							for (var j = 0; j < gasilciKraja.length; j++) {	
								var lokacijaGasilskePostaje = new google.maps.LatLng(gasilciKraja[j].latitude, gasilciKraja[j].longitude);
								var razdaljaMetri = google.maps.geometry.spherical.computeDistanceBetween(lokacijaGasilskePostaje, lokacijaNepremicnine);									
								var razdaljaKm = parseFloat(razdaljaMetri) / 1000;
								if (razdaljaKm <= parseFloat(izbranaOddaljenostGasilci)) {
									gasilciOK = true;
									break;
								}	
							}
						}
						
						// preveri oddaljenost od poštne poslovalnice
						var postaOK = true;
						if (izbranaOddaljenostPosta != "null" && cenaOK && velikostOK && vrtecOK && osnovnaSolaOK && srednjaSolaOK && visokosolskiZavodOK && zdravstvoOK && lekarnaoOK && javniPrometOK && gasilciOK) {
							postaOK = false;
							for (var j = 0; j < posteKraja.length; j++) {	
								var lokacijaPoste = new google.maps.LatLng(posteKraja[j].latitude, posteKraja[j].longitude);
								var razdaljaMetri = google.maps.geometry.spherical.computeDistanceBetween(lokacijaPoste, lokacijaNepremicnine);									
								var razdaljaKm = parseFloat(razdaljaMetri) / 1000;
								if (razdaljaKm <= parseFloat(izbranaOddaljenostPosta)) {
									postaOK = true;
									break;
								}	
							}
						}
						
						
						if (cenaOK && velikostOK && vrtecOK && osnovnaSolaOK && srednjaSolaOK && visokosolskiZavodOK && zdravstvoOK && lekarnaoOK && javniPrometOK && gasilciOK && postaOK) {
		  					var n = {
								id: json.feed.entry[i].gsx$id.$t,
								tip: json.feed.entry[i].gsx$tip.$t,
								podtip: json.feed.entry[i].gsx$podtip.$t,
								vrstaPonudbe: json.feed.entry[i].gsx$vrstaponudbe.$t,
								url: json.feed.entry[i].gsx$url.$t,
								naziv: json.feed.entry[i].gsx$naziv.$t,
								cena: json.feed.entry[i].gsx$cena.$t,
								regija: json.feed.entry[i].gsx$regija.$t,
								kraj: json.feed.entry[i].gsx$kraj.$t,
								predel: json.feed.entry[i].gsx$predel.$t,
								velikost: json.feed.entry[i].gsx$velikost.$t,
								letoIzgradnje: json.feed.entry[i].gsx$letoizgradnje.$t,
								naslov: json.feed.entry[i].gsx$naslov.$t,
								opis: json.feed.entry[i].gsx$opis.$t,
								pasovnaSirina: json.feed.entry[i].gsx$latitude.$t,
								pasovnaDolzina: json.feed.entry[i].gsx$longitude.$t,
								naslovnaSlika: json.feed.entry[i].gsx$naslovnaslika.$t,
								dodano: json.feed.entry[i].gsx$dodano.$t,
								zadnjicPregledano: json.feed.entry[i].gsx$zadnjicpregledano.$t 
							};
							// doda nepremičnino v seznam
							globalneSpremenljivke.nepremicnine.push(n);
							
							// izdela marker 
							createMarker(n, stevilkaNepremicnine);
							stevilkaNepremicnine++;
						}
	  					
	  				}
	  				
	  				if (stevilkaNepremicnine > 0) {
						// nastavi grupiranje nepremičnin brez naslova
						var markerBrezNaslovaLastnosti = {
							styles: nepremicnineBrezNaslovaMarkerStil,
							maxZoom: 16
						};
						markerNepremicnineBrezNaslovaCluster = new MarkerClusterer(globalneSpremenljivke.map, markersNepremicnineBrezNaslova, markerBrezNaslovaLastnosti);
						// doda listener da se nepremičnine ki nimajo naslova ob določenem zoom-u ne pokažejo več
						google.maps.event.addListener(globalneSpremenljivke.map, 'zoom_changed', function() {
							var zoom = globalneSpremenljivke.map.getZoom();
							// glede na zoom markerje postavi na vidne ali nevidne
							for (i = 0; i < markersNepremicnineBrezNaslova.length; i++) {
								markersNepremicnineBrezNaslova[i].setVisible(zoom <= 16);
							}
						});

						// nastavi grupiranje nepremičnin z naslovom
						var markerZNaslovomLastnosti = {
							styles: nepremicnineZNaslovomMarkerStil,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerNepremicnineZNaslovomCluster = new MarkerClusterer(globalneSpremenljivke.map, globalneSpremenljivke.markerjiNepremicnin, markerZNaslovomLastnosti);
						
						// prikaže število nepremičnin   					
						document.getElementById('steviloZadetkovStevilka').innerHTML = String(stevilkaNepremicnine).bold();
					}
			  		else {
			  			document.getElementById('steviloZadetkovStevilka').innerHTML = "0".bold();
			  			alert("Nobena nepremičnina ne ustreza poizvedbi! Ponovno izberi vrednosti parametrov.");			  			
			  		}
	  				
	  				
	  			}
	  			else {
	  				document.getElementById('steviloZadetkovStevilka').innerHTML = "0".bold();
		  			alert("Nobena nepremičnina ne ustreza poizvedbi! Ponovno izberi vrednosti parametrov.");		  			
		  		}
	  			
		    }); 
		 	
		 	// na zemljevidu prikaži izbrane bližnje objekte
		 	prikaziIzbraneObjekte();
		}
		
		// kreira marker in ga postavi na zemljevid
		function createMarker(neprem, index) { 
			var marker;			
			
			if (neprem.pasovnaSirina == "" && neprem.pasovnaDolzina == "") {
				var pasovnaSirinaMarker = parseFloat(globalneSpremenljivke.latLngMesta.sirina) + 0.001;
				var pasovnaDolzinaMarker = parseFloat(globalneSpremenljivke.latLngMesta.dolzina) - 0.001;
				marker = new google.maps.Marker({
					position: new google.maps.LatLng(pasovnaSirinaMarker, pasovnaDolzinaMarker),
					map: globalneSpremenljivke.map,
					icon: 'map_icons/realestate1.png',
					id: markersNepremicnineBrezNaslova.length,
					stevilka: index,
					slika: ""
			  	});
				markersNepremicnineBrezNaslova.push(marker);
			}
			else {	
				
				// preveri če je nepremičnina že med priljubljenimi
		  		for (var i = 0; i < globalneSpremenljivke.priljubljene.length; i++) {
		  			if (globalneSpremenljivke.priljubljene[i].id == neprem.id) {
		  				//marker.setMap(null);
		  				// ???????????
		  				var markerPriljubljen = globalneSpremenljivke.priljubljene[i].markerNepremicnine;
		  				markerPriljubljen.id = markersNepremicnineBrezNaslova.length; 
		  				markerPriljubljen.stevilka = index;
		  				globalneSpremenljivke.markerjiNepremicnin.push(markerPriljubljen);
		  				break;
		  			}
		  		}
		  		if (i == globalneSpremenljivke.priljubljene.length) {
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(neprem.pasovnaSirina, neprem.pasovnaDolzina),
						map: globalneSpremenljivke.map,
						icon: 'map_icons/realestate.png',
						idNepremicnine: neprem.id,
						id: globalneSpremenljivke.markerjiNepremicnin.length,
						stevilka: index,
						slika: neprem.naslovnaSlika
				  	}); 				
					
					var cena = "/";
					if (neprem.cena != "") {
						cena = neprem.cena + " €";
					}
					var podtip = "/";
					if (neprem.podtip != "") {
						podtip = neprem.podtip; 
					}
					var content = '<div id="infobox" style="width:360px;height:230px">' +
									'<div id="nazivNep_infobox">' + neprem.naziv + '</div>' +
									'<div id="picture-panel"><img id="nepPicture" alt="" src="' + neprem.naslovnaSlika + '" width="130" height="120"/></div>' + 		
									'<div id="properties-detail">' +
										'<table>' +
											'<tr><td id="nepNaslov"><b>Naslov:</b> ' + neprem.naslov + ' ' + neprem.kraj + '</td></tr>' +
											'<tr><td id="nepCena"><b>Cena:</b> ' + cena + '</td></tr>' +
											'<tr><td id="nepTip"><b>Tip:</b> ' + neprem.tip + '</td></tr>' +
											'<tr><td id="nepPodtip"><b>Podtip:</b> ' + podtip + '</td></tr>' +
											'<tr><td id="nepVelikost"><b>Velikost:</b> ' + neprem.velikost + ' m<sup>2</sup></td></tr>' +
										'</table>' +								
									'</div>' +
									'<div id="infobox-datumi">' +
										'<b>Dodano v iskalnik</b>: ' + neprem.dodano + ', <b>zadnji pregled</b>: ' + neprem.zadnjicPregledano +
									'</div>' +
									'<div id="infobox-buttons">' +
									'<input type="button" value="Podrobnosti" onclick="overlay()"> <input type="button" id="addFavourite" value="Priljubljene" onclick="addToFavourites()">' + 
									'</div>' +
								  '</div>';
								  
					// omogoči klik na marker
				  	google.maps.event.addListener(marker, 'click', function() { 	  			
				  		// prikaže okno
						globalneSpremenljivke.infowindow.open(globalneSpremenljivke.map, marker);
				  		// nastavi vsebino okna
						globalneSpremenljivke.infowindow.setContent(content);
						// začetni naslov za iskanje poti vpiše v začetno polje
				  		document.getElementById('start').value = neprem.naslov + " " + neprem.kraj;
						// zapomni si nepremičnino za overlay window
				  		globalneSpremenljivke.nepremicnina = neprem;
				  		// id markerja
				  		globalneSpremenljivke.currentMarker = marker.id;
				  	});
					
				 	// da dropdown priljubljene na default vrednost
			  		google.maps.event.addListener(globalneSpremenljivke.infowindow, 'closeclick', function() {
			  			document.getElementById("realestateFavourites").value = 0;
					});
			 	
		  		
		  			globalneSpremenljivke.markerjiNepremicnin.push(marker);
		  		}
		  		
		  		
			}		  	
		}
		
		// postavi zemljevid na center izbranega mesta
		function centerMesta(pasovnaSirina, pasovnaDolzina) {
			var latlngCenter = new google.maps.LatLng(pasovnaSirina, pasovnaDolzina);
			// nastavi center zemljevida
			globalneSpremenljivke.map.setCenter(latlngCenter);
			// nastavi zoom zemljevida
			globalneSpremenljivke.map.setZoom(14);
			// shrani pasovno širino in dolžino mesta
			globalneSpremenljivke.latLngMesta = {
				sirina: pasovnaSirina,
				dolzina: pasovnaDolzina
			};
		}
		
		// da vrednosti vseh parametrov na default
		function resetParametri() {
			// ponastavi vse dropdown liste parametrov iskanja
			var selectElementi = document.getElementsByTagName('select');
    		for (var i = 0; i < selectElementi.length - 1; i++) {
    			selectElementi[i].selectedIndex = 0;
    		}
    		// ponastavi vse textbox-e
    		document.getElementById("idCenaOd").value = "";
    		document.getElementById("idCenaDo").value = "";
    		document.getElementById("vrtciText").value = "";
    		document.getElementById("osnovnaSolaText").value = "";
    		document.getElementById("srednjaSolaText").value = "";
    		document.getElementById("visokosolskiZavodText").value = "";
    		document.getElementById("zdravstvoText").value = "";
    		document.getElementById("lekarnaText").value = "";
    		// ponastavi vse checkbox-e
    		var checkboxElementi = document.getElementsByTagName('input');
			for (var i = 0; i < checkboxElementi.length; i++)  {
				if (checkboxElementi[i].type == 'checkbox')   {
					checkboxElementi[i].checked = false;
			    }
			}
			
			// sprazne sezname in pobiše markerje ostalih objektov
			if (markersVrtci.length > 0) {
				for (var i = 0; i < markersVrtci.length; i++) {
					// odstrani marker iz zemljevida
					markersVrtci[i].setMap(null);
				}
				// sprazne seznam 
				markersVrtci = [];
				markerVrtecCluster.clearMarkers();
			}
			if (markersOsnovneSole.length > 0) {
				for (var i = 0; i < markersOsnovneSole.length; i++) {
					// odstrani markerje iz zemljevida
					markersOsnovneSole[i].setMap(null);
				}
				// sprazne seznam
				markersOsnovneSole = [];
				markerOSCluster.clearMarkers();
			}
			if (markersSrednjeSole.length > 0) {
				for (var i = 0; i < markersSrednjeSole.length; i++) {
					// odstrani markerje iz zemljevida
					markersSrednjeSole[i].setMap(null);
				}
				// sprazne seznam
				markersSrednjeSole = [];
				markerSSCluster.clearMarkers();
			}	
			if (markersFakultete.length > 0) {
				for (var i = 0; i < markersFakultete.length; i++) {
					// odstrani markerje iz zemljevida
					markersFakultete[i].setMap(null);
				}
				// sprazne seznam
				markersFakultete = [];
				markerFaksCluster.clearMarkers();
			}
			if (markersZdravstvo.length > 0 ) {
				for (var i = 0; i < markersZdravstvo.length; i++) {
					// odstrani markerje iz zemljevida
					markersZdravstvo[i].setMap(null);
				}					
				// sprazne seznam
				markersZdravstvo = [];
				markerZdravstvoCluster.clearMarkers();					
			}
			if (markersLekarne.length > 0) {
				for (var i = 0; i < markersLekarne.length; i++) {
					// odstrani markerje iz zemljevida
					markersLekarne[i].setMap(null);
				}
				// sprazne seznam
				markersLekarne = [];
				markerLekarneCluster.clearMarkers();
			}
			if (markersAvtobusnePostaje.length > 0) {
				for (var i = 0; i < markersAvtobusnePostaje.length; i++) {
					// odstrani markerje iz zemljevida
					markersAvtobusnePostaje[i].setMap(null);
				}
				// sprazne seznam
				markersAvtobusnePostaje = [];
				markerAvtobusnePostajeCluster.clearMarkers();
			}	
			if (markersZelezniskePostaje.length > 0) {	
				for (var i = 0; i < markersZelezniskePostaje.length; i++) {
					// odstrani markerje iz zemljevida
					markersZelezniskePostaje[i].setMap(null);
				}
				// sprazne seznam	
				markersZelezniskePostaje = [];
				markerZelezniskePostajeCluster.clearMarkers();
			}
			if (markersGasilci.length > 0) {
				for (var i = 0; i < markersGasilci.length; i++) {
					// odstrani marker iz zemljevida
					markersGasilci[i].setMap(null);
				}
				// sprazne seznam 
				markersGasilci = [];
				markerGasilciCluster.clearMarkers();
			}
			if (markersPosta.length > 0) {
				for (var i = 0; i < markersPosta.length; i++) {
					// odstrani marker iz zemljevida
					markersPosta[i].setMap(null);
				}
				// sprazne seznam 
				markersPosta = [];
				markerPostaCluster.clearMarkers();
			}
			
    		// pobriše vse nepremičnine iz zemljevida 
			// zbriše trenutno grupiranje markerjev 
			if (globalneSpremenljivke.markerjiNepremicnin.length > 0) {
				markerNepremicnineZNaslovomCluster.clearMarkers();
			}
			if (markersNepremicnineBrezNaslova.length > 0) {
				markerNepremicnineBrezNaslovaCluster.clearMarkers();
			} 
			// izbriše markerje iz zemljevida
			for (var i = 0; i < globalneSpremenljivke.markerjiNepremicnin.length; i++) {
				globalneSpremenljivke.markerjiNepremicnin[i].setMap(null);
			}
			for (var i = 0; i < markersNepremicnineBrezNaslova.length; i++) {
				markersNepremicnineBrezNaslova[i].setMap(null);
			}
			// izprazne sezname markerjev
			globalneSpremenljivke.nepremicnine = [];
			globalneSpremenljivke.markerjiNepremicnin = [];
			markersNepremicnineBrezNaslova = [];			
    				
			// zapre okno 
			globalneSpremenljivke.infowindow.close();
			// prikaže priljubljene na zemljevidu
			for (var i = 0; i < globalneSpremenljivke.priljubljene.length; i++) {
				globalneSpremenljivke.priljubljene[i].markerNepremicnine.setMap(globalneSpremenljivke.map);
			}
			
    		// da število zadetkov na nič
    		document.getElementById("steviloZadetkovStevilka").innerHTML = "0".bold();
    		// da zoom na začetek 
    		globalneSpremenljivke.map.setCenter(new google.maps.LatLng(46.151241, 14.995463));
    		globalneSpremenljivke.map.setZoom(8);
    		
    		
		}
		
		// stili in array-i za grupiranje markerjev
		var vrtecMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/daycare.png',							
			height: 37,
			width: 32						
		   }, 
		   {
			textColor: '#E62E00',
			textSize: 14,	
			url: 'map_icons/daycare.png',							
			height: 37,
			width: 32		
		   }, 
		   {
			textColor: 'red',
			textSize: 14,
			url: 'map_icons/daycare.png',							
			height: 37,
			width: 32										
		}];
		var markersVrtci = [];
		var markerVrtecCluster = null;
		var osMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/school.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 14,	
       		url: 'map_icons/school.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/school.png',
       	    width: 37,
       	    height: 32,
       	}];
		var markersOsnovneSole = [];
		var markerOSCluster = null;
		var ssMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/highschool.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 14,	
       		url: 'map_icons/highschool.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/highschool.png',
       	    width: 37,
       	    height: 32,
       	}];
		var markersSrednjeSole = [];
		var markerSSCluster = null;
		var zdravstvoMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/hospital.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 14,	
       		url: 'map_icons/hospital.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/hospital.png',
       	    width: 37,
       	    height: 32,
       	}];
		var markersZdravstvo = [];
		var markerZdravstvoCluster = null;
		var lekarneMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/pharmacy.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
       		textSize: 14,	
       		url: 'map_icons/pharmacy.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/pharmacy.png',
       	    width: 37,
       	    height: 32,
       	}];
		var markersLekarne = [];
		var markerLekarneCluster = null;
		var faxsMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/university.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 14,	
       		url: 'map_icons/university.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/university.png',
       	    width: 37,
       	    height: 32,
       	}];
		var markersFakultete = [];
		var markerFaksCluster = null;
		var postaMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/postal.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 14,	
       		url: 'map_icons/postal.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/postal.png',
       	    width: 37,
       	    height: 32,
       	}];
		var markersPosta = [];
		var markerPostaCluster = null;
       	var gasilciMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/firemen.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 14,	
       		url: 'map_icons/firemen.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/firemen.png',
       	    width: 37,
       	    height: 32,
       	}];
       	var markersGasilci = [];
		var markerGasilciCluster = null;
       	var avtobusMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/bus.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 14,	
       		url: 'map_icons/bus.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 13,
       		url: 'map_icons/bus.png',
       	    width: 37,
       	    height: 32,
       	}];
       	var markersAvtobusnePostaje = [];
		var markerAvtobusnePostajeCluster = null;
       	var vlakMarkerStyles = [{
			textColor: 'black',
			textSize: 13,
			url: 'map_icons/train.png',
	        height: 37,
	        width: 32,
       	   }, 
       	   {
       		textColor: '#E62E00',
			textSize: 14,	
       		url: 'map_icons/train.png', 
      	    height: 37,
      	    width: 32,    
       	   }, 
       	   {
       		textColor: 'red',
       		textSize: 14,
       		url: 'map_icons/train.png',
       	    width: 37,
       	    height: 32,
       	}];
		var markersZelezniskePostaje = [];
		var markerZelezniskePostajeCluster = null;
		
		// na zemljevidu prikaže ostale izbrane objekte
		function prikaziIzbraneObjekte() {
			var izbranKraj = document.getElementById("idMesto").value;
			
			// prikaže vse vrtce v izbranem kraju
			if (document.getElementById('vrtciBox').checked) {
				if (markersVrtci.length > 0) {
					for (var i = 0; i < markersVrtci.length; i++) {
						// odstrani marker iz zemljevida
						markersVrtci[i].setMap(null);
					}
					// sprazne seznam 
					markersVrtci = [];
					markerVrtecCluster.clearMarkers();
				}
				markersVrtci = new Array();					
				$.getJSON('https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/707867289/public/values?alt=json&sq=obcina="'+ izbranKraj + '"', function(json) {
					if (!jQuery.isEmptyObject(json.feed.entry)) {
						for (var i = 0; i < json.feed.entry.length; i++) {
							var latlng = new google.maps.LatLng(json.feed.entry[i].gsx$latitude.$t, json.feed.entry[i].gsx$longitude.$t);
							var marker = new google.maps.Marker({
								position: latlng,
								map: globalneSpremenljivke.map,
								icon: 'map_icons/daycare.png'
						  	}); 
							// vsebina na info oknu
							var content = '<div id="infoClosest" style="width:290px;height:165px">' + 
											'<div id="infoNaziv"><b>' + json.feed.entry[i].gsx$nazivvrtca.$t + '</b></div>' +
											'<div id="infoNaslov>' + json.feed.entry[i].gsx$naslov.$t + ', ' + json.feed.entry[i].gsx$postnastevilka.$t + ' ' + json.feed.entry[i].gsx$obcina.$t +'</div>' +
											'<div id="infoTelefon">Telefon: ' + json.feed.entry[i].gsx$telefon.$t + ' </div>' +
											'<div id="infoFax">Fax: ' + json.feed.entry[i].gsx$faks.$t + '</div>' +
											'<div id="infoPriOS">Pri OS: ' + json.feed.entry[i].gsx$prios.$t + '</div>' +
											'<div id="infoZaseben">Zaseben: ' + json.feed.entry[i].gsx$zaseben.$t + '</div>' +
											'<div id="infoEmail">Email: ' + json.feed.entry[i].gsx$email.$t + '</div>' +
											'<div id="infoSpletnaStran">Spletna stran: ' + json.feed.entry[i].gsx$spletnastran.$t + '</div>' +
										  '</div>';								  
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, content){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(content);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, content));
							// doda markerje vrtcev v seznam
							markersVrtci.push(marker);
						}
						var mcOptions = {
							styles: vrtecMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerVrtecCluster = new MarkerClusterer(globalneSpremenljivke.map, markersVrtci, mcOptions);
					}
			    });											
			}
			else {
				if (markersVrtci.length > 0) {
					for (var i = 0; i < markersVrtci.length; i++) {
						// odstrani marker iz zemljevida
						markersVrtci[i].setMap(null);
					}
					// sprazne seznam 
					markersVrtci = [];
					markerVrtecCluster.clearMarkers();
				}
			}
			
			// prikaže vse osnovne šole v izbranem kraju
			if (document.getElementById('osnovnaSolaBox').checked) {
				if (markersOsnovneSole.length > 0) {
					for (var i = 0; i < markersOsnovneSole.length; i++) {
						// odstrani markerje iz zemljevida
						markersOsnovneSole[i].setMap(null);
					}
					// sprazne seznam
					markersOsnovneSole = [];
					markerOSCluster.clearMarkers();
				}	
				markersOsnovneSole = new Array();					
				$.getJSON('https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/277039759/public/values?alt=json&sq=obcina="'+ izbranKraj + '"', function(json) {
					if (!jQuery.isEmptyObject(json.feed.entry)) {	
						for (var i = 0; i < json.feed.entry.length; i++) {
							var latlng = new google.maps.LatLng(json.feed.entry[i].gsx$latitude.$t, json.feed.entry[i].gsx$longitude.$t);
							var marker = new google.maps.Marker({
								position: latlng,
								map: globalneSpremenljivke.map,
								icon: 'map_icons/school.png'
						  	});	
							// vsebina na info oknu 
							var content = '<div id="infoClosest" style="width:300px;height:130px">' + 
											'<div id="infoNaziv"><b>' + json.feed.entry[i].gsx$nazivsole.$t + '</b></div>' +
											'<div id="infoNaslov">' + json.feed.entry[i].gsx$naslov.$t + ', ' + json.feed.entry[i].gsx$postnastevilka.$t + ' ' + json.feed.entry[i].gsx$obcina.$t +'</div>' +
											'<div id="infoTelefon">Telefon: ' + json.feed.entry[i].gsx$telefon.$t + ' </div>' +
											'<div id="infoFax">Fax: ' + json.feed.entry[i].gsx$faks.$t + '</div>' +
											'<div id="infoEmail">Email: ' + json.feed.entry[i].gsx$email.$t + '</div>' +
											'<div id="infoSpletnaStran">Spletna stran: ' + json.feed.entry[i].gsx$spletnastran.$t + '</div>' +
										  '</div>';								  
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, content){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(content);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, content));
							// doda markerje osnovnih šol v seznam
							markersOsnovneSole.push(marker);
						}
						var mcOptions = {
							styles: osMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerOSCluster = new MarkerClusterer(globalneSpremenljivke.map, markersOsnovneSole, mcOptions);	
					}
			    });										
			}
			else {
				if (markersOsnovneSole.length > 0) {
					for (var i = 0; i < markersOsnovneSole.length; i++) {
						// odstrani markerje iz zemljevida
						markersOsnovneSole[i].setMap(null);
					}
					// sprazne seznam
					markersOsnovneSole = [];
					markerOSCluster.clearMarkers();
				}				
			}	
			
			// prikaže srednje šole
			if (document.getElementById('srednjaSolaBox').checked) { 				
				if (markersSrednjeSole.length > 0) {
					for (var i = 0; i < markersSrednjeSole.length; i++) {
						// odstrani markerje iz zemljevida
						markersSrednjeSole[i].setMap(null);
					}
					// sprazne seznam
					markersSrednjeSole = [];
					markerSSCluster.clearMarkers();
				}	
				markersSrednjeSole = new Array();					
				$.getJSON('https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/2057546630/public/values?alt=json&sq=posta="'+ izbranKraj + '"', function(json) {
					if (!jQuery.isEmptyObject(json.feed.entry)) {
						for (var i = 0; i < json.feed.entry.length; i++) {							
							var latlng = new google.maps.LatLng(json.feed.entry[i].gsx$latitude.$t, json.feed.entry[i].gsx$longitude.$t);
							var marker = new google.maps.Marker({
								position: latlng,
								map: globalneSpremenljivke.map,
								icon: 'map_icons/highschool.png'
						  	}); 
							// vsebina na info oknu 
							var content = '<div id="infoClosest" style="width:290px;height:130px">' + 
											'<div id="infoNaziv"><b>' + json.feed.entry[i].gsx$nazivsole.$t + '</b></div>' +
											'<div id="infoNaslov">' + json.feed.entry[i].gsx$naslov.$t + ', ' + json.feed.entry[i].gsx$postnastevilka.$t + ' ' + json.feed.entry[i].gsx$posta.$t +'</div>' +
											'<div id="infoTelefon">Telefon: ' + json.feed.entry[i].gsx$telefon.$t + ' </div>' +
											'<div id="infoFax">Fax: ' + json.feed.entry[i].gsx$faks.$t + '</div>' +
											'<div id="infoEmail">Email: ' + json.feed.entry[i].gsx$email.$t + '</div>' +
											'<div id="infoSpletnaStran">Spletna stran: ' + json.feed.entry[i].gsx$spletnastran.$t + '</div>' +
										  '</div>';	
										 
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, content){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(content);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, content));
							// doda markerje srednjih šol v seznam
							markersSrednjeSole.push(marker);	
						}
						var mcOptions = {
							styles: ssMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerSSCluster = new MarkerClusterer(globalneSpremenljivke.map, markersSrednjeSole, mcOptions);
					}
			    });				
			}
			else {
				if (markersSrednjeSole.length > 0) {
					for (var i = 0; i < markersSrednjeSole.length; i++) {
						// odstrani markerje iz zemljevida
						markersSrednjeSole[i].setMap(null);
					}
					// sprazne seznam
					markersSrednjeSole = [];
					markerSSCluster.clearMarkers();
				}				
			}	
			
			// prikaže visokošolske zavode
			if (document.getElementById('visokosolskiZavodBox').checked) { 
				if (markersFakultete.length > 0) {
					for (var i = 0; i < markersFakultete.length; i++) {
						// odstrani markerje iz zemljevida
						markersFakultete[i].setMap(null);
					}
					// sprazne seznam
					markersFakultete = [];
					markerFaksCluster.clearMarkers();
				}	
				markersFakultete = new Array();					
				$.getJSON('https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/791582045/public/values?alt=json&sq=obcina="'+ izbranKraj + '"', function(json) {
					if (!jQuery.isEmptyObject(json.feed.entry)) {
						for (var i = 0; i < json.feed.entry.length; i++) {							
							var latlng = new google.maps.LatLng(json.feed.entry[i].gsx$latitude.$t, json.feed.entry[i].gsx$longitude.$t);
							var marker = new google.maps.Marker({
								position: latlng,
								map: globalneSpremenljivke.map,
								icon: 'map_icons/university.png'
						  	}); 
							// vsebina na info oknu 
							var content = '<div id="infoClosest" style="width:300px;height:150px">' + 
											'<div id="infoNaziv"><b>' + json.feed.entry[i].gsx$nazivsole.$t + '</b></div>' +
											'<div id="infoNaslov">' + json.feed.entry[i].gsx$naslov.$t + ', ' + json.feed.entry[i].gsx$postnastevilka.$t + ' ' + json.feed.entry[i].gsx$obcina.$t +'</div>' +
											'<div id="infoTelefon">Telefon: ' + json.feed.entry[i].gsx$telefon.$t + ' </div>' +
											'<div id="infoFax">Fax: ' + json.feed.entry[i].gsx$faks.$t + '</div>' +
											'<div id="infoEmail">Email: ' + json.feed.entry[i].gsx$email.$t + '</div>' +
											'<div id="infoSpletnaStran">Spletna stran: ' + json.feed.entry[i].gsx$spletnastran.$t + '</div>' +
										  '</div>';								  
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, content){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(content);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, content));
							// doda markerje fakultet v seznam								
							markersFakultete.push(marker);								
						}	
						var mcOptions = {
							styles: faxsMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerFaksCluster = new MarkerClusterer(globalneSpremenljivke.map, markersFakultete, mcOptions);
					}
			    });				
			}
			else {
				if (markersFakultete.length > 0) {
					for (var i = 0; i < markersFakultete.length; i++) {
						// odstrani markerje iz zemljevida
						markersFakultete[i].setMap(null);
					}
					// sprazne seznam
					markersFakultete = [];
					markerFaksCluster.clearMarkers();
				}				
			}	
			
			// prikaže bolnišnice in zdravstvene domove
			if (document.getElementById('zdravstvoBox').checked) {
				if (markersZdravstvo.length > 0 ) {
					for (var i = 0; i < markersZdravstvo.length; i++) {
						// odstrani markerje iz zemljevida
						markersZdravstvo[i].setMap(null);
					}					
					// sprazne seznam
					markersZdravstvo = [];
					markerZdravstvoCluster.clearMarkers();					
				}
				markersZdravstvo = new Array();					
				$.getJSON('https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/1289548467/public/values?alt=json&sq=posta="'+ izbranKraj + '"', function(json) {
					if (!jQuery.isEmptyObject(json.feed.entry)) {
						for (var i = 0; i < json.feed.entry.length; i++) {
							var latlng = new google.maps.LatLng(json.feed.entry[i].gsx$latitude.$t, json.feed.entry[i].gsx$longitude.$t);
							var marker = new google.maps.Marker({
								position: latlng,
								map: globalneSpremenljivke.map,
								icon: 'map_icons/hospital.png'
						  	}); 
							// vsebina na info oknu 
							var content = '<div id="infoClosest" style="width:280px;height:110px">' + 
											'<div id="infoNaziv"><b>' + json.feed.entry[i].gsx$naziv.$t + '</b></div>' +
											'<div id="infoNaslov">' + json.feed.entry[i].gsx$naslov.$t + ', ' + json.feed.entry[i].gsx$postnastevilka.$t + ' ' + json.feed.entry[i].gsx$posta.$t +'</div>' +
											'<div id="infoTelefon">Telefon: ' + json.feed.entry[i].gsx$telefon.$t + ' </div>' +
											'<div id="infoFax">Fax: ' + json.feed.entry[i].gsx$faks.$t + '</div>' +
											'<div id="infoSpletnaStran">Spletna stran: ' + json.feed.entry[i].gsx$spletnastran.$t + '</div>' +
										  '</div>';								  
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, content){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(content);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, content));
							// doda markerje bolnišnic, zdravstvenih domov v seznam
							markersZdravstvo.push(marker);								
						}	
						var mcOptions = {
							styles: zdravstvoMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerZdravstvoCluster = new MarkerClusterer(globalneSpremenljivke.map, markersZdravstvo, mcOptions);
					}
			    });	
			}
			else {
				if (markersZdravstvo.length > 0 ) {
					for (var i = 0; i < markersZdravstvo.length; i++) {
						// odstrani markerje iz zemljevida
						markersZdravstvo[i].setMap(null);
					}					
					// sprazne seznam
					markersZdravstvo = [];
					markerZdravstvoCluster.clearMarkers();					
				}
			}
			
			// prikaže lekarne
			if (document.getElementById('lekarnaBox').checked) {
				if (markersLekarne.length > 0) {
					for (var i = 0; i < markersLekarne.length; i++) {
						// odstrani markerje iz zemljevida
						markersLekarne[i].setMap(null);
					}
					// sprazne seznam
					markersLekarne = [];
					markerLekarneCluster.clearMarkers();
				}
				$.getJSON('https://spreadsheets.google.com/feeds/list/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/1550820942/public/values?alt=json&sq=posta="'+ izbranKraj + '"', function(json) {
					if (!jQuery.isEmptyObject(json.feed.entry)) {
						for (var i = 0; i < json.feed.entry.length; i++) {
							var latlng = new google.maps.LatLng(json.feed.entry[i].gsx$latitude.$t, json.feed.entry[i].gsx$longitude.$t);
							var marker = new google.maps.Marker({
								position: latlng,
								map: globalneSpremenljivke.map,
								icon: 'map_icons/pharmacy.png'
						  	}); 
							// vsebina na info oknu 
							var content = '<div id="infoClosest" style="width:280px;height:130px">' + 
											'<div id="infoNaziv"><b>' + json.feed.entry[i].gsx$naziv.$t + '</b></div>' +
											'<div id="infoNaslov">' + json.feed.entry[i].gsx$naslov.$t + ', ' + json.feed.entry[i].gsx$postnastevilka.$t + ' ' + json.feed.entry[i].gsx$posta.$t +'</div>' +
											'<div id="infoTelefon">Telefon: ' + json.feed.entry[i].gsx$telefon.$t + ' </div>' +
											'<div id="infoFax">Fax: ' + json.feed.entry[i].gsx$faks.$t + '</div>' +
											'<div id="infoEmail">Email: ' + json.feed.entry[i].gsx$email.$t + '</div>' +
											'<div id="infoSpletnaStran">Spletna stran: ' + json.feed.entry[i].gsx$spletnastran.$t + '</div>' +
										  '</div>';								  
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, content){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(content);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, content));
							// doda markerje lekarn v seznam
							markersLekarne.push(marker);								
						}	
						var mcOptions = {
							styles: lekarneMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerLekarneCluster = new MarkerClusterer(globalneSpremenljivke.map, markersLekarne, mcOptions);
					}
			    });
			}
			else {
				if (markersLekarne.length > 0) {
					for (var i = 0; i < markersLekarne.length; i++) {
						// odstrani markerje iz zemljevida
						markersLekarne[i].setMap(null);
					}
					// sprazne seznam
					markersLekarne = [];
					markerLekarneCluster.clearMarkers();
				}
			}
			
			// prikaže avtobusne in železniške postaje
			if (document.getElementById('javniPrometBox').checked) {
				// avtobusne postaje
				if (markersAvtobusnePostaje.length > 0) {
					for (var i = 0; i < markersAvtobusnePostaje.length; i++) {
						// odstrani markerje iz zemljevida
						markersAvtobusnePostaje[i].setMap(null);
					}
					// sprazne seznam 
					markersAvtobusnePostaje = [];
					markerAvtobusnePostajeCluster.clearMarkers();
				}
				// poizvedba
			  	var request = {
					location: new google.maps.LatLng(globalneSpremenljivke.latLngMesta.sirina, globalneSpremenljivke.latLngMesta.dolzina),
					radius: 10000,
					types: ['bus_station']
				};				
			  	service = new google.maps.places.PlacesService(globalneSpremenljivke.map);
			  	service.nearbySearch(request, function(results, status){
					if (status == google.maps.places.PlacesServiceStatus.OK) {
				    	for (var i = 0; i < results.length; i++) {
				    		// izdela marker in info okno
							var infoContent = '<div style="width:220px;height:40px">Avtobusna postaja ' + results[i].name + '<div>';
							var marker = new google.maps.Marker({
						    	map: globalneSpremenljivke.map,
						    	position: results[i].geometry.location,
								icon: 'map_icons/bus.png'
						  	});
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, infoContent){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(infoContent);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, infoContent));
						  	// doda marker v seznam
						  	markersAvtobusnePostaje.push(marker);
				    	}
				    	var mcOptions = {
							styles: avtobusMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerAvtobusnePostajeCluster = new MarkerClusterer(globalneSpremenljivke.map, markersAvtobusnePostaje, mcOptions);	
				  	}
			  	});	
			  	
			 	// železniške postaje postaje
				if (markersZelezniskePostaje.length > 0) {
					for (var i = 0; i < markersZelezniskePostaje.length; i++) {
						// odstrani markerje iz zemljevida
						markersZelezniskePostaje[i].setMap(null);
					}
					// sprazne seznam 
					markersZelezniskePostaje = [];
					markerZelezniskePostajeCluster.clearMarkers();
				}
				// poizvedba
			  	var request = {
					location: new google.maps.LatLng(globalneSpremenljivke.latLngMesta.sirina, globalneSpremenljivke.latLngMesta.dolzina),
					radius: 10000,
					types: ['train_station']
				};				
			  	service = new google.maps.places.PlacesService(globalneSpremenljivke.map);
			  	service.nearbySearch(request, function(results, status){
					if (status == google.maps.places.PlacesServiceStatus.OK) {
				    	for (var i = 0; i < results.length; i++) {
				    		// izdela marker in info okno
							var infoContent = '<div style="width:220px;height:40px">Železniška postaja ' + results[i].name + '<div>';
							var marker = new google.maps.Marker({
						    	map: globalneSpremenljivke.map,
						    	position: results[i].geometry.location,
								icon: 'map_icons/train.png'
						  	});
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, infoContent){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(infoContent);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, infoContent));
						  	// doda marker v seznam
						  	markersZelezniskePostaje.push(marker);
				    	}
				    	var mcOptions = {
							styles: vlakMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerZelezniskePostajeCluster = new MarkerClusterer(globalneSpremenljivke.map, markersZelezniskePostaje, mcOptions);	
				  	}
			  	});		  
					  
			}
			else {
				if (markersAvtobusnePostaje.length > 0) {
					for (var i = 0; i < markersAvtobusnePostaje.length; i++) {
						// odstrani markerje iz zemljevida
						markersAvtobusnePostaje[i].setMap(null);
					}
					// sprazne seznam
					markersAvtobusnePostaje = [];
					markerAvtobusnePostajeCluster.clearMarkers();
				}	
				if (markersZelezniskePostaje.length > 0) {	
					for (var i = 0; i < markersZelezniskePostaje.length; i++) {
						// odstrani markerje iz zemljevida
						markersZelezniskePostaje[i].setMap(null);
					}
					// sprazne seznam	
					markersZelezniskePostaje = [];
					markerZelezniskePostajeCluster.clearMarkers();
				}	
			}
			
			// prikaže gasilske postaje
			if (document.getElementById('gasilciBox').checked) {
				if (markersGasilci.length > 0) {
					for (var i = 0; i < markersGasilci.length; i++) {
						// odstrani marker iz zemljevida
						markersGasilci[i].setMap(null);
					}
					// sprazne seznam
					markersGasilci = [];
					markerGasilciCluster.clearMarkers();
				}
				// poizvedba
				var request = {
			    	location: new google.maps.LatLng(globalneSpremenljivke.latLngMesta.sirina, globalneSpremenljivke.latLngMesta.dolzina), 
			    	radius: '5000',
			    	query: 'gasilsko drustvo'
			  	};
			  	service = new google.maps.places.PlacesService(globalneSpremenljivke.map);
			  	service.textSearch(request, function(results, status){
					if (status == google.maps.places.PlacesServiceStatus.OK) {
				    	for (var i = 0; i < results.length; i++) {
							var infoContent = '<div style="width:200px;height:30px">Prostovoljno gasilsko društvo<div>';
							var marker = new google.maps.Marker({
						    	map: globalneSpremenljivke.map,
						    	position: results[i].geometry.location,
								icon: 'map_icons/firemen.png'
						  	});
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, infoContent){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(infoContent);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, infoContent));
						  	// doda marker v seznam
						  	markersGasilci.push(marker);
				    	}
				    	var mcOptions = {
							styles: gasilciMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerGasilciCluster = new MarkerClusterer(globalneSpremenljivke.map, markersGasilci, mcOptions);	
				  	}
			  	});			  		
			}
			else {
				if (markersGasilci.length > 0) {
					for (var i = 0; i < markersGasilci.length; i++) {
						// odstrani marker iz zemljevida
						markersGasilci[i].setMap(null);
					}
					// sprazne seznam 
					markersGasilci = [];
					markerGasilciCluster.clearMarkers();
				}
			}
			
			// prikaže poštne poslovalnice
			if (document.getElementById('postaBox').checked) {
				if (markersPosta.length > 0) {
					for (var i = 0; i < markersPosta.length; i++) {
						// odstrani marker iz zemljevida
						markersPosta[i].setMap(null);
					}
					// sprazne seznam 
					markersPosta = [];
					markerPostaCluster.clearMarkers();
				}
				// poizvedba
				var request = {
			    	location: new google.maps.LatLng(globalneSpremenljivke.latLngMesta.sirina, globalneSpremenljivke.latLngMesta.dolzina), 
			    	radius: '5000',
			    	query: 'postna poslovalnica'
			  	};
			  	service = new google.maps.places.PlacesService(globalneSpremenljivke.map);
			  	service.textSearch(request, function(results, status){
					if (status == google.maps.places.PlacesServiceStatus.OK) {
				    	for (var i = 0; i < results.length; i++) {
							var infoContent = '<div style="width:130px;height:30px">Poštna poslovalnica<div>';
							var marker = new google.maps.Marker({
						    	map: globalneSpremenljivke.map,
						    	position: results[i].geometry.location,
								icon: 'map_icons/postal.png'
						  	});
							// omogoči klik na marker
							google.maps.event.addListener(marker, 'click', (function(marker, infoContent){ 
								return function() {
									// nastavi vsebino okna
								   	globalneSpremenljivke.infowindowData.setContent(infoContent);
								   	// odpre okno
									globalneSpremenljivke.infowindowData.open(globalneSpremenljivke.map, marker);
								};
							})(marker, infoContent));
						  	// doda marker v seznam
						  	markersPosta.push(marker);
				    	}
				    	var mcOptions = {
							styles: postaMarkerStyles,
							maxZoom: 18
						};
						// omogoči grupiranje markerjev
						markerPostaCluster = new MarkerClusterer(globalneSpremenljivke.map, markersPosta, mcOptions);	
				  	}
			  	});			  		
			}
			else {
				if (markersPosta.length > 0) {
					for (var i = 0; i < markersPosta.length; i++) {
						// odstrani marker iz zemljevida
						markersPosta[i].setMap(null);
					}
					// sprazne seznam 
					markersPosta = [];
					markerPostaCluster.clearMarkers();
				}
			}
		}
		
		// doda nepremičnino med priljubljene
		function addToFavourites() {
			// zamenja gumba na popup-u			
			document.getElementById('idPriljubljeneOverlay').style.display = "none";
			document.getElementById('idOdstraniOverlay').style.display = "initial";

			var placeMarker = globalneSpremenljivke.markerjiNepremicnin[globalneSpremenljivke.currentMarker];
			
			// spremeni ikono markerja
			placeMarker.setIcon('map_icons/realestatePriljubljene.png');
			
			var cena = "/";
			if (globalneSpremenljivke.nepremicnina.cena != "") {
				cena = globalneSpremenljivke.nepremicnina.cena + " €";
			}
			var podtip = "/";
			if (globalneSpremenljivke.nepremicnina.podtip != "") {
				podtip = globalneSpremenljivke.nepremicnina.podtip; 
			}
			var contentNew = '<div id="infobox" style="width:360px;height:230px">' +
								'<div id="nazivNep_infobox">' + globalneSpremenljivke.nepremicnina.naziv + '</div>' +
								'<div id="picture-panel"><img id="nepPicture" alt="" src=' + placeMarker.slika + ' width="130" height="120"/></div>' + 		
								'<div id="properties-detail">' +
									'<table>' +
										'<tr><td id="nepNaslov"><b>Naslov:</b> ' + globalneSpremenljivke.nepremicnina.naslov + ' ' + globalneSpremenljivke.nepremicnina.kraj + '</td></tr>' +
										'<tr><td id="nepCena"><b>Cena:</b> ' + cena + '</td></tr>' +
										'<tr><td id="nepTip"><b>Tip:</b> ' + globalneSpremenljivke.nepremicnina.tip + '</td></tr>' +
										'<tr><td id="nepPodtip"><b>Podtip:</b> ' + podtip + '</td></tr>' +
										'<tr><td id="nepVelikost"><b>Velikost:</b> ' + globalneSpremenljivke.nepremicnina.velikost + ' m<sup>2</sup></td></tr>' +
									'</table>' +								
								'</div>' +
								'<div id="infobox-datumi">' +
									'<b>Dodano v iskalnik</b>: ' + globalneSpremenljivke.nepremicnina.dodano + ', <b>zadnji pregled</b>: ' + globalneSpremenljivke.nepremicnina.zadnjicPregledano +
								'</div>' +
								'<div id="infobox-buttons">' +
								'<input type="button" value="Podrobnosti" onclick="overlay()"> <input type="button" id="removeFavourite" value="Odstrani" onclick="removeFromFavourites()"> ' + 
								'</div>' +
							 '</div>';
		  	
		  	google.maps.event.addListener(placeMarker, 'click', function() { 	  			
		  		// prikaže okno
				globalneSpremenljivke.infowindow.open(globalneSpremenljivke.map, placeMarker);
		  		// nastavi vsebino okna
				globalneSpremenljivke.infowindow.setContent(contentNew);
								
				// začetni naslov za iskanje poti vpiše v začetno polje
		  		document.getElementById('start').value = globalneSpremenljivke.nepremicnina.naslov + " " + globalneSpremenljivke.nepremicnina.kraj;
		  		// zapomi si id markerja
		  		globalneSpremenljivke.currentMarker = placeMarker.id;
		  	});
		  	
			// doda spremenjen marker 
			globalneSpremenljivke.markerjiNepremicnin[globalneSpremenljivke.currentMarker] = placeMarker;
	
			var value = "action=dodajPriljubljene&imeUporabnika=" + globalneSpremenljivke.imeUporabnika + "&idPriljubljene=" + globalneSpremenljivke.nepremicnina.id + "&latitude=" + globalneSpremenljivke.nepremicnina.pasovnaSirina + "&longitude=" + globalneSpremenljivke.nepremicnina.pasovnaDolzina;
			// shrani id nepremicnine v tabelo priljubljene
			jQuery.ajax({
			     url: "napredeniskalniknepremicnin",
			     //type: "post",
			     data: value,
			     cache: false,
			     success: function(data) {
			     
			     }
		    });	
			
			// doda nepremičnino v dropdown Priljubljene
			var opt = document.createElement("option");
	        document.getElementById("realestateFavourites").options.add(opt);
	        opt.text = globalneSpremenljivke.nepremicnina.naziv;
	        opt.value = globalneSpremenljivke.nepremicnina.naziv;
			// zapre infowindow
			globalneSpremenljivke.infowindow.close();
			// ponovno odpre infowindow
			globalneSpremenljivke.infowindow.open(globalneSpremenljivke.map, placeMarker);
	  		// nastavi vsebino okna
			globalneSpremenljivke.infowindow.setContent(contentNew);		
	  		
			// shrani marker v seznam priljubljenih			
			var priljubljen = {
				id: globalneSpremenljivke.nepremicnina.id,	
				naziv: globalneSpremenljivke.nepremicnina.naziv,
				markerNepremicnine: placeMarker,
				idMarkerja: globalneSpremenljivke.currentMarker 
			}
			globalneSpremenljivke.priljubljene.push(priljubljen);			
		}		
		
		// odstrani iz seznama priljubljenih
		function removeFromFavourites() {
			// zamenja gumba na popup-u
			document.getElementById('idOdstraniOverlay').style.display = "none";
			document.getElementById('idPriljubljeneOverlay').style.display = "initial";
			
			for (var i = 0; i < globalneSpremenljivke.priljubljene.length; i++) {
				if (globalneSpremenljivke.priljubljene[i].id == globalneSpremenljivke.nepremicnina.id) {
					//var placeMarker = globalneSpremenljivke.markerjiNepremicnin[globalneSpremenljivke.currentMarker];
					var placeMarker = globalneSpremenljivke.priljubljene[i].markerNepremicnine;
					var idMarkerjaPriljubljene = i;
				}
			}
			
		  	for (var i = 0; i < globalneSpremenljivke.markerjiNepremicnin.length; i++) {
		  		if (globalneSpremenljivke.markerjiNepremicnin[i].idNepremicnine == globalneSpremenljivke.nepremicnina.id) {
		  			// spremeni ikono markerja
					placeMarker.setIcon('map_icons/realestate.png');
					
					var cena = "/";
					if (globalneSpremenljivke.nepremicnina.cena != "") {
						cena = globalneSpremenljivke.nepremicnina.cena + " €";
					}
					var podtip = "/";
					if (globalneSpremenljivke.nepremicnina.podtip != "") {
						podtip = globalneSpremenljivke.nepremicnina.podtip; 
					}
					var contentNew = '<div id="infobox" style="width:360px;height:220px">' +
										'<div id="nazivNep_infobox">' + globalneSpremenljivke.nepremicnina.naziv + '</div>' +
										'<div id="picture-panel"><img id="nepPicture" alt="" src=' + placeMarker.slika + ' width="130" height="120"/></div>' + 		
										'<div id="properties-detail">' +
											'<table>' +
												'<tr><td id="nepNaslov"><b>Naslov:</b> ' + globalneSpremenljivke.nepremicnina.naslov + ' ' + globalneSpremenljivke.nepremicnina.kraj + '</td></tr>' +
												'<tr><td id="nepCena"><b>Cena:</b> ' + cena + '</td></tr>' +
												'<tr><td id="nepTip"><b>Tip:</b> ' + globalneSpremenljivke.nepremicnina.tip + '</td></tr>' +
												'<tr><td id="nepPodtip"><b>Podtip:</b> ' + podtip + '</td></tr>' +
												'<tr><td id="nepVelikost"><b>Velikost:</b> ' + globalneSpremenljivke.nepremicnina.velikost + ' m<sup>2</sup></td></tr>' +
											'</table>' +								
										'</div>' +
										'<div id="infobox-datumi">' +
											'<b>Dodano v iskalnik</b>: ' + globalneSpremenljivke.nepremicnina.dodano + ', <b>zadnji pregled</b>: ' + globalneSpremenljivke.nepremicnina.zadnjicPregledano +
										'</div>' +
										'<div id="infobox-buttons">' +
										'<input type="button" value="Podrobnosti" onclick="overlay()"> <input type="button" id="addFavourite" value="Priljubljene" onclick="addToFavourites()">' + 
										'</div>' +
									 '</div>';			
				  	
				  	google.maps.event.addListener(placeMarker, 'click', function() { 	  			
				  		// prikaže okno
						globalneSpremenljivke.infowindow.open(globalneSpremenljivke.map, placeMarker);
				  		// nastavi vsebino okna
						globalneSpremenljivke.infowindow.setContent(contentNew);
										
						// začetni naslov za iskanje poti vpiše v začetno polje
				  		document.getElementById('start').value = globalneSpremenljivke.nepremicnina.naslov + " " + globalneSpremenljivke.nepremicnina.kraj;
				  		// zapomi si id markerja
				  		globalneSpremenljivke.currentMarker = placeMarker.id;
				  	});
				  	
		  			// doda spremenjen marker 
					globalneSpremenljivke.markerjiNepremicnin[globalneSpremenljivke.currentMarker] = placeMarker;			
					// zapre infowindow
					globalneSpremenljivke.infowindow.close();
					// ponovno odpre infowindow
					globalneSpremenljivke.infowindow.open(globalneSpremenljivke.map, placeMarker);
			  		// nastavi vsebino okna
					globalneSpremenljivke.infowindow.setContent(contentNew);
			  		
		  			break;
		  		}
		  	}
		  	
		  	// zbriše marker iz zemljevida, če ni med trenutnimi zadetki
		  	if (i == globalneSpremenljivke.markerjiNepremicnin.length) {
		  		placeMarker.setMap(null);
		  	}
			
			var value = "action=odstraniPriljubljene&imeUporabnika=" + globalneSpremenljivke.imeUporabnika + "&idPriljubljene=" + globalneSpremenljivke.nepremicnina.id;
			// odstrani nepremicnine iz tabele priljubljene
			jQuery.ajax({
			     url: "napredeniskalniknepremicnin",
			     //type: "post",
			     data: value,
			     cache: false,
			     success: function(data) {
			     
			     }
		    });	
			
			// odstrani nepremičnino iz dropdown-a Priljubljene
			var selectFavourites = document.getElementById("realestateFavourites");
			for (var i = 0; i < selectFavourites.length;  i++) {
				if (selectFavourites.options[i].value == globalneSpremenljivke.nepremicnina.naziv) {
					selectFavourites.remove(i);
			   		break;
			   	}
			}
			
			// ostrani marker iz seznama priljubljenih
			globalneSpremenljivke.priljubljene.splice(idMarkerjaPriljubljene, 1);
		}
			
		// s klikom na naziv nepremicnine v dropdoen-u priljubljene odpre popup okno markerja
		function openFavouriteMarker() {			
			var priljubljeneMarker = document.getElementById("realestateFavourites").value;
			
			// poišče marker v seznamu priljubljenih
			for (var i = 0; i < globalneSpremenljivke.priljubljene.length; i++) {				
				if (globalneSpremenljivke.priljubljene[i].naziv == priljubljeneMarker) {					
					// zapre infowindow
					globalneSpremenljivke.infowindow.close();
					// ponovno odpre infowindow
					google.maps.event.trigger(globalneSpremenljivke.priljubljene[i].markerNepremicnine,"click");					
					globalneSpremenljivke.currentMarker = globalneSpremenljivke.priljubljene[i].idMarkerja;
					break;
				}
			}			
		}	
		
		
		
		
		
		
		
		
		
		// pridobi slke nepremicnine
		function getSlikeNepremicnine(idNepremicnine) {
			var slike = new Array();
			jQuery.ajax({
		    	url: 'https://spreadsheets.google.com/feeds/list/1r8riC_DLyeH4JOjzNrmDDGQxY_uh3Mfu1ku9y4w4V5Q/1484405031/public/values?alt=json&sq=idnepremicnine="' + idNepremicnine + '"',
		    	dataType: 'json',
		    	success: function(json) {
		    		if (!jQuery.isEmptyObject(json.feed.entry)) {
			    		for (var i = 0; i < json.feed.entry.length; i++) {
			    			slike.push(json.feed.entry[i].gsx$urlslike.$t);		    			
			    		}
		    		}
		        },
			    async:false
			});
						
			return slike;
		}
		
		// pridobi dodatne lastnosti nepremicnin
		function getDodatneLastnostiNepremicnine(idNepremicnine) {
			var dodatneLastnosti = new Array();
			jQuery.ajax({
		    	url: 'https://spreadsheets.google.com/feeds/list/12uxvD5_SvFaKkZT9yPYcGN_qL2HZwGibOPnUkxuk5hM/492720119/public/values?alt=json&sq=idnepremicnine="' + idNepremicnine + '"',
		    	dataType: 'json',
		    	success: function(json) {
		    		if (!jQuery.isEmptyObject(json.feed.entry)) {
			    		for (var i = 0; i < json.feed.entry.length; i++) {
			    			dodatneLastnosti.push(json.feed.entry[i].gsx$lastnost.$t);
			    		}
		    		}
		        },
			    async:false
			});
			
			return dodatneLastnosti;
		}
		
		// prikaže popup okno za pregled podrobnih podatkov o nepremičnini		
		function overlay() {
			var el = document.getElementById("overlay");
			var visibility = (el.style.visibility == "visible") ? "hidden" : "visible"
	    	el.style.visibility = visibility;
			if (visibility == "visible") {
				// naziv
				document.getElementById('naziv_nepremičnine').innerHTML = globalneSpremenljivke.nepremicnina.naziv;
				
				// gre čez seznam priljubljenih
				var selectFavourites = document.getElementById("realestateFavourites");
				var isFavourite = false;
				for (var i = 0; i < selectFavourites.length;  i++) {
					if (selectFavourites.options[i].value == globalneSpremenljivke.nepremicnina.naziv) {
						document.getElementById('idPriljubljeneOverlay').style.display = "none";
						document.getElementById('idOdstraniOverlay').style.display = "initial";
						isFavourite = true;
						break;
				   	}
				}
				if (!isFavourite) {
					document.getElementById('idOdstraniOverlay').style.display = "none";
					document.getElementById('idPriljubljeneOverlay').style.display = "initial";
				}
								
				// tab Opis
				document.getElementById('opis_nepremicnine').innerHTML = globalneSpremenljivke.nepremicnina.opis;
								
				// tab Slike
				globalneSpremenljivke.slikeNepremicnine = [];
    		 	globalneSpremenljivke.slikeNepremicnine = getSlikeNepremicnine(globalneSpremenljivke.nepremicnina.id);
    		 	document.getElementById("slike").src = globalneSpremenljivke.slikeNepremicnine[0];	
    		 	    		 	
				// tab Dodatno
				var cena = "Cena: ";
				cena = cena.bold();
				if (globalneSpremenljivke.nepremicnina.cena == "") {
					cena = cena + "/";
				}
				else {
					cena = cena + globalneSpremenljivke.nepremicnina.cena + " €"
				}
				document.getElementById('dodatno_cena').innerHTML = cena;
				var ponudba = "Vrsta ponudbe: ";
				ponudba = ponudba.bold();
				document.getElementById('dodatno_ponudba').innerHTML = ponudba + globalneSpremenljivke.nepremicnina.vrstaPonudbe;
				var leto = "Leto izgradnje: ";
				leto = leto.bold();
				if (globalneSpremenljivke.nepremicnina.letoIzgradnje == "") {
					leto = leto + "/";
				}
				else {
					leto = leto + globalneSpremenljivke.nepremicnina.letoIzgradnje;
				}
				document.getElementById('dodatno_leto').innerHTML = leto; 
				var regija = "Regija: ";
				regija = regija.bold();
				if (globalneSpremenljivke.nepremicnina.regija == "") {
					regija = regija + "/";
				}
				else {
					regija = regija + globalneSpremenljivke.nepremicnina.regija;
				}
				document.getElementById('regija').innerHTML = regija; 
				var predel = "Predel: ";
				predel = predel.bold();
				if (globalneSpremenljivke.nepremicnina.predel == "") {
					predel = predel + "/";
				}
				else {
					predel = predel + globalneSpremenljivke.nepremicnina.predel;
				}
				document.getElementById('predel').innerHTML = predel;
				
				var dodatno = "Dodatno: ";
				dodatno = dodatno.bold();
				document.getElementById('dodatno').innerHTML = dodatno;
				// seznam dodatnih lastnosti
				var dodatneLastnosti = getDodatneLastnostiNepremicnine(globalneSpremenljivke.nepremicnina.id); 
				document.getElementById("listDodatno").innerHTML = "";
				for (var i = 0; i < dodatneLastnosti.length; i++) {
					var liNode = document.createElement("LI");
					var spanNode = document.createElement("span");
					var textnode = document.createTextNode(dodatneLastnosti[i]);
					spanNode.appendChild(textnode);
					liNode.appendChild(spanNode);
					document.getElementById("listDodatno").appendChild(liNode);					 
				}
			}
		}
		
		// odpre originalen oglas v novem zavihku
		function openInNewTab() {
			var win = window.open(globalneSpremenljivke.nepremicnina.url, '_blank');
			win.focus();						
		}
		
		window.onload = function() {
	    	// pridobi tab container
	    	var container = document.getElementById("tabContainer");
	    	// nastavi trenutni tab
	    	var navitem = container.querySelector(".tabs ul li");
	    	// zapomni si na katerem tabu smo
	    	var ident = navitem.id.split("_")[1];
	    	   
	    	navitem.parentNode.setAttribute("data-current",ident);
	    	// prvemu tabo nastavi class activetabheader 
	    	navitem.setAttribute("class","tabActiveHeader");

	    	// skrije vsebino tab-ov, ki jih ne potrebuje
	    	var pages = container.querySelectorAll(".tabPage");
	    	for (var i = 1; i < pages.length; i++) {
	    		pages[i].style.display = "none";
	    	}

	    	// doda klik event na tab-e (da prikaže vsebino tab-a)
	    	var tabs = container.querySelectorAll(".tabs ul li");
	    	for (var i = 0; i < tabs.length; i++) {
	    	    tabs[i].onclick = displayPage;
	    	}
	    }
		
	    	    	
    	// klik na enega izmed tab-ov, prikaže vsebino tab-a
    	function displayPage() {
    		var trenutni = this.parentNode.getAttribute("data-current");
    	  	// odstrani class activetabheader neaktivnemu tab-u in skrije staro vsebino
    	 	document.getElementById("tabHeader_" + trenutni).removeAttribute("class");
    	  	document.getElementById("tabPage_" + trenutni).style.display = "none";

    	  	var ident = this.id.split("_")[1];
    	  	// da class activetabheader novemu aktivnemu tab-u in prikaže vsebino
    	  	this.setAttribute("class", "tabActiveHeader");
    	  	document.getElementById("tabPage_" + ident).style.display="block";
    	  	this.parentNode.setAttribute("data-current",ident);
    	  	
    	  	// za tab Slike omogoči pregled slik
    	  	if (ident == "2") {
   		      	var imageIndex = 0;
   		     
   		      	document.getElementById("previous_button").onclick = goToPrevious;
   		      	document.getElementById("next_button").onclick = goToNext;
   		      	document.getElementById("slike").src = globalneSpremenljivke.slikeNepremicnine[0];
   		   
	   		   	function goToPrevious(){
		   	    	if (imageIndex == 0) {
		   	    		imageIndex = globalneSpremenljivke.slikeNepremicnine.length;
			   	    }
			   	    imageIndex--;
		   	    	document.getElementById("slike").src = globalneSpremenljivke.slikeNepremicnine[imageIndex];
		   	    	return false;
				}
	   	   	
		   	    function goToNext(){
		   	    	imageIndex++;
		   	    	if (imageIndex == globalneSpremenljivke.slikeNepremicnine.length) {
		   	    		imageIndex = 0;
		   	    	}
		   	    	document.getElementById("slike").src = globalneSpremenljivke.slikeNepremicnine[imageIndex];
		   	    	return false;
		   	    }   		      
    		}
    	  	
    	}
		
	</script>
	
</head>
<body>

<%  
UserService userService = UserServiceFactory.getUserService();
User uporabnik = userService.getCurrentUser();

String thisURL = request.getRequestURI();
response.setContentType("text/html");
if (uporabnik == null) {
	out.println("<fieldset style='width:220px; height:120px; margin:150px auto;'>" + 
				"<p style='font-size: 15pt; padding:25px 0 0 30px'>Potrebna je prijava.</p> <br><a style='padding:0 0 0 80px' href=\"" + userService.createLoginURL(thisURL) + "\"><button>Prijava</button></a></fieldset>");	

} else { %>
	<div id="container">		
    	<div id="header">
    		<table style="width:100%">
    			<tr>
			        <td>
			        	<div id="favourites" >
							<img alt="" src="star.png" width="10" height="10" />
			   				<select name="priljubljene_nepremicnine" id="realestateFavourites" onchange="openFavouriteMarker()" style="width:375px">
								<option value="0">-- Priljubljene --</option>
							</select>
			   			</div>
			        </td>   
			        <td style="">		        	
			        	<% out.print("<div id='userdata'>" + uporabnik.getEmail() +  " &nbsp;&nbsp <a href=\"" +  userService.createLogoutURL(thisURL) + "\"><button>Odjava</button></a></div>"); %>
			        </td>      
			    </tr>
    		
    		</table>
    			   
		</div>
		<div id="leftPanel">
			<div id="leftPanelUp">
				<div id="steviloZadetkov" style="height:27px; padding:3px 0 0 0; margin:0; font-size:13pt;">
					<div style="font-size:13pt; color:#303030; text-decoration:underline; width:125px; float:left">Število zadetkov: </div>	<div id="steviloZadetkovStevilka" style="float:left"></div>				
				</div>				
				
    			<p style="font-size:13pt; color:#303030; text-decoration:underline;">Parametri iskanja</p>
    			<table>
    				<tr>
				        <td align="right"><b>*</b>Mesto:</td>   
				        <td> 	        	
				        	<select name="mesto" id="idMesto"> 
				        		<option value="0">-izberi-</option>	  
				        		<%  List<Kraj> kraji = Data.getKraji();
				        			for (Kraj k : kraji) {
				        				out.println("<option value=" + "\"" + k.getNaziv() + "\"" + ">" + k.getNaziv() + "</option>");
				        			}
				        		%>		   			
				            </select>
				        </td> 
				    </tr>
    				<tr>
				        <td align="right"><b>*</b>Tip nepremičnine:</td>   
				        <td>		        	
				        	<select name="tip" id="idTip" onChange="dodajOpcijeParametrov()">
				        		<option value="0">-izberi-</option>
				        		<option value="Stanovanja">Stanovanje</option>
				               	<option value="Hiše">Hiša</option>
				               	<option value="Počitniški objekt">Počitniški objekt</option>
				              	<option value="Posesti">Posest</option>
				              	<option value="Garaže">Garaža</option>
				              	<option value="Poslovni prostori">Poslovni prostor</option>	    			
				            </select>
				        </td>      
				    </tr>				
				    <tr>
				    	<td align="right">Podtip nepremičnine:</td>   
				        <td>		        	
				        	<select name="podtip" id="idPodtip" disabled="disabled">
				        		<option value="0">-izberi-</option>
				            </select>
				        </td>      
				    </tr>
				</table>
				Obseg cene: od <input type="text" id="idCenaOd" style="width: 100px"> do <input type="text" id="idCenaDo" style="width: 100px"> EUR
				<table style="padding-left:32px;">
					<tr>
				        <td align="right">Vrsta ponudbe:</td>   
				        <td>		        	
				        	<select name="ponudba" id="idPonudba">
				        		<option value="0">-izberi-</option>
				        		<% for (int i = 0; i < vrstaPonudbe.length; i++) {
			        		    	   out.println("<option value=" + "\"" + vrstaPonudbe[i] + "\"" + ">" + vrstaPonudbe[i] + "</option>");	
				        		   }
				        		%>		    			
				            </select>
				        </td>      
				    </tr>
				    <tr>
				        <td align="right">Velikost (m<sup>2</sup>):</td>   
				        <td>		        	
				        	<select name="velikost" id="idVelikost" disabled="disabled">
				        		<option value="0">-izberi-</option>
				            </select>
				        </td>      
				    </tr>				 
				    <tr>
				        <td align="right">Leto izgradnje:</td>   
				        <td>		        	
				        	<select name="leto" id="idLetoIzgradnje" >
				        		<option value="0">-izberi-</option>	
				        		<% for (int i = 0; i < letoIzgradnjeSeznam.length; i++) {
				        		       out.println("<option value=" + "\"" + letoIzgradnjeValue[i] + "\"" + ">" + letoIzgradnjeSeznam[i] + "</option>");	
					        	   }
					        	%>			        		    			
				            </select>
				        </td>      
				   	</tr>    				
    			</table>
    			Oddaljenost od: <br>
    			<table>
    				<tr>
    					<td style="float:left;padding-top:4px"><input type="checkbox" id="vrtciBox" value=""></td>
    					<td style="float:right;">vrtca <input type="text" id="vrtciText" style="width:100px;"> </td>
    					<td> do <select name="vrtec" id="idVrtec">
			        		        <option value="null">-izberi-</option>
			               	        <% for (int i = 0; i < oddaljenost.length; i++) {
			        		               out.println("<option value=" + "\"" + oddaljenost[i] + "\"" + ">" + oddaljenost[i] + "</option>");	
				        	           }
				        	         %>	
			           	        </select> km <br>
    					</td>
    				</tr>
    				<tr>
    					<td style="float:left;padding-top:4px"><input type="checkbox" id="osnovnaSolaBox" value=""></td>
    					<td style="float:right;">osnovne šole <input type="text" id="osnovnaSolaText" style="width:100px;"> </td>
    					<td> do <select name="osnovnaSola" id="idOsnovnaSola">
			        		        <option value="null">-izberi-</option>
			               	        <% for (int i = 0; i < oddaljenost.length; i++) {
			        		               out.println("<option value=" + "\"" + oddaljenost[i] + "\"" + ">" + oddaljenost[i] + "</option>");	
				        	           }
				        	         %>	
			           	        </select> km <br>
    					</td>
    				</tr>
    				<tr>
    					<td style="float:left;padding-top:4px"><input type="checkbox" id="srednjaSolaBox" value=""></td>
    					<td style="float:right;">srednje šole <input type="text" id="srednjaSolaText" style="width:100px;"> </td>
    					<td> do <select name="srednjaSola" id="idSrednjaSola">
			        		        <option value="null">-izberi-</option>
			               	        <% for (int i = 0; i < oddaljenost.length; i++) {
			        		               out.println("<option value=" + "\"" + oddaljenost[i] + "\"" + ">" + oddaljenost[i] + "</option>");	
				        	           }
				        	         %>	
			           	        </select> km <br>
    					</td>
    				</tr>
    				<tr>
    					<td style="float:left;padding-top:4px"><input type="checkbox" id="visokosolskiZavodBox" value=""></td>
    					<td style="float:right;">visokošolskega zavoda <input type="text" id="visokosolskiZavodText" style="width:100px;"> </td>
    					<td> do <select name="visokosolskiZavod" id="idVisokosolskiZavod">
			        		        <option value="null">-izberi-</option>
			               	        <% for (int i = 0; i < oddaljenost.length; i++) {
			        		               out.println("<option value=" + "\"" + oddaljenost[i] + "\"" + ">" + oddaljenost[i] + "</option>");	
				        	           }
				        	         %>	
			           	        </select> km <br>
    					</td>
    				</tr>
    				<tr>
    					<td style="float:left;padding-top:4px"><input type="checkbox" id="zdravstvoBox" value=""></td>
    					<td style="float:right;">zdravstva <input type="text" id="zdravstvoText" style="width:100px;"> </td>
    					<td> do <select name="zdravstvo" id="idZdravstvo">
			        		        <option value="null">-izberi-</option>
			               	        <% for (int i = 0; i < oddaljenost.length; i++) {
			        		               out.println("<option value=" + "\"" + oddaljenost[i] + "\"" + ">" + oddaljenost[i] + "</option>");	
				        	           }
				        	         %>	
			           	        </select> km <br>
    					</td>
    				</tr>
    				<tr>
    					<td style="float:left;padding-top:4px"><input type="checkbox" id="lekarnaBox" value=""></td>
    					<td style="float:right;">lekarne <input type="text" id="lekarnaText" style="width:100px;"> </td>
    					<td> do <select name="lekarna" id="idLekarna">
			        		        <option value="null">-izberi-</option>
			               	        <% for (int i = 0; i < oddaljenost.length; i++) {
			        		               out.println("<option value=" + "\"" + oddaljenost[i] + "\"" + ">" + oddaljenost[i] + "</option>");	
				        	           }
				        	         %>	
			           	        </select> km <br>
    					</td>
    				</tr>
    			</table>
    			<table>
    				<tr>
    					<td style="float:left;padding-top:3px"><input type="checkbox" id="javniPrometBox" value=""></td>    					
    					<td style="float:right;">javnega prometa do <select name="javniPromet" id="idJavniPromet">
							        		       <option value="null">-izberi-</option>
							               	       <% for (int i = 0; i < oddaljenost.length; i++) {
							        		              out.println("<option value=" + "\"" + oddaljenost[i] + "\"" + ">" + oddaljenost[i] + "</option>");	
								        	          }
								        	       %>	
							           	       </select> km <br>
    					</td>
    				</tr>
    				<tr>
    					<td style="float:left;padding-top:3px"><input type="checkbox" id="gasilciBox" value=""></td>    					
    					<td style="float:right;">gasilske postaje do <select name="gasilci" id="idGasilci">
							        		       <option value="null">-izberi-</option>
							               	       <% for (int i = 0; i < oddaljenost.length; i++) {
							        		              out.println("<option value=" + "\"" + oddaljenost[i] + "\"" + ">" + oddaljenost[i] + "</option>");	
								        	          }
								        	       %>	
							           	       </select> km <br>
    					</td>
    				</tr>
    				<tr>
    					<td style="float:left;padding-top:3px"><input type="checkbox" id="postaBox" value=""></td>    					
    					<td style="float:right;">poštne poslovalnice do <select name="posta" id="idPosta">
							        		       <option value="null">-izberi-</option>
							               	       <% for (int i = 0; i < oddaljenost.length; i++) {
							        		              out.println("<option value=" + "\"" + oddaljenost[i] + "\"" + ">" + oddaljenost[i] + "</option>");	
								        	          }
								        	       %>	
							           	       </select> km <br>
    					</td>
    				</tr>
    			</table>
    			      
    			<input type="button" value="Prikaži" onclick="filtrirajNepremicnine()" style="margin: 5px 0 0 23px"> <input type="reset" onclick="resetParametri()" style="margin: 5px 0 0 0"> 	    		
	    	 			
			</div>
			<div id="leftPanelDown">				
				<div id="direction-parameters" style="padding-left: 7px;">
					<p style="font-size:13pt; color:#303030; text-decoration:underline;">Prikaz poti</p>
					<table>
						<tr>
							<td align="right">Začetek:</td>
							<td><input type="text" id="start" style="width:270px;"></td>
						</tr>
						<tr>
							<td align="right">Konec:</td>
							<td><input type="text" id="end" style="width:270px;"></td>
						</tr>
					</table>
					&nbsp;&nbsp;&nbsp; Način: <select id="mode" >
									      <option value="DRIVING">Avto</option>
									      <option value="WALKING">Peš</option>
									      <option value="BICYCLING">Kolo</option>
									      <option value="TRANSIT">Javni prevoz</option>
									    </select>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" value="Prikaži" onclick="calcRoute();" > <input type="button" value="Reset" onclick="clearRoute();" > 
				</div>			    
	    		<div id="directions-panel"></div>
	    	</div>
	    </div>
	    <div id="map-canvas"></div>    
    </div>       
    
    <div id="overlay">
     	<div id="overlay-panel">
     		<div id="overlay-header">
     			<div id="naziv_nepremičnine" style="width:570px;float:left;padding-top:10px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;font-size:14pt;font-style:oblique;"></div>
     			<div style="float:left;padding-top:7px;margin-bottom:5px;"><input type="button" id="idPriljubljeneOverlay" value="Priljubljene" onclick="addToFavourites()" style=""> <input type="button" id="idOdstraniOverlay" value="Odstrani" onclick="removeFromFavourites()" style=""></div> <a class="close_button" id="close_button" onclick='overlay()'></a>
     		</div>       	
     		<div id="tabContainer">
   				<div class="tabs">
   					<ul>
 						<li id="tabHeader_1">Opis</li>
   						<li id="tabHeader_2">Slike</li>
   						<li id="tabHeader_3">Dodatno</li>
   					</ul>     				
   				</div>
   				<div class="tabsContent">
   					<div class="tabPage" id="tabPage_1">
   						<div id="opis_nepremicnine" style="height:370px;font-size:13pt;line-height:130%;padding-top:5px;overflow:auto;"></div> 
   						<input type="button" id="button_podrobnosti" value="Podrobnosti" onclick='openInNewTab()'>
   					</div>
   					<div class="tabPage" id="tabPage_2">   						
   						<div id="previousImage"><a class="previous_button" id="previous_button"></a></div>
   						<div id="nextImage"><a class="next_button" id="next_button" ></a></div>
   						<div id="imageContent" > <img alt="" src="" id="slike" style="max-height: 400px; max-width: 555px;" /> </div>
   					</div>
   					<div class="tabPage" id="tabPage_3">
   						<div id="dodatno_cena" style="padding-bottom:4px;"></div>
   						<div id="dodatno_ponudba" style="padding-bottom:4px;"></div>
   						<div id="dodatno_leto" style="padding-bottom:4px;"></div>
   						<div id="regija" style="padding-bottom:4px;"></div>
   						<div id="predel" style="padding-bottom:4px;"></div>
   						<div id="dodatno" style="padding-bottom:4px;"></div>
   						<div id="dodatno_seznam" style="padding-bottom:4px;overflow:auto;">
   							<ul id="listDodatno" class="listDodatnoClass"></ul>
   						</div>
  					</div>     				
   				</div>     			
   			</div>       	
     	</div>
	</div>	
	               
<%}%>
</body>
</html>
