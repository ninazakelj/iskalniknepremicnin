package com.cron.jobs;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

@SuppressWarnings("serial")
public class CronNepremicnine extends HttpServlet {
	
	private static SpreadsheetService service;
	private static String CLIENT_ID = "711446868423-nfjmhs7ftcjb2okck27o8t18765ofa3v.apps.googleusercontent.com";
	private static String CLIENT_SECRET = "BDq7BF5r51yhHJLUp8bHoRFe";	
	private static final String SERVICE_ACCOUNT_EMAIL = "711446868423-nfjmhs7ftcjb2okck27o8t18765ofa3v@developer.gserviceaccount.com";
	private static final String SERVICE_ACCOUNT_PKCS12_FILE_PATH = "WEB-INF/key.p12";
	// tabele 
    private static WorksheetEntry tabelaNepremicnin = null;
    private static WorksheetEntry tabelaLastnosti = null;
    private static WorksheetEntry tabelaSlik = null;
    private static WorksheetEntry tabelaZapisi = null;
    
    private static ListEntry rowZapisi;
    private static String zadnjiTip;
    private static String zadnjiPodtip;
    private static String zadnjaVrstaPonudbe;
    private static String zadnjaStran;
    private static String zadnjaNaStrani;
    
    private static String danasnjiDatum = null;
    
	private static WebClient webClient;
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	{
		cronJobNepremicnine();
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) {
		doGet(req, resp);
	}
	
	public static void cronJobNepremicnine()  {
		// pridobi dana�nji datum
		SimpleDateFormat formatDatuma = new SimpleDateFormat("dd.MM.yyyy");
		Calendar cal = Calendar.getInstance();
		danasnjiDatum = formatDatuma.format(cal.getTime());
		
		List<String> scopes = new ArrayList<String>();
		scopes.add("https://spreadsheets.google.com/feeds");
		
		HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
		JsonFactory JSON_FACTORY = new JacksonFactory();
		
		webClient = new WebClient();
		
		try {
			
			// omogo�i dostop do spreadsheet-a
			GoogleCredential credential = new GoogleCredential.Builder()
			.setTransport(HTTP_TRANSPORT)
			.setJsonFactory(JSON_FACTORY)
			.setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
			.setServiceAccountScopes(scopes)
			.setServiceAccountPrivateKeyFromP12File(new File(SERVICE_ACCOUNT_PKCS12_FILE_PATH))
			.build();
        	
        	service = new SpreadsheetService("com.cron.jobs");
        	service.setOAuth2Credentials(credential);	
	        // dolo�i timeout za service
	        service.setConnectTimeout(300000);
	        service.setReadTimeout(300000);
	        URL spreadsheetUrl = new URL("https://spreadsheets.google.com/feeds/spreadsheets/private/full");        
	        			
	        // pridobi vse spreadsheet-e
	        SpreadsheetFeed feed = service.getFeed(spreadsheetUrl, SpreadsheetFeed.class);
	        List<SpreadsheetEntry> spreadsheets = feed.getEntries();
		
	        SpreadsheetEntry sheetNepremicnine = null;
	        SpreadsheetEntry sheetDodatneLastnosti = null;
	        SpreadsheetEntry sheetSlike = null;
	        SpreadsheetEntry sheetZapisi = null;
	        // gre �ez vse spreadsheet-e in si zapomni tistega, ki se uporablja za nepremicnine
	        for (SpreadsheetEntry spreadsheet : spreadsheets) {
	        	if (spreadsheet.getTitle().getPlainText().equals("Nepremicnine_podatki")) {
	        		sheetNepremicnine = spreadsheet;
	        	}  
	        	else if (spreadsheet.getTitle().getPlainText().equals("Nepremicnine_dodatne_lastnosti")) {
	        		sheetDodatneLastnosti = spreadsheet;
	        	} 
	        	else if (spreadsheet.getTitle().getPlainText().equals("Nepremicnine_slike")) {
	        		sheetSlike = spreadsheet;
	        	}
	        	else if (spreadsheet.getTitle().getPlainText().equals("Baza")) {
	        		sheetZapisi = spreadsheet;
	        	}
	        }
	        
	        // pridobi worksheet za podatke o nepremi�nini
	        List<WorksheetEntry> worksheetsNepremicnine = sheetNepremicnine.getWorksheets();   
	        for (WorksheetEntry worksheet : worksheetsNepremicnine) {
	        	if (worksheet.getTitle().getPlainText().equals("Nepremicnine")) {
	        		tabelaNepremicnin = worksheet;	
	        		break;
	        	}
	        }  	
	        
	        // pridobi worksheet za dodatne lastnosti nepremi�nine
		    List<WorksheetEntry> worksheetsDodatneLastnosti = sheetDodatneLastnosti.getWorksheets();   
		    for (WorksheetEntry worksheet : worksheetsDodatneLastnosti) {
		    	if (worksheet.getTitle().getPlainText().equals("Dodatne_lastnosti")) {
		    		tabelaLastnosti = worksheet;
		    		break;
		    	}
		    }  
	        
		    // pridobi worksheet za slike nepremi�nine
		    List<WorksheetEntry> worksheetsSlike = sheetSlike.getWorksheets();   
		    for (WorksheetEntry worksheet : worksheetsSlike) {
		    	if (worksheet.getTitle().getPlainText().equals("Slike")) {
		    		tabelaSlik = worksheet;
		    		break;
		    	}
		    }  
	        
		    // pridobi worksheet o podatkiz zadnje obiskano
		    List<WorksheetEntry> worksheetsZapisi = sheetZapisi.getWorksheets();   
		    for (WorksheetEntry worksheet : worksheetsZapisi) {
		    	if (worksheet.getTitle().getPlainText().equals("Stanje_nepremicnine")) {
		    		tabelaZapisi = worksheet;
		    		break;
		    	}
		    }  
		    
		    
		    // pridobi nazadnje obiskano
	        URL listFeedUrlZapisi = tabelaZapisi.getListFeedUrl();
	        ListFeed listFeedZapisi = service.getFeed(listFeedUrlZapisi, ListFeed.class);
	        rowZapisi = listFeedZapisi.getEntries().get(0);
	        zadnjiTip = rowZapisi.getCustomElements().getValue("tip");
	        zadnjiPodtip = rowZapisi.getCustomElements().getValue("podtip");
	        zadnjaVrstaPonudbe = rowZapisi.getCustomElements().getValue("vrstaponudbe");
	        zadnjaStran = rowZapisi.getCustomElements().getValue("stran");
	        zadnjaNaStrani = rowZapisi.getCustomElements().getValue("nastrani");
		    
	        // nastavitve webclient-a
		    webClient.getOptions().setJavaScriptEnabled(false); // Enables/disables JavaScript support. By default, this property is enabled.
			webClient.getOptions().setThrowExceptionOnScriptError(false); // Changes the behavior of this webclient when a script error occurs.
			webClient.getOptions().setUseInsecureSSL(true); // If set to true, the client will accept connections to any host, regardless of whether they have valid certificates or not. This is especially useful when you are trying to connect to a server with expired or corrupt certificates.
			webClient.getOptions().setCssEnabled(false); // Enables/disables CSS support.
			
			// nepremi�nine
			HtmlPage pageNepremicnine = webClient.getPage("http://www.bolha.com/nepremicnine/");
			// pridobi stevilo tipov nepremicnin
			List<HtmlElement> seznamTipNepremicnine = (List<HtmlElement>) pageNepremicnine.getByXPath("//*[@id='categorySubmenu']/ul/li");
			
			String tipNepremicnine = "";
			String podtipNepremicnine = "";	
			for (int i = 2; i <= seznamTipNepremicnine.size(); i++) {
				HtmlElement elementTipNepremicnine = (HtmlElement) pageNepremicnine.getByXPath("//*[@id='categorySubmenu']/ul/li[" + i + "]/a").get(0);
				tipNepremicnine = elementTipNepremicnine.asText();
				// gre na zadnje obiskani tip nepremi�nine
				if (tipNepremicnine.equals(zadnjiTip)) {
					// gre na tip nepremicnine
					HtmlPage pageTipNepremicnine = elementTipNepremicnine.click();
					
					if (!tipNepremicnine.equals("Gara�e")) {
						// pridobi stevilo podtipov nepremicnin
						HtmlElement divPodtipNepremicnine = (HtmlElement) pageTipNepremicnine.getByXPath("//*[@id='facetSet']/div[1]/div").get(0);
						List<HtmlElement> seznamPodtipNepremicnine = divPodtipNepremicnine.getElementsByTagName("div");
						
						for (int j = 1; j <= seznamPodtipNepremicnine.size(); j++) {
							// gre na zadnje obiskani podtip nepremi�nine
							if (j == Integer.parseInt(zadnjiPodtip)) {
								HtmlElement elementPodtipNepremicnine = (HtmlElement) divPodtipNepremicnine.getByXPath("div[" + j + "]/a").get(0);
								podtipNepremicnine = elementPodtipNepremicnine.asText().replace(elementPodtipNepremicnine.getElementsByTagName("span").get(0).asText(), "");
								
								// gre na podip nepremicnine
								HtmlPage pagePodtipNepremicnine = elementPodtipNepremicnine.click();
								
								List<HtmlElement> leviMeni = (List<HtmlElement>) pagePodtipNepremicnine.getByXPath("//*[@id='facetSet']/div");
								for (HtmlElement elementVMeniju : leviMeni) {
									if (elementVMeniju.getElementsByTagName("h4").get(0).asText().equals("Vrsta ponudbe")) {
										// pridobi stevilo razli�nih vrst ponudb
										List<HtmlElement> seznamVrstaPonudbe = (List<HtmlElement>) elementVMeniju.getByXPath("div/div");
										for (int k = 1; k <= seznamVrstaPonudbe.size(); k++) {
											HtmlElement elementVrstaPonudbe = (HtmlElement) elementVMeniju.getByXPath("div/div[" + k + "]/a").get(0);
											String vrstaPonudbe = elementVrstaPonudbe.asText().replace(elementVrstaPonudbe.getElementsByTagName("span").get(0).asText(), "");
											
											// gre na zadnjo vrsto ponudbe
											if (vrstaPonudbe.equals(zadnjaVrstaPonudbe)) {
												if (vrstaPonudbe.equals("Prodam") || vrstaPonudbe.equals("Oddam") || vrstaPonudbe.equals("Podarim")) {
													// gre na vrsto ponudbe
													HtmlPage pageVrstaPonudbe = elementVrstaPonudbe.click();
													List<HtmlElement> seznamStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a");
													
													int stevilkaStrani = 1;
													if (seznamStrani.size() > 0) {
														boolean obstajaNaslednja = true;
														while (obstajaNaslednja) {
															if (stevilkaStrani == Integer.parseInt(zadnjaStran)) {
																// gre na posamezno nepremi�nino
																List<HtmlElement> nepremicnineNaStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='list']/div");
																preglejNepremicnineNaStrani(nepremicnineNaStrani, tipNepremicnine, podtipNepremicnine, vrstaPonudbe);
																
																// gre na naslednjo stran
																HtmlElement naslednjaStran = (HtmlElement) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a[" + seznamStrani.size() + "]").get(0);
																if (naslednjaStran.getAttribute("class").equals("forward")) {
																	pageVrstaPonudbe = naslednjaStran.click();
																	seznamStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a");
																	naslednjaStran = (HtmlElement) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a[" + seznamStrani.size() + "]").get(0);
																	
																	stevilkaStrani++;
																	zadnjaStran = Integer.toString(stevilkaStrani);
																	URL listFeedUrlZapisiStran = tabelaZapisi.getListFeedUrl();
															        ListFeed listFeedZapisiStran = service.getFeed(listFeedUrlZapisiStran, ListFeed.class);
															        rowZapisi = listFeedZapisiStran.getEntries().get(0);
																	rowZapisi.getCustomElements().setValueLocal("stran", zadnjaStran);
																	rowZapisi.update();
																}
																else {
																	obstajaNaslednja = false;
																}
															}
															else {
																// gre na naslednjo stran
																HtmlElement naslednjaStran = (HtmlElement) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a[" + seznamStrani.size() + "]").get(0);
																if (naslednjaStran.getAttribute("class").equals("forward")) {
																	// gre na naslednjo stran
																	pageVrstaPonudbe = naslednjaStran.click();
																	seznamStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a");
																	naslednjaStran = (HtmlElement) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a[" + seznamStrani.size() + "]").get(0);
																	
																	stevilkaStrani++;
																}
																else {
																	obstajaNaslednja = false;
																}
															}
														}	
														
														zadnjaStran = "1";
														URL listFeedUrlZapisiStran = tabelaZapisi.getListFeedUrl();
												        ListFeed listFeedZapisiStran = service.getFeed(listFeedUrlZapisiStran, ListFeed.class);
												        rowZapisi = listFeedZapisiStran.getEntries().get(0);
														rowZapisi.getCustomElements().setValueLocal("stran", zadnjaStran);
														rowZapisi.update();
													}
													else {
														// gre na posamezno nepremi�nino
														List<HtmlElement> nepremicnineNaStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='list']/div");
														preglejNepremicnineNaStrani(nepremicnineNaStrani, tipNepremicnine, podtipNepremicnine, vrstaPonudbe);
													}
												}
												
												URL listFeedUrlZapisiPonudba = tabelaZapisi.getListFeedUrl();
										        ListFeed listFeedZapisiPonudba = service.getFeed(listFeedUrlZapisiPonudba, ListFeed.class);
										        rowZapisi = listFeedZapisiPonudba.getEntries().get(0);
										        int naslednjaVrstaPonudbe = k + 1;
												if (naslednjaVrstaPonudbe <= seznamVrstaPonudbe.size()) {
													HtmlElement elVrstaPonudbe = (HtmlElement) elementVMeniju.getByXPath("div/div[" + naslednjaVrstaPonudbe + "]/a").get(0);
													zadnjaVrstaPonudbe = elVrstaPonudbe.asText().replace(elVrstaPonudbe.getElementsByTagName("span").get(0).asText(), "");
													rowZapisi.getCustomElements().setValueLocal("vrstaponudbe", zadnjaVrstaPonudbe);
													rowZapisi.update();
												}
												else {
													zadnjaVrstaPonudbe = "Prodam";
													rowZapisi.getCustomElements().setValueLocal("vrstaponudbe", zadnjaVrstaPonudbe);
													rowZapisi.update();
												}
											}
										}
										
										break;
									}	
								}
								
								URL listFeedUrlZapisiPodtip = tabelaZapisi.getListFeedUrl();
						        ListFeed listFeedZapisiPodtip = service.getFeed(listFeedUrlZapisiPodtip, ListFeed.class);
						        rowZapisi = listFeedZapisiPodtip.getEntries().get(0);
						        int naslednjiPodtip = j + 1;
								if (naslednjiPodtip <= seznamPodtipNepremicnine.size()) {
									zadnjiPodtip = Integer.toString(naslednjiPodtip);
									rowZapisi.getCustomElements().setValueLocal("podtip", zadnjiPodtip);
									rowZapisi.update();
								}
								else {
									zadnjiPodtip = "1";
									rowZapisi.getCustomElements().setValueLocal("podtip", zadnjiPodtip);
									rowZapisi.update();
								}
							}
						}
					}
					else {
						List<HtmlElement> leviMeni = (List<HtmlElement>) pageTipNepremicnine.getByXPath("//*[@id='facetSet']/div");
						for (HtmlElement elementVMeniju : leviMeni) {
							if (elementVMeniju.getElementsByTagName("h4").get(0).asText().equals("Vrsta ponudbe")) {
								// pridobi stevilo razli�nih vrst ponudb
								List<HtmlElement> seznamVrstaPonudbe = (List<HtmlElement>) elementVMeniju.getByXPath("div/div");
								for (int k = 1; k <= seznamVrstaPonudbe.size(); k++) {
									HtmlElement elementVrstaPonudbe = (HtmlElement) elementVMeniju.getByXPath("div/div[" + k + "]/a").get(0);
									String vrstaPonudbe = elementVrstaPonudbe.asText().replace(elementVrstaPonudbe.getElementsByTagName("span").get(0).asText(), "");
									
									// gre na zadnjo vrsto ponudbe
									if (vrstaPonudbe.equals(zadnjaVrstaPonudbe)) {
										if (vrstaPonudbe.equals("Prodam") || vrstaPonudbe.equals("Oddam") || vrstaPonudbe.equals("Podarim")) {
											// gre na vrsto ponudbe
											HtmlPage pageVrstaPonudbe = elementVrstaPonudbe.click();
											List<HtmlElement> seznamStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a");
											
											int stevilkaStrani = 1;
											if (seznamStrani.size() > 0) {
												boolean obstajaNaslednja = true;
												while (obstajaNaslednja) {
													if (stevilkaStrani == Integer.parseInt(zadnjaStran)) {
														// gre na posamezno nepremi�nino
														List<HtmlElement> nepremicnineNaStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='list']/div");
														preglejNepremicnineNaStrani(nepremicnineNaStrani, tipNepremicnine, podtipNepremicnine, vrstaPonudbe);
														
														// gre na naslednjo stran
														HtmlElement naslednjaStran = (HtmlElement) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a[" + seznamStrani.size() + "]").get(0);
														if (naslednjaStran.getAttribute("class").equals("forward")) {
															pageVrstaPonudbe = naslednjaStran.click();
															seznamStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a");
															naslednjaStran = (HtmlElement) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a[" + seznamStrani.size() + "]").get(0);
															
															stevilkaStrani++;
															zadnjaStran = Integer.toString(stevilkaStrani);
															URL listFeedUrlZapisiStran = tabelaZapisi.getListFeedUrl();
													        ListFeed listFeedZapisiStran = service.getFeed(listFeedUrlZapisiStran, ListFeed.class);
													        rowZapisi = listFeedZapisiStran.getEntries().get(0);
															rowZapisi.getCustomElements().setValueLocal("stran", zadnjaStran);
															rowZapisi.update();
														}
														else {
															obstajaNaslednja = false;
														}
													}
													else {
														// gre na naslednjo stran
														HtmlElement naslednjaStran = (HtmlElement) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a[" + seznamStrani.size() + "]").get(0);
														if (naslednjaStran.getAttribute("class").equals("forward")) {
															// gre na naslednjo stran
															pageVrstaPonudbe = naslednjaStran.click();
															seznamStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a");
															naslednjaStran = (HtmlElement) pageVrstaPonudbe.getByXPath("//*[@id='toolBox-bottom']/div/div/a[" + seznamStrani.size() + "]").get(0);
															stevilkaStrani++;
														}
														else {
															obstajaNaslednja = false;
														}
													}
												}	
												
												zadnjaStran = "1";
												URL listFeedUrlZapisiStran = tabelaZapisi.getListFeedUrl();
										        ListFeed listFeedZapisiStran = service.getFeed(listFeedUrlZapisiStran, ListFeed.class);
										        rowZapisi = listFeedZapisiStran.getEntries().get(0);
												rowZapisi.getCustomElements().setValueLocal("stran", zadnjaStran);
												rowZapisi.update();
											}
											else {
												// gre na posamezno nepremi�nino
												List<HtmlElement> nepremicnineNaStrani = (List<HtmlElement>) pageVrstaPonudbe.getByXPath("//*[@id='list']/div");
												preglejNepremicnineNaStrani(nepremicnineNaStrani, tipNepremicnine, podtipNepremicnine, vrstaPonudbe);
											}
										}
										
										URL listFeedUrlZapisiPonudba = tabelaZapisi.getListFeedUrl();
								        ListFeed listFeedZapisiPonudba = service.getFeed(listFeedUrlZapisiPonudba, ListFeed.class);
								        rowZapisi = listFeedZapisiPonudba.getEntries().get(0);
								        int naslednjaVrstaPonudbe = k + 1;
										if (naslednjaVrstaPonudbe <= seznamVrstaPonudbe.size()) {
											HtmlElement elVrstaPonudbe = (HtmlElement) elementVMeniju.getByXPath("div/div[" + naslednjaVrstaPonudbe + "]/a").get(0);
											zadnjaVrstaPonudbe = elVrstaPonudbe.asText().replace(elVrstaPonudbe.getElementsByTagName("span").get(0).asText(), "");
											rowZapisi.getCustomElements().setValueLocal("vrstaponudbe", zadnjaVrstaPonudbe);
											rowZapisi.update();
										}
										else {
											zadnjaVrstaPonudbe = "Prodam";
											rowZapisi.getCustomElements().setValueLocal("vrstaponudbe", zadnjaVrstaPonudbe);
											rowZapisi.update();
										}
									}
								}
								
								break;
							}
						}
					}
					
					URL listFeedUrlZapisiTip = tabelaZapisi.getListFeedUrl();
			        ListFeed listFeedZapisiTip = service.getFeed(listFeedUrlZapisiTip, ListFeed.class);
			        rowZapisi = listFeedZapisiTip.getEntries().get(0);
			        int naslednjiTip = i + 1;
					if (naslednjiTip <= seznamTipNepremicnine.size()) {
						
						HtmlElement elTipNepremicnine = (HtmlElement) pageNepremicnine.getByXPath("//*[@id='categorySubmenu']/ul/li[" + naslednjiTip + "]/a").get(0);
						zadnjiTip = elTipNepremicnine.asText();
						rowZapisi.getCustomElements().setValueLocal("tip", zadnjiTip);
						rowZapisi.update();
					}
					else {
						zadnjiTip = "Gara�e";
						rowZapisi.getCustomElements().setValueLocal("tip", zadnjiTip);
						rowZapisi.update();
					}
				}			
			}
			
		} catch (FailingHttpStatusCodeException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (AuthenticationException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {			
			e.printStackTrace();
		}
		
	}
	
	private static void preglejNepremicnineNaStrani(List<HtmlElement> nepremicnineNaStrani, String tipNepremicnine, String podtipNepremicnine, String vrstaPonudbe) {
		int stevilkaNepremNaStrani = 1;
		
		for (HtmlElement element : nepremicnineNaStrani) {
			if (element.getAttribute("class").equals("adGrid span-5")) {
				if (stevilkaNepremNaStrani == Integer.parseInt(zadnjaNaStrani)) {
					// preveri da ni nepremi�nina v tujini
					List<HtmlElement> elementiSeznamRegija = (List<HtmlElement>) element.getByXPath("div/div[3]/p/span");
					if (elementiSeznamRegija.size() > 0) {
						HtmlElement elementSeznamRegija = elementiSeznamRegija.get(0);
						if (!elementSeznamRegija.asText().equals("Tujina")) {
							getPodatkiNepremi�nine(element, tipNepremicnine, podtipNepremicnine, vrstaPonudbe);
						}
					}

					stevilkaNepremNaStrani++;
					zadnjaNaStrani = Integer.toString(stevilkaNepremNaStrani);
					URL listFeedUrlZapisiNaStrani = tabelaZapisi.getListFeedUrl();
			        ListFeed listFeedZapisiNaStrani;
					try {
						listFeedZapisiNaStrani = service.getFeed(listFeedUrlZapisiNaStrani, ListFeed.class);
						rowZapisi = listFeedZapisiNaStrani.getEntries().get(0);
						rowZapisi.getCustomElements().setValueLocal("nastrani", zadnjaNaStrani);
						rowZapisi.update();
					} catch (IOException e) {
						return;
					} catch (ServiceException e) {
						return;
					}
			        
				}
				else {
					stevilkaNepremNaStrani++;
				}
				
			}
		}
		
		zadnjaNaStrani = "1";
		URL listFeedUrlZapisiNaStrani = tabelaZapisi.getListFeedUrl();
        ListFeed listFeedZapisiNaStrani;
		try {
			listFeedZapisiNaStrani = service.getFeed(listFeedUrlZapisiNaStrani, ListFeed.class);
			rowZapisi = listFeedZapisiNaStrani.getEntries().get(0);
			rowZapisi.getCustomElements().setValueLocal("nastrani", zadnjaNaStrani);
			rowZapisi.update();
		} catch (IOException e) {
			return;
		} catch (ServiceException e) {
			return;
		}
        
	}
	
	private static void getPodatkiNepremi�nine(HtmlElement nepremNaStrani, String tip, String podtip, String vrstaPonudbe) {
		// url nepremi�nine
		HtmlElement elementUrlNepremicnine = (HtmlElement) nepremNaStrani.getByXPath("div/div[4]/h3/a").get(0); 
		String urlNepremicnine = "http://www.bolha.com" + elementUrlNepremicnine.getAttribute("href").split("\\?")[0];
		// id nepremi�nine
		String idNepremicnine = urlNepremicnine.replaceAll("[^a-zA-Z0-9]", "");
				
		try {
			URL listFeedUrlNeprem = tabelaNepremicnin.getListFeedUrl();
			ListQuery query = new ListQuery(listFeedUrlNeprem);
			query.setSpreadsheetQuery("id=" + idNepremicnine);
		    ListFeed listFeedNeprem = service.query(query, ListFeed.class);
			
			if (listFeedNeprem.getEntries().size() > 0) {
				// �e �e obstaja samo spremeni stolpec zadnji� pregledano
				ListEntry row = listFeedNeprem.getEntries().get(0);
				row.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
				row.getCustomElements().setValueLocal("status", "aktiven");
				row.update();
			}
			else {
				// gre na stran nepremi�nine
				HtmlPage pageNepremicnina = webClient.getPage(urlNepremicnine);
				HtmlElement oglasNepremicnine = (HtmlElement) pageNepremicnina.getByXPath("//*[@id='adDetail']/div[2]").get(0); 
				
				// naziv nepremi�nine
				String naziv = oglasNepremicnine.getElementsByTagName("h1").get(0).asText();	
				// cena nepremi�nine
				String cena = ""; 
				List<HtmlElement> elementCena = (List<HtmlElement>) oglasNepremicnine.getByXPath("div[1]/span");
				if (elementCena.size() > 0) {
					cena = elementCena.get(0).asText().split(" ")[0];
				}
				
				String regija = "", kraj = "", predel = "", velikost = "", letoIzgradnje = "";
				String naslov = "";
				String opis = "";
				String latitude = "", longitude = "";
				HtmlElement oglas = (HtmlElement) pageNepremicnina.getElementById("box-oglas-levo");				
				if (oglas != null) {
					opis = pageNepremicnina.getElementById("box-oglas-levo").asText();
					// podatki o regiji, kraju, predelu, velikosti, naselju, letu izgradnje
					List<HtmlTable> tabelaOglasPodatkiSeznam = (List<HtmlTable>) pageNepremicnina.getByXPath("//*[@id='box-oglas-levo']/table[1]"); 				
					if (tabelaOglasPodatkiSeznam.size() > 0) {
						HtmlTable tabelaOglasPodatki = tabelaOglasPodatkiSeznam.get(0);
						for (HtmlTableRow vrsticaOglasPodatki : tabelaOglasPodatki.getRows()) {
							if (vrsticaOglasPodatki.getCells().get(0).asText().equals("Regija:")) {
								regija = vrsticaOglasPodatki.getCells().get(1).asText();
							}
							else if (vrsticaOglasPodatki.getCells().get(0).asText().equals("Kraj:")) {
								kraj = vrsticaOglasPodatki.getCells().get(1).asText();
							}
							else if (vrsticaOglasPodatki.getCells().get(0).asText().equals("Predel:")) {
								predel = vrsticaOglasPodatki.getCells().get(1).asText();
							}
							else if (vrsticaOglasPodatki.getCells().get(0).asText().equals("Velikost (m2):")) {
								velikost = vrsticaOglasPodatki.getCells().get(1).asText();
							}
							else if (vrsticaOglasPodatki.getCells().get(0).asText().equals("Leto izgradnje:")) {
								letoIzgradnje = vrsticaOglasPodatki.getCells().get(1).asText();
							}
							// odstrani vrstico iz opisa
							opis = opis.replace(vrsticaOglasPodatki.asText(), "");
						}
					}
					
					// podatek o naslovu nepremi�nine					
					List<HtmlTable> tabelaOglasPodatkiOstaloSeznam = (List<HtmlTable>) pageNepremicnina.getByXPath("//*[@id='box-oglas-levo']/table[2]"); 
					if (tabelaOglasPodatkiOstaloSeznam.size() > 0) {
						HtmlTable tabelaOglasPodatkiOstalo = tabelaOglasPodatkiOstaloSeznam.get(0);
						for (HtmlTableRow vrsticaOglasPodatkiOstalo : tabelaOglasPodatkiOstalo.getRows()) {
							if (vrsticaOglasPodatkiOstalo.getCells().get(0).asText().equals("Naslov:")) {														
								naslov = vrsticaOglasPodatkiOstalo.getCells().get(1).asText();															
							}
							if (tip.equals("Gara�e")) {
								if (vrsticaOglasPodatkiOstalo.getCells().get(0).asText().equals("Leto izgradnje:")) {														
									letoIzgradnje = vrsticaOglasPodatkiOstalo.getCells().get(1).asText();	
								}
							}
							// odstrani vrstico iz opisa
							opis = opis.replace(vrsticaOglasPodatkiOstalo.asText(), "");
						}
					}
					
					// pasovna �irina in dol�ina naslova
					if (!naslov.equals("") || !predel.equals("")) {
						String[] latLng = geocoding(naslov + " " + kraj + " Slovenija");
						latitude = latLng[0];
						longitude = latLng[1];
					}
					
					// zbri�e lastnosti �e �e obstajajo
					URL listFeedUrlDodatneLastnosti = tabelaLastnosti.getListFeedUrl();
					ListQuery queryLastnosti = new ListQuery(listFeedUrlDodatneLastnosti);
					queryLastnosti.setSpreadsheetQuery("idnepremicnine=" + idNepremicnine);
				    ListFeed listFeedDodatneLastnosti = service.query(queryLastnosti, ListFeed.class);
					if (listFeedDodatneLastnosti.getEntries().size() > 0) {
						for (ListEntry row : listFeedDodatneLastnosti.getEntries()) {
							row.delete();
						}
					}
					// doda dodatne lastnosti
					List<HtmlElement> dodatneLastnosti = (List<HtmlElement>) pageNepremicnina.getElementById("box-oglas-levo").getElementsByTagName("ul");
					if (dodatneLastnosti.size() > 0) {
						for (HtmlElement elementLastnosti : dodatneLastnosti) {
							List<HtmlElement> lastnosti = elementLastnosti.getElementsByTagName("li");
							for (HtmlElement lastnost : lastnosti) {
								// doda lastnost v tabelo
								URL listFeedUrlLastnosti = tabelaLastnosti.getListFeedUrl();
						        ListEntry rowLastnost = new ListEntry();
						        rowLastnost.getCustomElements().setValueLocal("idnepremicnine", idNepremicnine);
						        rowLastnost.getCustomElements().setValueLocal("lastnost", lastnost.asText());
						        service.insert(listFeedUrlLastnosti, rowLastnost);
							}	
							// odstrani vse lastnosti iz opisa
							opis = opis.replace(elementLastnosti.asText(), "");
						}
					}
					
					// iz opisa odstrani vse naslove
					List<HtmlElement> naslovDodatniOpis = (List<HtmlElement>) pageNepremicnina.getElementById("box-oglas-levo").getElementsByTagName("strong");
					if (naslovDodatniOpis.size() > 0) {
						opis = opis.replace(naslovDodatniOpis.get(0).asText(), "");
					}
					List<HtmlElement> nasloviDodatneLastnosti = (List<HtmlElement>) pageNepremicnina.getElementById("box-oglas-levo").getElementsByTagName("b");
					for (HtmlElement naslovLastnosti : nasloviDodatneLastnosti) {
						opis = opis.replace(naslovLastnosti.asText(), "");
					}
					opis = opis.replace("\r","").replace("\n","");
					
				}
				
				// zbri�e slike �e �e obstajajo
				URL listFeedUrlSlike = tabelaSlik.getListFeedUrl();
				ListQuery querySlike = new ListQuery(listFeedUrlSlike);
				querySlike.setSpreadsheetQuery("idnepremicnine=" + idNepremicnine);
			    ListFeed listFeedSlike = service.query(querySlike, ListFeed.class);
				if (listFeedSlike.getEntries().size() > 0) {
					for (ListEntry row : listFeedSlike.getEntries()) {
						row.delete();
					}
				}
				
				// prikazna slika 
				HtmlElement elementPrikaznaSlika = (HtmlElement) pageNepremicnina.getElementById("gallery").getElementsByTagName("img").get(0); 
				String srcPrikaznaSlika = elementPrikaznaSlika.getAttribute("src");
				shraniSliko(srcPrikaznaSlika, idNepremicnine);
				
				// ostale slike na seznamu
				HtmlElement galerijaSlik = (HtmlElement) pageNepremicnina.getElementById("gal");
				if (galerijaSlik != null) {
					List<HtmlElement> seznamSlik = galerijaSlik.getElementsByTagName("img");
					for (HtmlElement slika : seznamSlik) {
						// url naslov slike
						String src = slika.getAttribute("src").replace("thumb", "image");
						shraniSliko(src, idNepremicnine);
					}
				}	
				
				// doda podatke o nepremi�nini v tabelo
				URL listFeedUrlNepremicnine = tabelaNepremicnin.getListFeedUrl();
		        ListEntry rowNepremicnine = new ListEntry();
		        rowNepremicnine.getCustomElements().setValueLocal("id", idNepremicnine);
		        rowNepremicnine.getCustomElements().setValueLocal("tip", tip);
		        rowNepremicnine.getCustomElements().setValueLocal("podtip", podtip);
		        rowNepremicnine.getCustomElements().setValueLocal("vrstaponudbe", vrstaPonudbe);
		        rowNepremicnine.getCustomElements().setValueLocal("url", urlNepremicnine);
		        rowNepremicnine.getCustomElements().setValueLocal("naziv", naziv);
		        rowNepremicnine.getCustomElements().setValueLocal("cena", cena);
		        rowNepremicnine.getCustomElements().setValueLocal("regija", regija);
		        rowNepremicnine.getCustomElements().setValueLocal("kraj", kraj);
		        rowNepremicnine.getCustomElements().setValueLocal("predel", predel);
		        rowNepremicnine.getCustomElements().setValueLocal("velikost", velikost);
		        rowNepremicnine.getCustomElements().setValueLocal("letoizgradnje", letoIzgradnje);
		        rowNepremicnine.getCustomElements().setValueLocal("naslov", naslov);
		        rowNepremicnine.getCustomElements().setValueLocal("opis", opis);
		        rowNepremicnine.getCustomElements().setValueLocal("latitude", latitude);
		        rowNepremicnine.getCustomElements().setValueLocal("longitude", longitude);
		        rowNepremicnine.getCustomElements().setValueLocal("naslovnaslika", srcPrikaznaSlika);
		        rowNepremicnine.getCustomElements().setValueLocal("dodano", danasnjiDatum);
		        rowNepremicnine.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
		        rowNepremicnine.getCustomElements().setValueLocal("status", "aktiven");
		        
		        service.insert(listFeedUrlNepremicnine, rowNepremicnine);
			}	
			
		} catch (FailingHttpStatusCodeException e) {
			return;
		} catch (MalformedURLException e) {
			return;
		} catch (IOException e) {
			return;
		} catch (ServiceException e) {
			return;
		}
	}
	
	public static void shraniSliko(String src, String idNepremicnine) {				
                
    	try {
			// doda sliko v tabelo
	        URL listFeedUrlSlike = tabelaSlik.getListFeedUrl();
	        ListEntry rowSlika = new ListEntry();
	        rowSlika.getCustomElements().setValueLocal("idnepremicnine", idNepremicnine);
	        rowSlika.getCustomElements().setValueLocal("urlslike", src);
	        service.insert(listFeedUrlSlike, rowSlika);
			
		} catch (IOException e) {
			return;
		} catch (ServiceException e) {
			return;
		}	
               
	}
	
	public static String[] geocoding(String naslov)	{
		String [] latLng = new String[2];
	    // kreira url s podatki v json obliki
	    String urlJson = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=";
	    
	    try {
			urlJson += URLEncoder.encode(naslov, "UTF-8");
			URL url = new URL(urlJson);
			 
		    // bere iz url
		    Scanner scan = new Scanner(url.openStream());
		    String jsonString = new String();
		    while (scan.hasNext()) {
		    	jsonString += scan.nextLine();
		    }	        
		    scan.close();
		 
		    // kreira JSON objekt
		    JSONObject jsonObjekt = new JSONObject(jsonString);
		    if (! jsonObjekt.getString("status").equals("OK")) {
		    	latLng[0] = latLng[1] = "";
		    	return latLng;
		    }	        
		 
		    // vzame prvi rezultat
		    JSONObject rezultat = jsonObjekt.getJSONArray("results").getJSONObject(0);
		    JSONObject loc = rezultat.getJSONObject("geometry").getJSONObject("location");
		    latLng[0] = loc.get("lat").toString();
		    latLng[1] = loc.get("lng").toString();
			
		} catch (UnsupportedEncodingException e) {
			latLng[0] = latLng[1] = "";
	    	return latLng;
		} catch (MalformedURLException e) {
			latLng[0] = latLng[1] = "";
	    	return latLng;
		} catch (IOException e) {
			latLng[0] = latLng[1] = "";
	    	return latLng;
		} catch (JSONException e) {
			latLng[0] = latLng[1] = "";
	    	return latLng;
		}
		
	    return latLng;
	}

}