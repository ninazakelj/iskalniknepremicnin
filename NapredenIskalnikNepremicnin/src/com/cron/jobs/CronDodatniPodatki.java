package com.cron.jobs;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

@SuppressWarnings("serial")
public class CronDodatniPodatki extends HttpServlet {
	
	private static SpreadsheetService service;
	private static String CLIENT_ID = "711446868423-nfjmhs7ftcjb2okck27o8t18765ofa3v.apps.googleusercontent.com";
	private static String CLIENT_SECRET = "BDq7BF5r51yhHJLUp8bHoRFe";	
	private static final String SERVICE_ACCOUNT_EMAIL = "711446868423-nfjmhs7ftcjb2okck27o8t18765ofa3v@developer.gserviceaccount.com";
	private static final String SERVICE_ACCOUNT_PKCS12_FILE_PATH = "WEB-INF/key.p12";
	// tabele 
	private static WorksheetEntry tabelaKraji = null;
    private static WorksheetEntry tabelaVrtci = null;
    private static WorksheetEntry tabelaOsnovneSole = null;
    private static WorksheetEntry tabelaSrednjeSole = null;
    private static WorksheetEntry tabelaVisjeStrokovneSole = null;
    private static WorksheetEntry tabelaZdravstvo = null;
    private static WorksheetEntry tabelaLekarne = null;
    private static WorksheetEntry tabelaZapisi = null;
    
    private static ListEntry rowZapisi;
    private static String zadnjiTipPodatkov;
    private static String zadnjaVrstica; 
    
    private static String danasnjiDatum = null;
    
	private static WebClient webClient;
	
    
	public void doGet(HttpServletRequest req, HttpServletResponse resp)	{
		cronJobDodatniPodatki();
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) {
		doGet(req, resp);
	}
    
	public static void cronJobDodatniPodatki()  {
		// pridobi dana�nji datum
		SimpleDateFormat formatDatuma = new SimpleDateFormat("dd.MM.yyyy");
		Calendar cal = Calendar.getInstance();
		danasnjiDatum = formatDatuma.format(cal.getTime());
		
		List<String> scopes = new ArrayList<String>();
		scopes.add("https://spreadsheets.google.com/feeds");
		
		HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
		JsonFactory JSON_FACTORY = new JacksonFactory();		
		
		// da se ne izpisuje obvestil v log
		LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

	    java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF); 
	    java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
		webClient = new WebClient();
		
        try {
        	// omogo�i dostop do spreadsheet-a
        	GoogleCredential credential = new GoogleCredential.Builder()
			.setTransport(HTTP_TRANSPORT)
			.setJsonFactory(JSON_FACTORY)
			.setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
			.setServiceAccountScopes(scopes)
			.setServiceAccountPrivateKeyFromP12File(new File(SERVICE_ACCOUNT_PKCS12_FILE_PATH))
			.build();
        	
        	service = new SpreadsheetService("com.cron.jobs");
        	service.setOAuth2Credentials(credential);
			// dolo�i timeout za service
	        service.setConnectTimeout(300000);
	        service.setReadTimeout(300000);
	        URL spreadsheetUrl = new URL("https://spreadsheets.google.com/feeds/spreadsheets/private/full");        
	        
	        // pridobi vse spreadsheet-e
	        SpreadsheetFeed feed = service.getFeed(spreadsheetUrl, SpreadsheetFeed.class);
	        List<SpreadsheetEntry> spreadsheets = feed.getEntries();
	        
	        SpreadsheetEntry sheetBaza = null;
	        // gre �ez vse spreadsheet-e in si zapomni tistega, ki se uporablja za dodatne podatke
	        for (SpreadsheetEntry spreadsheet : spreadsheets) {
	        	if (spreadsheet.getTitle().getPlainText().equals("Baza")) {
	        		sheetBaza = spreadsheet;
	        		break;
	        	}  
	        }
	        
	        // pridobi worksheet za podatke o dodatnih lastnostih
	        List<WorksheetEntry> worksheetsBaza = sheetBaza.getWorksheets();   
	        for (WorksheetEntry worksheet : worksheetsBaza) {
	        	if (worksheet.getTitle().getPlainText().equals("Kraji")) {
	        		tabelaKraji = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Vrtci")) {
	        		tabelaVrtci = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Osnovne_sole")) {
	        		tabelaOsnovneSole = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Srednje_sole")){
	        		tabelaSrednjeSole = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Visje_strokovne_sole")){
	        		tabelaVisjeStrokovneSole = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Zdravstvo")){
	        		tabelaZdravstvo = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Lekarne")){
	        		tabelaLekarne = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Stanje_dodatni_podatki")){
	        		tabelaZapisi = worksheet;	
	        	}
	        }  	
			
	        
	        // pridobi nazadnje obiskano
	        URL listFeedUrlZapisi = tabelaZapisi.getListFeedUrl();
	        ListFeed listFeedZapisi = service.getFeed(listFeedUrlZapisi, ListFeed.class);
	        rowZapisi = listFeedZapisi.getEntries().get(0);
	        zadnjiTipPodatkov = rowZapisi.getCustomElements().getValue("tippodatkov");
	        zadnjaVrstica = rowZapisi.getCustomElements().getValue("vrstica");
	        
        
	        // nastavitve webclient-a
        	webClient.getOptions().setJavaScriptEnabled(false); // Enables/disables JavaScript support. By default, this property is enabled.
			webClient.getOptions().setThrowExceptionOnScriptError(false); // Changes the behavior of this webclient when a script error occurs.
			webClient.getOptions().setUseInsecureSSL(true); // If set to true, the client will accept connections to any host, regardless of whether they have valid certificates or not. This is especially useful when you are trying to connect to a server with expired or corrupt certificates.
			webClient.getOptions().setCssEnabled(false); // Enables/disables CSS support.
			
			
			// kraji
			if (zadnjiTipPodatkov.equals("Kraji")) {
				HtmlPage pageSeznamKraji = webClient.getPage("http://www.skupnostobcin.si/?id=414");
				HtmlTable spletnaTabelaKraji = (HtmlTable) pageSeznamKraji.getByXPath("//*[@id='c185']/div/table/tbody/tr/td/table").get(0);
				
				int stevecKraji = 1;
				for (int i = 1; i < spletnaTabelaKraji.getRows().size(); i++) {
					if (stevecKraji == Integer.parseInt(zadnjaVrstica)) {
						HtmlTableRow vrstica = spletnaTabelaKraji.getRows().get(i);
						
						// vse velike crke spemeni v male
						String nazivObcine = vrstica.getCells().get(1).asText().toLowerCase();
						String[] besedeNaziva = nazivObcine.split(" ");
						nazivObcine = "";
						for (int j = 0; j < besedeNaziva.length; j++) {
							if (!besedeNaziva[j].equals("mesto") && !besedeNaziva[j].equals("vas") && !besedeNaziva[j].equals("trg") && !besedeNaziva[j].equals("selo") && !besedeNaziva[j].equals("ob") && !besedeNaziva[j].equals("na") && !besedeNaziva[j].equals("pri") && !besedeNaziva[j].equals("v")) {
								besedeNaziva[j] = Character.toUpperCase(besedeNaziva[j].charAt(0)) + besedeNaziva[j].substring(1);
							}
							nazivObcine = nazivObcine + besedeNaziva[j] + " ";
						}
						// odstrani presledek na koncu
						nazivObcine = nazivObcine.substring(0, nazivObcine.length() - 1);
						// preveri ce obcina ze obstaja v tabli
						boolean obstaja = false;
						URL listFeedUrlKraji = tabelaKraji.getListFeedUrl();
				        ListFeed listFeedKraji = service.getFeed(listFeedUrlKraji, ListFeed.class); 
				        ListEntry vrsticaTabeleKraji = null;
				        for (ListEntry row : listFeedKraji.getEntries()) {
				        	if (nazivObcine.equals(row.getTitle().getPlainText())) {
				        		obstaja = true;
				        		vrsticaTabeleKraji = row;
				        		break;
				        	}
				        }
				        if (obstaja) {
							// �e �e obstaja samo spremeni stolpec zadnji� pregledano
				        	vrsticaTabeleKraji.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
				        	vrsticaTabeleKraji.getCustomElements().setValueLocal("status", "aktiven");
				        	vrsticaTabeleKraji.update();
						}
						else {
							String[] lokacija = geocoding(nazivObcine + " Slovenija");
							// doda kraj tabelo
							URL listFeedUrlOsnovnaSola = tabelaOsnovneSole.getListFeedUrl();
					        ListEntry vrsticaKraj = new ListEntry();
					        vrsticaKraj.getCustomElements().setValueLocal("nazivobcine", nazivObcine);
					        vrsticaKraj.getCustomElements().setValueLocal("naslov", vrstica.getCells().get(3).asText());
					        vrsticaKraj.getCustomElements().setValueLocal("posta", vrstica.getCells().get(4).asText());
					        vrsticaKraj.getCustomElements().setValueLocal("latitude", lokacija[0]);
					        vrsticaKraj.getCustomElements().setValueLocal("longitude", lokacija[1]);
					        vrsticaKraj.getCustomElements().setValueLocal("dodano", danasnjiDatum);
					        vrsticaKraj.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
					        vrsticaKraj.getCustomElements().setValueLocal("status", "aktiven");
					        service.insert(listFeedUrlKraji, vrsticaKraj);	
						} 
				        
				        stevecKraji++;
				        updateVrstica(stevecKraji);
					}
					else {
						stevecKraji++;
					}
				}
				
				updateTipPodatkov("Vrtci");
				updateVrstica(1);
			}
						

			// vrtci
			if (zadnjiTipPodatkov.equals("Vrtci")) {
				HtmlPage pageSeznamVrtci = webClient.getPage("https://krka1.mss.edus.si/registriweb/SeznamVrtci.aspx?Enote=1");
				HtmlTable spletnaTabelaVrtci = (HtmlTable) pageSeznamVrtci.getByXPath("//*[@id='form1']/div[3]/table/tbody/tr[2]/td/table").get(0);
				
				int stevecVrtci = 1;
				for (int i = 1; i < spletnaTabelaVrtci.getRows().size(); i++) {
					if (stevecVrtci == Integer.parseInt(zadnjaVrstica)) {
						HtmlTableRow vrstica = spletnaTabelaVrtci.getRows().get(i);
						
						String nazivVrtec = vrstica.getCells().get(2).asText();
						// preveri �e vrtec �e obstaja v bazi
						boolean obstaja = false;
						URL listFeedUrlVrtci = tabelaVrtci.getListFeedUrl();
				        ListFeed listFeedVrtci = service.getFeed(listFeedUrlVrtci, ListFeed.class); 
				        ListEntry vrsticaTabeleVrtec = null;
				        for (ListEntry row : listFeedVrtci.getEntries()) {
				        	if (nazivVrtec.equals(row.getTitle().getPlainText())) {
				        		obstaja = true;
				        		vrsticaTabeleVrtec = row;
				        		break;
				        	}
				        }
						if (obstaja) {
							// �e �e obstaja samo spremeni stolpec zadnji� pregledano
							vrsticaTabeleVrtec.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
							vrsticaTabeleVrtec.getCustomElements().setValueLocal("status", "aktiven");
							vrsticaTabeleVrtec.update();
						}
						else {
							String[] lokacija = geocoding(vrstica.getCells().get(3).asText() + " " + vrstica.getCells().get(1).asText() + " Slovenija");
							// doda vrtec v tabelo
							URL listFeedUrlVrtec= tabelaVrtci.getListFeedUrl();
					        ListEntry vrsticaVrtec = new ListEntry();
					        vrsticaVrtec.getCustomElements().setValueLocal("nazivvrtca", vrstica.getCells().get(2).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("naslov", vrstica.getCells().get(3).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("postnastevilka", vrstica.getCells().get(4).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("obcina", vrstica.getCells().get(1).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("telefon", vrstica.getCells().get(6).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("faks", vrstica.getCells().get(7).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("prios", vrstica.getCells().get(11).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("zaseben", vrstica.getCells().get(10).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("email", vrstica.getCells().get(8).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("spletnastran", vrstica.getCells().get(9).asText());
					        vrsticaVrtec.getCustomElements().setValueLocal("latitude", lokacija[0]);
					        vrsticaVrtec.getCustomElements().setValueLocal("longitude", lokacija[1]);
					        vrsticaVrtec.getCustomElements().setValueLocal("dodano", danasnjiDatum);
					        vrsticaVrtec.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
					        vrsticaVrtec.getCustomElements().setValueLocal("status", "aktiven");
					        service.insert(listFeedUrlVrtec, vrsticaVrtec);
						}
						
						stevecVrtci++;
						updateVrstica(stevecVrtci);
					}
					else {
						stevecVrtci++;
					}
				}
				
				updateTipPodatkov("Osnovne �ole");
				updateVrstica(1);
			}
			

			// osnovne sole
			if (zadnjiTipPodatkov.equals("Osnovne �ole")) {
				HtmlPage pageSeznamOsnovneSole = webClient.getPage("https://krka1.mss.edus.si/registriweb/Seznam1.aspx?Seznam=2010");
				HtmlTable spletnaTabelaOsnovneSole = (HtmlTable) pageSeznamOsnovneSole.getByXPath("//*[@id='form1']/div[3]/table/tbody/tr[2]/td/table").get(0);
				
				int stevecOsnovneSole = 1;
				for (int i = 1; i < spletnaTabelaOsnovneSole.getRows().size(); i++) {
					if (stevecOsnovneSole == Integer.parseInt(zadnjaVrstica)) {
						HtmlTableRow vrstica = spletnaTabelaOsnovneSole.getRows().get(i);
						
						String nazivOS = vrstica.getCells().get(2).asText();
						// preveri �e osnovna �ola �e obstaja v tabeli
						boolean obstaja = false;
						URL listFeedUrlOsnovneSole = tabelaOsnovneSole.getListFeedUrl();
				        ListFeed listFeedOsnovneSole = service.getFeed(listFeedUrlOsnovneSole, ListFeed.class); 
				        ListEntry vrsticaTabeleOsnovnaSola = null;
				        for (ListEntry row : listFeedOsnovneSole.getEntries()) {
				        	if (nazivOS.equals(row.getTitle().getPlainText())) {
				        		obstaja = true;
				        		vrsticaTabeleOsnovnaSola = row;
				        		break;
				        	}
				        }
						if (obstaja) {
							// �e �e obstaja samo spremeni stolpec zadnji� pregledano
							vrsticaTabeleOsnovnaSola.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
							vrsticaTabeleOsnovnaSola.getCustomElements().setValueLocal("status", "aktiven");
							vrsticaTabeleOsnovnaSola.update();
						}
						else {
							String[] lokacija = geocoding(vrstica.getCells().get(3).asText() + " " + vrstica.getCells().get(1).asText() + " Slovenija");
							// doda osnovno �olo v tabelo
							URL listFeedUrlOsnovnaSola = tabelaOsnovneSole.getListFeedUrl();
					        ListEntry vrsticaOsnovnaSola = new ListEntry();
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("nazivsole", vrstica.getCells().get(2).asText());
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("naslov", vrstica.getCells().get(3).asText());
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("postnastevilka", vrstica.getCells().get(4).asText());
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("obcina", vrstica.getCells().get(1).asText());
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("telefon", vrstica.getCells().get(6).asText());
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("faks", vrstica.getCells().get(7).asText());
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("email", vrstica.getCells().get(8).getElementsByTagName("a").get(0).getAttribute("href").replace("mailto:", ""));
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("spletnastran", vrstica.getCells().get(9).getElementsByTagName("a").get(0).getAttribute("href"));
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("latitude", lokacija[0]);
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("longitude", lokacija[1]);
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("dodano", danasnjiDatum);
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
					        vrsticaOsnovnaSola.getCustomElements().setValueLocal("status", "aktiven");
					        service.insert(listFeedUrlOsnovnaSola, vrsticaOsnovnaSola);
						}
						
						stevecOsnovneSole++;
						updateVrstica(stevecOsnovneSole);
					}
					else {
						stevecOsnovneSole++;
					}
				}
				
				updateTipPodatkov("Srednje �ole");
				updateVrstica(1);
			}
			
			
			// srednje sole
			if (zadnjiTipPodatkov.equals("Srednje �ole")) {
				HtmlPage pageSeznamSrednjeSole = webClient.getPage("https://krka1.mss.edus.si/registriweb/Seznam2.aspx?Seznam=3010");
				HtmlTable spletnaTabelaSrednjeSole = (HtmlTable) pageSeznamSrednjeSole.getByXPath("//*[@id='form1']/div[3]/table/tbody/tr[2]/td/table").get(0);
				
				int stevecSrednjeSole = 1;
				for (int i = 2; i < spletnaTabelaSrednjeSole.getRows().size(); i++) {
					if (stevecSrednjeSole == Integer.parseInt(zadnjaVrstica)) {
						HtmlTableRow vrstica = spletnaTabelaSrednjeSole.getRows().get(i);
						if (!vrstica.getCells().get(0).asText().equals("NAZIV �OLE") && !vrstica.getCells().get(0).asText().contains("regija") && !vrstica.getCells().get(0).asText().contains("Slovenija")) {
							
							String nazivSS = vrstica.getCells().get(0).asText();
							// preveri �e srednja �ola �e obstaja v tabeli
							boolean obstaja = false;
							URL listFeedUrlSrednjeSole = tabelaSrednjeSole.getListFeedUrl();
					        ListFeed listFeedSrednjeSole = service.getFeed(listFeedUrlSrednjeSole, ListFeed.class); 
					        ListEntry vrsticaTabeleSrednjaSola = null;
					        for (ListEntry row : listFeedSrednjeSole.getEntries()) {
					        	if (nazivSS.equals(row.getTitle().getPlainText())) {
					        		obstaja = true;
					        		vrsticaTabeleSrednjaSola = row;
					        		break;
					        	}
					        }
					        if (obstaja) {
								// �e �e obstaja samo spremeni stolpec zadnji� pregledano
					        	vrsticaTabeleSrednjaSola.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
					        	vrsticaTabeleSrednjaSola.getCustomElements().setValueLocal("status", "aktiven");
					        	vrsticaTabeleSrednjaSola.update();
							}
							else {
								String[] lokacija = geocoding(vrstica.getCells().get(1).asText() + " " + vrstica.getCells().get(3).asText() + " Slovenija");
								// doda srednjo �olo v tabelo
								URL listFeedUrlSrednjaSola = tabelaSrednjeSole.getListFeedUrl();
						        ListEntry vrsticaSrednjaSola = new ListEntry();
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("nazivsole", vrstica.getCells().get(0).asText());
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("naslov", vrstica.getCells().get(1).asText());
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("postnastevilka", vrstica.getCells().get(2).asText());
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("posta", vrstica.getCells().get(3).asText());
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("telefon", vrstica.getCells().get(4).asText());
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("faks", vrstica.getCells().get(5).asText());
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("email", vrstica.getCells().get(6).getElementsByTagName("a").get(0).getAttribute("href").replace("mailto:", ""));
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("spletnastran", vrstica.getCells().get(7).getElementsByTagName("a").get(0).getAttribute("href"));
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("latitude", lokacija[0]);
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("longitude", lokacija[1]);
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("dodano", danasnjiDatum);
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
						        vrsticaSrednjaSola.getCustomElements().setValueLocal("status", "aktiven");
						        service.insert(listFeedUrlSrednjaSola, vrsticaSrednjaSola);
							}
						}
						
						stevecSrednjeSole++;
						updateVrstica(stevecSrednjeSole);
					}
					else {
						stevecSrednjeSole++;
					}	
				}
				
				updateTipPodatkov("Vi�je strokovne �ole");
				updateVrstica(1);
			}
			
			
			// visje strokovne sole in fakultete
			if (zadnjiTipPodatkov.equals("Vi�je strokovne �ole")) {
				HtmlPage pageSeznamStrokovneSole = webClient.getPage("https://krka1.mss.edus.si/registriweb/Seznam1.aspx?Seznam=5040-enote");
				HtmlTable spletnaTabelaStrokovneSole = (HtmlTable) pageSeznamStrokovneSole.getByXPath("//*[@id='form1']/div[3]/table/tbody/tr[2]/td/table").get(0);
				
				int stevecVisjeSole = 1;
				for (int i = 1; i < spletnaTabelaStrokovneSole.getRows().size(); i++) {
					if (stevecVisjeSole == Integer.parseInt(zadnjaVrstica)) {
						HtmlTableRow vrstica = spletnaTabelaStrokovneSole.getRows().get(i);
						
						String nazivVSS = vrstica.getCells().get(2).asText();
						// preveri �e srednja �ola �e obstaja v tabeli
						boolean obstaja = false;
						URL listFeedUrlVisjeStrokovneSole = tabelaVisjeStrokovneSole.getListFeedUrl();
				        ListFeed listFeedVisjeStrokovneSole = service.getFeed(listFeedUrlVisjeStrokovneSole, ListFeed.class); 
				        ListEntry vrsticaTabeleVisjaStrokovnaSola = null;
				        for (ListEntry row : listFeedVisjeStrokovneSole.getEntries()) {
				        	if (nazivVSS.equals(row.getTitle().getPlainText())) {
				        		obstaja = true;
				        		vrsticaTabeleVisjaStrokovnaSola = row;
				        		break;
				        	}
				        }
				        if (obstaja) {
							// �e �e obstaja samo spremeni stolpec zadnji� pregledano
				        	vrsticaTabeleVisjaStrokovnaSola.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
				        	vrsticaTabeleVisjaStrokovnaSola.getCustomElements().setValueLocal("status", "aktiven");
				        	vrsticaTabeleVisjaStrokovnaSola.update();
						}
						else {
							String[] lokacija = geocoding(vrstica.getCells().get(3).asText() + " " + vrstica.getCells().get(1).asText() + " Slovenija");
							// doda vi�jo strokovno �olo v tabelo
							URL listFeedUrlVisjaStrokovnaSola = tabelaVisjeStrokovneSole.getListFeedUrl();
					        ListEntry vrsticaVisjaStrokovnaSola = new ListEntry();
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("nazivsole", vrstica.getCells().get(2).asText());
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("naslov", vrstica.getCells().get(3).asText());
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("postnastevilka", vrstica.getCells().get(4).asText());
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("obcina", vrstica.getCells().get(1).asText());
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("telefon", vrstica.getCells().get(6).asText());
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("faks", vrstica.getCells().get(7).asText());
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("email", vrstica.getCells().get(8).getElementsByTagName("a").get(0).getAttribute("href").replace("mailto:", ""));
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("spletnastran", vrstica.getCells().get(9).getElementsByTagName("a").get(0).getAttribute("href"));
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("latitude", lokacija[0]);
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("longitude", lokacija[1]);
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("dodano", danasnjiDatum);
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
					        vrsticaVisjaStrokovnaSola.getCustomElements().setValueLocal("status", "aktiven");
					        service.insert(listFeedUrlVisjaStrokovnaSola, vrsticaVisjaStrokovnaSola);	
						}
				        
				        stevecVisjeSole++;
						updateVrstica(stevecVisjeSole);
					}
					else {
						stevecVisjeSole++;
					}					
				}
				
				updateTipPodatkov("Zdravstvo");
				updateVrstica(1);
			}
						
			
			// zdravstvo
			if (zadnjiTipPodatkov.equals("Zdravstvo")) {
				HtmlPage pageZdravstvo = webClient.getPage("https://zdrzz.si/index.php?option=com_content&view=article&id=68&Itemid=105");
				List<?> seznamZdravstvenihUstanov = (List) pageZdravstvo.getByXPath("//*[@id='full-core']/div[2]") ; 
				HtmlElement seznam = (HtmlElement) seznamZdravstvenihUstanov.get(0);
				List<HtmlElement> zdravstveneUstanove = seznam.getElementsByTagName("div");
				
				int stevecZdravstvo = 1;
				for (int i = 0; i < zdravstveneUstanove.size() - 1; i++) {
					if (stevecZdravstvo == Integer.parseInt(zadnjaVrstica)) {
						HtmlElement ustanova = zdravstveneUstanove.get(i);
						String[] podatkiUstanove = ustanova.asText().split("\n");
						
						String nazivUstanove = podatkiUstanove[0].replace("\r", "");
						// preveri �e zdravstvena ustanova �e obstaja v tabeli
						boolean obstaja = false;
						URL listFeedUrlZdravstvo = tabelaZdravstvo.getListFeedUrl();
				        ListFeed listFeedZdravstvo = service.getFeed(listFeedUrlZdravstvo, ListFeed.class); 
				        ListEntry vrsticaTabeleZdravstvo = null;
				        for (ListEntry row : listFeedZdravstvo.getEntries()) {
				        	if (nazivUstanove.equals(row.getTitle().getPlainText())) {
				        		obstaja = true;
				        		vrsticaTabeleZdravstvo = row;
				        		break;
				        	}
				        }
				        if (obstaja) {
							// �e �e obstaja samo spremeni stolpec zadnji� pregledano
				        	vrsticaTabeleZdravstvo.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
				        	vrsticaTabeleZdravstvo.getCustomElements().setValueLocal("status", "aktiven");
				        	vrsticaTabeleZdravstvo.update();
						}
						else {
							String naslov = podatkiUstanove[1].replace("\r", "");
							String posta = "", postnaStevilka="";
							if (podatkiUstanove[2].contains("Telefon:")) {
								postnaStevilka = podatkiUstanove[1].substring(0, 4);
								posta = podatkiUstanove[1].substring(5).replace("\r", "").replaceAll("^\\s+|\\s+$", "");
							}
							else {
								postnaStevilka = podatkiUstanove[2].substring(0, 4); 
								posta = podatkiUstanove[2].substring(5).replace("\r", "").replaceAll("^\\s+|\\s+$", "");
							}
							
							String telefon = "", fax = "", spletnaStran = "";
							for (int j = 2; j < podatkiUstanove.length; j++) {
								if (podatkiUstanove[j].contains("Telefon:")) {
									telefon = podatkiUstanove[j].replace("Telefon:", "").replace("\r", "");
								}
								else if (podatkiUstanove[j].contains("Faks:")) {
									fax = podatkiUstanove[j].replace("Faks:", "").replace("\r", "");
								}
								else if (podatkiUstanove[j].contains("Spletni naslov:")) {
									spletnaStran = podatkiUstanove[j].replace("Spletni naslov:", "");
								}
								
							}
							
							String[] lokacija = geocoding(naslov + " " + posta + " Slovenija");
							// doda zdravstveno ustanovo v tabelo
							URL listFeedUrlZdravstvenaUstanova = tabelaZdravstvo.getListFeedUrl();
					        ListEntry vrsticaZdravstvo = new ListEntry();
					        vrsticaZdravstvo.getCustomElements().setValueLocal("naziv", nazivUstanove);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("naslov", naslov);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("postnastevilka", postnaStevilka);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("posta", posta);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("telefon", telefon);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("faks", fax);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("spletnastran", spletnaStran);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("latitude", lokacija[0]);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("longitude", lokacija[1]);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("dodano", danasnjiDatum);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
					        vrsticaZdravstvo.getCustomElements().setValueLocal("status", "aktiven");
					        service.insert(listFeedUrlZdravstvenaUstanova, vrsticaZdravstvo);
					        
						}
				        
				        stevecZdravstvo++;
						updateVrstica(stevecZdravstvo);
					}
					else {
						stevecZdravstvo++;
					}	
				}
				
				updateTipPodatkov("Lekarne");
				updateVrstica(1);
			}
			
			
			if (zadnjiTipPodatkov.equals("Lekarne")) {
				webClient.getOptions().setJavaScriptEnabled(true); 
			
				// lekarne
				HtmlPage pageLekarne = webClient.getPage("http://www.lzs.si/Mrezalekarn/Iskalnik/tabid/80/language/sl-SI/Default.aspx");
				HtmlForm form = pageLekarne.getFormByName("Form");
				
				HtmlSubmitInput button = form.getInputByName("dnn$ctr508$Seznam$cmdOK");
				HtmlSelect select = (HtmlSelect) pageLekarne.getElementById("dnn_ctr508_Seznam_kr");
				//HtmlInput intputBox = pageLekarne.getHtmlElementById("dnn_ctr508_Seznam_iz");
				List<HtmlElement> options = select.getElementsByTagName("option");
				
				int stevecLekarne = 1;
				for (int i = 1; i < options.size(); i++) {
					if (stevecLekarne == Integer.parseInt(zadnjaVrstica)) {
						HtmlOption option = (HtmlOption) options.get(i);
						select.setSelectedAttribute(option, true);
					
						pageLekarne = button.click();
						webClient.waitForBackgroundJavaScript(10 * 1000);
						
						HtmlTable spletnaTabelaLekarne = (HtmlTable) pageLekarne.getElementById("dnn_ctr508_Seznamlst");
						if (spletnaTabelaLekarne != null) {
							for (int j = 3; j < spletnaTabelaLekarne.getRows().size(); j++) {
							
								HtmlTableRow vrstica = spletnaTabelaLekarne.getRows().get(j);
								
								String nazivLekarne = vrstica.getCells().get(1).asText().replace("\r", "").replace("\n", "");
								// preveri �e lekarna �e obstaja v tabeli
								boolean obstaja = false;
								URL listFeedUrlLekarne = tabelaLekarne.getListFeedUrl();
						        ListFeed listFeedLekarne = service.getFeed(listFeedUrlLekarne, ListFeed.class); 
						        ListEntry vrsticaTabeleLekarne = null;
						        for (ListEntry row : listFeedLekarne.getEntries()) {
						        	if (nazivLekarne.equals(row.getTitle().getPlainText())) {
						        		obstaja = true;
						        		vrsticaTabeleLekarne = row;
						        		break;
						        	}
						          
						        }
						        if (obstaja) {
									// �e �e obstaja samo spremeni stolpec zadnji� pregledano
						        	vrsticaTabeleLekarne.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
						        	vrsticaTabeleLekarne.getCustomElements().setValueLocal("status", "aktiven");
						        	vrsticaTabeleLekarne.update();
								}
								else {
									String[] naslovPosta = vrstica.getCells().get(2).asText().split("\n");
									String naslov = naslovPosta[0].replace("\r", "").replaceAll("^\\s+|\\s+$", "");
									String postnaStevilka = naslovPosta[1].replaceAll("^\\s+|\\s+$", "").substring(0, 4);
									String posta = naslovPosta[1].substring(5).replace("\r", "").replaceAll("^\\s+|\\s+$", "");
									
									String[] ostaliPodatki = vrstica.getCells().get(3).asText().split("\n");
									String telefon = "", faks = "", email= "", spletnaStran="";
									for (int k = 0; k < ostaliPodatki.length; k++) {
										if (ostaliPodatki[k].contains("T:")) {
											telefon = ostaliPodatki[k].replace("T:", "").replaceAll("^\\s+|\\s+$", "");
										}
										else if (ostaliPodatki[k].contains("F:")) {
											faks = ostaliPodatki[k].replace("F:", "").replaceAll("^\\s+|\\s+$", "");
										}
										else if (ostaliPodatki[k].contains("E:")) {
											email = ostaliPodatki[k].replace("E:", "").replaceAll("^\\s+|\\s+$", "");
										}
										else if (ostaliPodatki[k].contains("I:")) {
											spletnaStran = ostaliPodatki[k].replace("I:", "").replaceAll("^\\s+|\\s+$", "");
										}
										
									}
									
									String[] lokacija = geocoding(naslov + " " + posta + " Slovenija");
									// doda zdravstveno ustanovo v tabelo
									URL listFeedUrlLekarna = tabelaLekarne.getListFeedUrl();
							        ListEntry vrsticaLekarna = new ListEntry();
							        vrsticaLekarna.getCustomElements().setValueLocal("naziv", nazivLekarne);
							        vrsticaLekarna.getCustomElements().setValueLocal("naslov", naslov);
							        vrsticaLekarna.getCustomElements().setValueLocal("postnastevilka", postnaStevilka);
							        vrsticaLekarna.getCustomElements().setValueLocal("posta", posta);
							        vrsticaLekarna.getCustomElements().setValueLocal("telefon", telefon);
							        vrsticaLekarna.getCustomElements().setValueLocal("faks", faks);
							        vrsticaLekarna.getCustomElements().setValueLocal("email", email);
							        vrsticaLekarna.getCustomElements().setValueLocal("spletnastran", spletnaStran);
							        vrsticaLekarna.getCustomElements().setValueLocal("latitude", lokacija[0]);
							        vrsticaLekarna.getCustomElements().setValueLocal("longitude", lokacija[1]);
							        vrsticaLekarna.getCustomElements().setValueLocal("dodano", danasnjiDatum);
							        vrsticaLekarna.getCustomElements().setValueLocal("zadnjicpregledano", danasnjiDatum);
							        vrsticaLekarna.getCustomElements().setValueLocal("status", "aktiven");
							        service.insert(listFeedUrlLekarna, vrsticaLekarna);
								}
							}
							
						}
						
						stevecLekarne++;
						updateVrstica(stevecLekarne);
					}
					else {
						stevecLekarne++;
					}
	        	}
				
				updateTipPodatkov("Kraji");
				updateVrstica(1);
			}
			
			
		} catch (AuthenticationException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}  
			
		
	}
	
	private static void updateTipPodatkov(String tipPodatkov) {
		zadnjiTipPodatkov = tipPodatkov;
		URL listFeedUrlZapisiTipPodatkov = tabelaZapisi.getListFeedUrl();
        ListFeed listFeedZapisiTipPodatkov;
		try {
			listFeedZapisiTipPodatkov = service.getFeed(listFeedUrlZapisiTipPodatkov, ListFeed.class);
			rowZapisi = listFeedZapisiTipPodatkov.getEntries().get(0);
			rowZapisi.getCustomElements().setValueLocal("tippodatkov", zadnjiTipPodatkov);
			rowZapisi.update();
		} catch (IOException e) {
			return;
		} catch (ServiceException e) {
			return;
		}
        
	}
	
	private static void updateVrstica(int stevec) {
		zadnjaVrstica = Integer.toString(stevec);
		URL listFeedUrlZapisiVrstica = tabelaZapisi.getListFeedUrl();
        ListFeed listFeedZapisiVrstica;
		try {
			listFeedZapisiVrstica = service.getFeed(listFeedUrlZapisiVrstica, ListFeed.class);
			rowZapisi = listFeedZapisiVrstica.getEntries().get(0);
			rowZapisi.getCustomElements().setValueLocal("vrstica", zadnjaVrstica);
			rowZapisi.update();
		} catch (IOException e) {
			return;
		} catch (ServiceException e) {
			return;
		}
        
	}
	
	public static String[] geocoding(String naslov)	{
		String [] latLng = new String[2];
	    // kreira url s podatki v json obliki
	    String urlJson = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=";
	    
	    try {
			urlJson += URLEncoder.encode(naslov, "UTF-8");
			URL url = new URL(urlJson);
			 
		    // bere iz url
		    Scanner scan = new Scanner(url.openStream());
		    String jsonString = new String();
		    while (scan.hasNext()) {
		    	jsonString += scan.nextLine();
		    }	        
		    scan.close();
		 
		    // kreira JSON objekt
		    JSONObject jsonObjekt = new JSONObject(jsonString);
		    if (! jsonObjekt.getString("status").equals("OK")) {
		    	latLng[0] = latLng[1] = "";
		    	return latLng;
		    }	        
		 
		    // vzame prvi rezultat
		    JSONObject rezultat = jsonObjekt.getJSONArray("results").getJSONObject(0);
		    JSONObject loc = rezultat.getJSONObject("geometry").getJSONObject("location");
		    latLng[0] = loc.get("lat").toString();
		    latLng[1] = loc.get("lng").toString();
			
		} catch (UnsupportedEncodingException e) {
			latLng[0] = latLng[1] = "";
	    	return latLng;
		} catch (MalformedURLException e) {
			latLng[0] = latLng[1] = "";
	    	return latLng;
		} catch (IOException e) {
			latLng[0] = latLng[1] = "";
	    	return latLng;
		} catch (JSONException e) {
			latLng[0] = latLng[1] = "";
	    	return latLng;
		}
		
	    return latLng;
	}

}

