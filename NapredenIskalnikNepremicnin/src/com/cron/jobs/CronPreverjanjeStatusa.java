package com.cron.jobs;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

@SuppressWarnings("serial")
public class CronPreverjanjeStatusa extends HttpServlet {
	
	private static SpreadsheetService service;
	private static String CLIENT_ID = "711446868423-nfjmhs7ftcjb2okck27o8t18765ofa3v.apps.googleusercontent.com";
	private static String CLIENT_SECRET = "BDq7BF5r51yhHJLUp8bHoRFe";	
	private static final String SERVICE_ACCOUNT_EMAIL = "711446868423-nfjmhs7ftcjb2okck27o8t18765ofa3v@developer.gserviceaccount.com";
	private static final String SERVICE_ACCOUNT_PKCS12_FILE_PATH = "WEB-INF/key.p12";
	// tabele 
    private static WorksheetEntry tabelaNepremicnin = null;
    private static WorksheetEntry tabelaLastnosti = null;
    private static WorksheetEntry tabelaSlik = null;
    
 	private static WorksheetEntry tabelaKraji = null;
 	private static WorksheetEntry tabelaVrtci = null;
    private static WorksheetEntry tabelaOsnovneSole = null;
    private static WorksheetEntry tabelaSrednjeSole = null;
    private static WorksheetEntry tabelaVisjeStrokovneSole = null;
    private static WorksheetEntry tabelaZdravstvo = null;
    private static WorksheetEntry tabelaLekarne = null;
    
    private static WorksheetEntry tabelaZapisi = null;
    
    private static ListEntry rowZapisi;
    private static String zadnjiTipPodatkov;
    private static String zadnjaVrstica; 
	
    private static String danasnjiDatum = null;
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp)	{
    	cronJobPreverjanjeStatusa();
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) {
		doGet(req, resp);
	}
    
	public static void cronJobPreverjanjeStatusa() {
		
		// pridobi dana�nji datum
		SimpleDateFormat formatDatuma = new SimpleDateFormat("dd.MM.yyyy");
		Calendar cal = Calendar.getInstance();
		danasnjiDatum = formatDatuma.format(cal.getTime());
		
		List<String> scopes = new ArrayList<String>();
		scopes.add("https://spreadsheets.google.com/feeds");

		HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
		JsonFactory JSON_FACTORY = new JacksonFactory();
				
		// omogo�i dostop do spreadsheet-a
        try {
        	GoogleCredential credential = new GoogleCredential.Builder()
			.setTransport(HTTP_TRANSPORT)
			.setJsonFactory(JSON_FACTORY)
			.setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
			.setServiceAccountScopes(scopes)
			.setServiceAccountPrivateKeyFromP12File(new File(SERVICE_ACCOUNT_PKCS12_FILE_PATH))
			.build();
        	
        	service = new SpreadsheetService("com.cron.jobs");
        	service.setOAuth2Credentials(credential);
			// dolo�i timeout za service
	        service.setConnectTimeout(300000);
	        service.setReadTimeout(300000);
	        URL spreadsheetUrl = new URL("https://spreadsheets.google.com/feeds/spreadsheets/private/full");        
	        			
	        // pridobi vse spreadsheet-e
	        SpreadsheetFeed feed = service.getFeed(spreadsheetUrl, SpreadsheetFeed.class);
	        List<SpreadsheetEntry> spreadsheets = feed.getEntries();
		
	        SpreadsheetEntry sheetNepremicnine = null;
	        SpreadsheetEntry sheetDodatneLastnosti = null;
	        SpreadsheetEntry sheetSlike = null;
	        SpreadsheetEntry sheetBaza = null;
	        
	        
	        // gre �ez vse spreadsheet-e
	        for (SpreadsheetEntry spreadsheet : spreadsheets) {
	        	if (spreadsheet.getTitle().getPlainText().equals("Nepremicnine_podatki")) {
	        		sheetNepremicnine = spreadsheet;
	        	}  
	        	else if (spreadsheet.getTitle().getPlainText().equals("Nepremicnine_dodatne_lastnosti")) {
	        		sheetDodatneLastnosti = spreadsheet;
	        	} 
	        	else if (spreadsheet.getTitle().getPlainText().equals("Nepremicnine_slike")) {
	        		sheetSlike = spreadsheet;
	        	}
	        	else if (spreadsheet.getTitle().getPlainText().equals("Baza")) {
	        		sheetBaza = spreadsheet;
	        	}
	        }
	        
	        // pridobi worksheet za podatke o nepremi�nini
	        List<WorksheetEntry> worksheetsNepremicnine = sheetNepremicnine.getWorksheets();   
	        for (WorksheetEntry worksheet : worksheetsNepremicnine) {
	        	if (worksheet.getTitle().getPlainText().equals("Nepremicnine")) {
	        		tabelaNepremicnin = worksheet;	
	        		break;
	        	}
	        }  	
	        
	        // pridobi worksheet za dodatne lastnosti nepremi�nine
		    List<WorksheetEntry> worksheetsDodatneLastnosti = sheetDodatneLastnosti.getWorksheets();   
		    for (WorksheetEntry worksheet : worksheetsDodatneLastnosti) {
		    	if (worksheet.getTitle().getPlainText().equals("Dodatne_lastnosti")) {
		    		tabelaLastnosti = worksheet;
		    		break;
		    	}
		    }  
	        
		    // pridobi worksheet za slike nepremi�nine
		    List<WorksheetEntry> worksheetsSlike = sheetSlike.getWorksheets();   
		    for (WorksheetEntry worksheet : worksheetsSlike) {
		    	if (worksheet.getTitle().getPlainText().equals("Slike")) {
		    		tabelaSlik = worksheet;
		    		break;
		    	}
		    }  
	        
		    // pridobi worksheet za podatke o dodatnih lastnostih
	        List<WorksheetEntry> worksheetsBaza = sheetBaza.getWorksheets();   
	        for (WorksheetEntry worksheet : worksheetsBaza) {
	        	if (worksheet.getTitle().getPlainText().equals("Kraji")) {
	        		tabelaKraji = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Vrtci")) {
	        		tabelaVrtci = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Osnovne_sole")) {
	        		tabelaOsnovneSole = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Srednje_sole")){
	        		tabelaSrednjeSole = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Visje_strokovne_sole")){
	        		tabelaVisjeStrokovneSole = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Zdravstvo")){
	        		tabelaZdravstvo = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Lekarne")){
	        		tabelaLekarne = worksheet;	
	        	}
	        	else if (worksheet.getTitle().getPlainText().equals("Stanje_neaktivni")){
	        		tabelaZapisi = worksheet;	
	        	}
	        }  	
	        
	        // pridobi nazadnje obiskano
	        URL listFeedUrlZapisi = tabelaZapisi.getListFeedUrl();
	        ListFeed listFeedZapisi = service.getFeed(listFeedUrlZapisi, ListFeed.class);
	        rowZapisi = listFeedZapisi.getEntries().get(0);
	        zadnjiTipPodatkov = rowZapisi.getCustomElements().getValue("tippodatkov");
	        zadnjaVrstica = rowZapisi.getCustomElements().getValue("vrstica");
	        
	     
	        if (zadnjiTipPodatkov.equals("Nepremi�nine")) {
            	boolean seObstaja = true;
            	int trenutnaVrstica = Integer.parseInt(zadnjaVrstica);
            	while (seObstaja) {
            		URL listFeedUrlNeprem = tabelaNepremicnin.getListFeedUrl();
        			ListQuery query = new ListQuery(listFeedUrlNeprem);
        			
        			query.setStartIndex(trenutnaVrstica);
        			query.setMaxResults(1);
        		    ListFeed listFeedNeprem = service.query(query, ListFeed.class);
        		    
        		    if (listFeedNeprem.getEntries().size() > 0) {
        				ListEntry rowNepremicnina = listFeedNeprem.getEntries().get(0);
        				
        				String zadnjicPregledano = rowNepremicnina.getCustomElements().getValue("zadnjicpregledano");
						if (rowNepremicnina.getCustomElements().getValue("status").equals("aktiven")) {
							// preveri �e ima datum zadnje pregledano manj�i kot 7 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 14) { 
								// da status na neaktiven
								rowNepremicnina.getCustomElements().setValueLocal("status", "neaktiven");
								rowNepremicnina.update();
							}
							
							trenutnaVrstica++;
							updateVrstica(trenutnaVrstica);
						}
						else {
							// preveri �e ima datum zadnje pregledano manj�i kot 14 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 28) { 
								// zbri�e nepremi�nino iz tabele
								String idNepremicnine = rowNepremicnina.getCustomElements().getValue("id");
								rowNepremicnina.delete();

								// izbri�e slike iz tabele
								URL listFeedUrlSlike = tabelaSlik.getListFeedUrl();
								ListQuery querySlike = new ListQuery(listFeedUrlSlike);
								querySlike.setSpreadsheetQuery("idnepremicnine=" + idNepremicnine);
							    ListFeed listFeedSlike = service.query(querySlike, ListFeed.class);
								if (listFeedSlike.getEntries().size() > 0) {
									for (ListEntry row : listFeedSlike.getEntries()) {
										row.delete();
									}
								}
								
								// izbri�e dodatne lastnosti iz tabele
								URL listFeedUrlDodatneLastnosti = tabelaLastnosti.getListFeedUrl();
								ListQuery queryLastnosti = new ListQuery(listFeedUrlDodatneLastnosti);
								queryLastnosti.setSpreadsheetQuery("idnepremicnine=" + idNepremicnine);
							    ListFeed listFeedDodatneLastnosti = service.query(queryLastnosti, ListFeed.class);
								if (listFeedDodatneLastnosti.getEntries().size() > 0) {
									for (ListEntry row : listFeedDodatneLastnosti.getEntries()) {
										row.delete();
									}
								}
							}
							else {
								trenutnaVrstica++;
								updateVrstica(trenutnaVrstica);
							}
						}
        		    }
        			else {
        				seObstaja = false;
        			}
            	}
                      	
            	updateTipPodatkov("Kraji");
				updateVrstica(0);            	
            }
	        
	        
            if (zadnjiTipPodatkov.equals("Kraji")) {
            	URL listFeedUrlKraji = tabelaKraji.getListFeedUrl();
	            ListFeed listFeedKraji = service.getFeed(listFeedUrlKraji, ListFeed.class);
	            
	            for (int i = 0; i < listFeedKraji.getEntries().size(); i++) {
	            	if (i == Integer.parseInt(zadnjaVrstica)) {
	            		ListEntry rowKraj = listFeedKraji.getEntries().get(i);
	            		String zadnjicPregledano = rowKraj.getCustomElements().getValue("zadnjicpregledano");
						if (rowKraj.getCustomElements().getValue("status").equals("aktiven")) {
							// preveri �e ima datum zadnje pregledano manj�i kot 7 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 7) { 
								// da status na neaktiven
								rowKraj.getCustomElements().setValueLocal("status", "neaktiven");
								rowKraj.update();
							}
							updateVrstica(i + 1);
						}
						else {
							// preveri �e ima datum zadnje pregledano manj�i kot 14 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 14) { 
								// zbri�e kraj iz tabele
								rowKraj.delete();
								i = i - 1;
								
								listFeedUrlKraji = tabelaKraji.getListFeedUrl();
					            listFeedKraji = service.getFeed(listFeedUrlKraji, ListFeed.class);
							}
							else {
								updateVrstica(i + 1);
							}
						}
	            	}
	            }
	                        	
            	updateTipPodatkov("Vrtci");
				updateVrstica(0);
            }
	        
            
            if (zadnjiTipPodatkov.equals("Vrtci")) {
            	URL listFeedUrlVrtci = tabelaVrtci.getListFeedUrl();
	            ListFeed listFeedVrtci = service.getFeed(listFeedUrlVrtci, ListFeed.class);

	            for (int i = 0; i < listFeedVrtci.getEntries().size(); i++) {
	            	if (i == Integer.parseInt(zadnjaVrstica)) {
	            		ListEntry rowVrtec = listFeedVrtci.getEntries().get(i);
	            		String zadnjicPregledano = rowVrtec.getCustomElements().getValue("zadnjicpregledano");
	            		if (rowVrtec.getCustomElements().getValue("status").equals("aktiven")) {
							// preveri �e ima datum zadnje pregledano manj�i kot 7 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 7) { 
								// da status na neaktiven
								rowVrtec.getCustomElements().setValueLocal("status", "neaktiven");
								rowVrtec.update();
							}
							updateVrstica(i + 1);
						}
						else {
							// preveri �e ima datum zadnje pregledano manj�i kot 14 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 14) { 
								// zbri�e vrtec iz tabele
								rowVrtec.delete();
								i = i - 1;
								
								listFeedUrlVrtci = tabelaVrtci.getListFeedUrl();
					            listFeedVrtci = service.getFeed(listFeedUrlVrtci, ListFeed.class);
					        }	
							else {
								updateVrstica(i + 1);
							}
						}
	            	}
	            }
	            
	            updateTipPodatkov("Osnovne �ole");
				updateVrstica(0);
            }
            
            
            if (zadnjiTipPodatkov.equals("Osnovne �ole")) {
            	URL listFeedUrlOsnovneSole = tabelaOsnovneSole.getListFeedUrl();
	            ListFeed listFeedOsnovneSole = service.getFeed(listFeedUrlOsnovneSole, ListFeed.class);

	            for (int i = 0; i < listFeedOsnovneSole.getEntries().size(); i++) {
	            	if (i == Integer.parseInt(zadnjaVrstica)) {
	            		ListEntry rowOsnovnaSola = listFeedOsnovneSole.getEntries().get(i);
	            		String zadnjicPregledano = rowOsnovnaSola.getCustomElements().getValue("zadnjicpregledano");
						if (rowOsnovnaSola.getCustomElements().getValue("status").equals("aktiven")) {
							// preveri �e ima datum zadnje pregledano manj�i kot 7 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 7) { 
								// da status na neaktiven
								rowOsnovnaSola.getCustomElements().setValueLocal("status", "neaktiven");
								rowOsnovnaSola.update();
							}
							updateVrstica(i + 1);
						}
						else {
							// preveri �e ima datum zadnje pregledano manj�i kot 14 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 14) { 
								// zbri�e osnovno �olo iz tabele
								rowOsnovnaSola.delete();
								i = i - 1;
								
								listFeedUrlOsnovneSole = tabelaOsnovneSole.getListFeedUrl();
					            listFeedOsnovneSole = service.getFeed(listFeedUrlOsnovneSole, ListFeed.class);
							}
							else {
								updateVrstica(i + 1);
							}
						}
	            	}
	            }
	            
	            updateTipPodatkov("Srednje �ole");
				updateVrstica(0);
            }
	        
            
            if (zadnjiTipPodatkov.equals("Srednje �ole")) {
            	URL listFeedUrlSrednjeSole = tabelaSrednjeSole.getListFeedUrl();
	            ListFeed listFeedSrednjeSole = service.getFeed(listFeedUrlSrednjeSole, ListFeed.class);

	            for (int i = 0; i < listFeedSrednjeSole.getEntries().size(); i++) {
	            	if (i == Integer.parseInt(zadnjaVrstica)) {
	            		ListEntry rowSrednjaSola = listFeedSrednjeSole.getEntries().get(i);
		            	String zadnjicPregledano = rowSrednjaSola.getCustomElements().getValue("zadnjicpregledano");
						if (rowSrednjaSola.getCustomElements().getValue("status").equals("aktiven")) {
							// preveri �e ima datum zadnje pregledano manj�i kot 7 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 7) { 
								// da status na neaktiven
								rowSrednjaSola.getCustomElements().setValueLocal("status", "neaktiven");
								rowSrednjaSola.update();
							}
							updateVrstica(i + 1);
						}
						else {
							// preveri �e ima datum zadnje pregledano manj�i kot 14 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 14) { 
								// zbri�e srednjo �olo iz tabele
								rowSrednjaSola.delete();
								i = i - 1;
								
								listFeedUrlSrednjeSole = tabelaSrednjeSole.getListFeedUrl();
					            listFeedSrednjeSole = service.getFeed(listFeedUrlSrednjeSole, ListFeed.class);
					        }
							else {
								updateVrstica(i + 1);
							}
						}
	            	}
	            }
            	
            	updateTipPodatkov("Vi�je strokovne �ole");
				updateVrstica(0);
            }
	        
            
            if (zadnjiTipPodatkov.equals("Vi�je strokovne �ole")) {
            	URL listFeedUrlVisjeSole = tabelaVisjeStrokovneSole.getListFeedUrl();
	            ListFeed listFeedVisjeSole = service.getFeed(listFeedUrlVisjeSole, ListFeed.class);
	            
	            for (int i = 0; i < listFeedVisjeSole.getEntries().size(); i++) {
	            	if (i == Integer.parseInt(zadnjaVrstica)) {
	            		ListEntry rowVisjaSola = listFeedVisjeSole.getEntries().get(i);
	            		String zadnjicPregledano = rowVisjaSola.getCustomElements().getValue("zadnjicpregledano");
						if (rowVisjaSola.getCustomElements().getValue("status").equals("aktiven")) {
							// preveri �e ima datum zadnje pregledano manj�i kot 7 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 3) { 
								// da status na neaktiven
								rowVisjaSola.getCustomElements().setValueLocal("status", "neaktiven");
								rowVisjaSola.update();
							}
							updateVrstica(i + 1);
						}
						else {
							// preveri �e ima datum zadnje pregledano manj�i kot 14 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 6) { 
								// zbri�e vi�jo strokovno �olo iz tabele
								rowVisjaSola.delete();
								i = i - 1;
								
								listFeedUrlVisjeSole = tabelaVisjeStrokovneSole.getListFeedUrl();
					            listFeedVisjeSole = service.getFeed(listFeedUrlVisjeSole, ListFeed.class);
							}
							else {
								updateVrstica(i + 1);
							}
						}
	            	}
	            }
	            
	            updateTipPodatkov("Zdravstvo");
				updateVrstica(0);
            }
	        
            if (zadnjiTipPodatkov.equals("Zdravstvo")) {
            	URL listFeedUrlZdravstvo = tabelaZdravstvo.getListFeedUrl();
	            ListFeed listFeedZdravstvo = service.getFeed(listFeedUrlZdravstvo, ListFeed.class);

            	for (int i = 0; i < listFeedZdravstvo.getEntries().size(); i++) {
            		if (i == Integer.parseInt(zadnjaVrstica)) {
            			ListEntry rowZdravstvo = listFeedZdravstvo.getEntries().get(i);
            			String zadnjicPregledano = rowZdravstvo.getCustomElements().getValue("zadnjicpregledano");
						if (rowZdravstvo.getCustomElements().getValue("status").equals("aktiven")) {
							// preveri �e ima datum zadnje pregledano manj�i kot 7 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 7) { 
								// da status na neaktiven
								rowZdravstvo.getCustomElements().setValueLocal("status", "neaktiven");
								rowZdravstvo.update();
							}
							updateVrstica(i + 1);
						}
						else {
							// preveri �e ima datum zadnje pregledano manj�i kot 14 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 14) { 
								// zbri�e zdravstveno ustanovo iz tabele
								rowZdravstvo.delete();
								i = i - 1;
								
								listFeedUrlZdravstvo = tabelaZdravstvo.getListFeedUrl();
					            listFeedZdravstvo = service.getFeed(listFeedUrlZdravstvo, ListFeed.class);
							}	
							else {
								updateVrstica(i + 1);
							}
						}
            		}			
            	}
            	
	        	updateTipPodatkov("Lekarne");
				updateVrstica(0);
            }
	        
	       
            if (zadnjiTipPodatkov.equals("Lekarne")) {
            	URL listFeedUrlLekarne = tabelaLekarne.getListFeedUrl();
	            ListFeed listFeedLekarne = service.getFeed(listFeedUrlLekarne, ListFeed.class);
	            
	            for (int i = 0; i < listFeedLekarne.getEntries().size(); i++) {
	            	if (i == Integer.parseInt(zadnjaVrstica)) {
	            		ListEntry rowLekarna = listFeedLekarne.getEntries().get(i);
	            		String zadnjicPregledano = rowLekarna.getCustomElements().getValue("zadnjicpregledano");
						if (rowLekarna.getCustomElements().getValue("status").equals("aktiven")) {
							// preveri �e ima datum zadnje pregledano manj�i kot 7 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 7) { 
								// da status na neaktiven
								rowLekarna.getCustomElements().setValueLocal("status", "neaktiven");
								rowLekarna.update();
							}
							updateVrstica(i + 1);
						}
						else {
							// preveri �e ima datum zadnje pregledano manj�i kot 14 dni nazaj
							int razlika = getSteviloDni(formatDatuma, danasnjiDatum, zadnjicPregledano);
							if (razlika > 14) { 
								// zbri�e kraj iz tabele
								rowLekarna.delete();
								i = i - 1;
								
								listFeedUrlLekarne = tabelaLekarne.getListFeedUrl();
					            listFeedLekarne = service.getFeed(listFeedUrlLekarne, ListFeed.class);
							}	
							else {
								updateVrstica(i + 1);
							}
						}
	            	}
	            }

	            updateTipPodatkov("Nepremi�nine");
	            updateVrstica(1);
            }
            
	        
	        
		} catch (AuthenticationException e) {
			e.printStackTrace();
			return;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		} catch (ServiceException e) {
			e.printStackTrace();
			return;
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			return;
		}	
        
		
	}
	
	private static void updateTipPodatkov(String tipPodatkov) {
		zadnjiTipPodatkov = tipPodatkov;
		URL listFeedUrlZapisiTipPodatkov = tabelaZapisi.getListFeedUrl();
        ListFeed listFeedZapisiTipPodatkov;
		try {
			listFeedZapisiTipPodatkov = service.getFeed(listFeedUrlZapisiTipPodatkov, ListFeed.class);
			rowZapisi = listFeedZapisiTipPodatkov.getEntries().get(0);
			rowZapisi.getCustomElements().setValueLocal("tippodatkov", zadnjiTipPodatkov);
			rowZapisi.update();
		} catch (IOException e) {
			return;
		} catch (ServiceException e) {
			return;
		}
        
	}
	
	private static void updateVrstica(int stevec) {
		zadnjaVrstica = Integer.toString(stevec);
		URL listFeedUrlZapisiVrstica = tabelaZapisi.getListFeedUrl();
        ListFeed listFeedZapisiVrstica;
		try {
			listFeedZapisiVrstica = service.getFeed(listFeedUrlZapisiVrstica, ListFeed.class);
			rowZapisi = listFeedZapisiVrstica.getEntries().get(0);
			rowZapisi.getCustomElements().setValueLocal("vrstica", zadnjaVrstica);
			rowZapisi.update();
		} catch (IOException e) {
			return;
		} catch (ServiceException e) {
			return;
		}
        
	}
	
	private static int getSteviloDni(SimpleDateFormat formatDatuma, String datum1, String datum2) {
		int razlikaDnevi = 0;
		try {
			Date d1 = formatDatuma.parse(datum1);
			Date d2 = formatDatuma.parse(datum2);
			
			long razlikaMilisekunde = d2.getTime() - d1.getTime();
			razlikaDnevi = (int)razlikaMilisekunde / (24 * 60 * 60 * 1000);
			
		} catch (ParseException e) {
			return 0;
		}
		
		return Math.abs(razlikaDnevi);
	}

}
