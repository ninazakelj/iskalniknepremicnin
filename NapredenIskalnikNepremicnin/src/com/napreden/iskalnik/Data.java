package com.napreden.iskalnik;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.ServiceException;
import com.napreden.iskalnik.entity.Kraj;

public class Data {
	static SpreadsheetService service = new SpreadsheetService("com.napreden.iskalnik");
	
	public static List<Kraj> getKraji() {
		List<Kraj> kraji = new ArrayList<Kraj>();
		URL url;
		WorksheetFeed feed;
		ListFeed listFeed = null;
		try {
			int timeout = 60000;
			service.setConnectTimeout(timeout);
			// url google spreadsheet-a
			url = new URL("https://spreadsheets.google.com/feeds/worksheets/1JFsl4M76xcjXWaMPnox8oW3dzdB10_6T_4nO2vcVqTE/public/values");			
			feed = service.getFeed(url, WorksheetFeed.class);
			// seznam woorksheet-ov
			List<WorksheetEntry> worksheets = feed.getEntries();
			WorksheetEntry worksheetKraji = null;
			// woorksheet z kraji
			for (WorksheetEntry worksheet : worksheets) {
				if (worksheet.getTitle().getPlainText().equals("Kraji")) {
					worksheetKraji = worksheet;
				}
			}
			// Fetch the list feed of the worksheet.
		    URL listFeedUrl = worksheetKraji.getListFeedUrl();
		    listFeed = service.getFeed(listFeedUrl, ListFeed.class);			
		} 
		catch (MalformedURLException e) {			
			e.printStackTrace();
		} 
		catch (IOException e) {			
			e.printStackTrace();
		} 
		catch (ServiceException e) {			
			e.printStackTrace();
		}	
		
	    // gre �ez vrstice v worksheet-u
	    for (ListEntry row : listFeed.getEntries()) {
	    	String naziv = "";
	    	double lat = 0;
	    	double lng = 0;
	    	int i = 0;
	    	for (String tag : row.getCustomElements().getTags()) {
	    		switch (i) {
	    			case 0: naziv = row.getCustomElements().getValue(tag) ;
	    				break;
	    			case 3: lat = Double.parseDouble(row.getCustomElements().getValue(tag));
	    				break;
	    			case 4: lng = Double.parseDouble(row.getCustomElements().getValue(tag));
	    				break;	    		
	    		}
	    		i++;
		    }
	    	Kraj k = new Kraj(naziv, lat, lng);
	    	kraji.add(k);
	    }
		
		return kraji;		
	}

}
