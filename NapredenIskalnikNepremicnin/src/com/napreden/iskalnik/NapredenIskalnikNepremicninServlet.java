package com.napreden.iskalnik;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.gdata.client.spreadsheet.ListQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.ListEntry;
import com.google.gdata.data.spreadsheet.ListFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

@SuppressWarnings("serial")
public class NapredenIskalnikNepremicninServlet extends HttpServlet {
	private static String CLIENT_ID = "711446868423-nfjmhs7ftcjb2okck27o8t18765ofa3v.apps.googleusercontent.com";
	private static String CLIENT_SECRET = "BDq7BF5r51yhHJLUp8bHoRFe";	
	private static final String SERVICE_ACCOUNT_EMAIL = "711446868423-nfjmhs7ftcjb2okck27o8t18765ofa3v@developer.gserviceaccount.com";
	private static final String SERVICE_ACCOUNT_PKCS12_FILE_PATH = "WEB-INF/key.p12";
	
	private static SpreadsheetService service;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("action").equals("dodajPriljubljene")) {
			// email uporabnika
			String uporabnik = request.getParameter("imeUporabnika");
			// id priljubljene nepremi�nine
			String idNepremicnine = request.getParameter("idPriljubljene");
			// pasovna �irina in dol�ina
			String pasovnaSirina = request.getParameter("latitude");
			String pasovnaDolzina = request.getParameter("longitude");
						
			WorksheetEntry tabelaPriljubljene = getWorksheet();
			// doda nepremicnino med priljubljene 
			URL listFeedUrlPriljubljene = tabelaPriljubljene.getListFeedUrl();
	        ListEntry rowPriljubljena = new ListEntry();
	        rowPriljubljena.getCustomElements().setValueLocal("uporabnik", uporabnik);
	        rowPriljubljena.getCustomElements().setValueLocal("idnepremicnine", idNepremicnine);
	        rowPriljubljena.getCustomElements().setValueLocal("latitude", pasovnaSirina);
	        rowPriljubljena.getCustomElements().setValueLocal("longitude", pasovnaDolzina);
	        try {
				service.insert(listFeedUrlPriljubljene, rowPriljubljena);
			} catch (ServiceException e) {
				e.printStackTrace();
				return;
			}    
		}	
		else if (request.getParameter("action").equals("odstraniPriljubljene")) {
			// email uporabnika
			String uporabnik = request.getParameter("imeUporabnika");
			// id priljubljene nepremi�nine
			String idNepremicnine = request.getParameter("idPriljubljene");
			
			WorksheetEntry tabelaPriljubljene = getWorksheet();
			try {
				URL listFeedUrlPriljubljene = tabelaPriljubljene.getListFeedUrl();
				ListQuery query = new ListQuery(listFeedUrlPriljubljene);
				query.setSpreadsheetQuery("idnepremicnine=" + idNepremicnine);
				ListFeed listFeedPriljubljene = service.query(query, ListFeed.class);
				if (listFeedPriljubljene.getEntries().size() > 0) {
					for (ListEntry row : listFeedPriljubljene.getEntries()) {
						if (row.getTitle().getPlainText().equals(uporabnik)) {
							// odstrani nepremi�nino iz priljubljenih 
							row.delete();
							break;
						}
					}
				}
				
			} catch (ServiceException e) {
				e.printStackTrace();
				return;
			}
		}
		
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		doGet(request, response);
		
	}
	
	private static WorksheetEntry getWorksheet() {
		List<String> scopes = new ArrayList<String>();
		scopes.add("https://spreadsheets.google.com/feeds");
	
		HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
		JsonFactory JSON_FACTORY = new JacksonFactory();
		
		WorksheetEntry tabelaPriljubljene = null;
		try {
			// omogo�i dostop do spreadsheet-a		
			GoogleCredential credential = new GoogleCredential.Builder()
			.setTransport(HTTP_TRANSPORT)
			.setJsonFactory(JSON_FACTORY)
			.setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
			.setServiceAccountScopes(scopes)
			.setServiceAccountPrivateKeyFromP12File(new File(SERVICE_ACCOUNT_PKCS12_FILE_PATH))
			.build();
			
			service = new SpreadsheetService("com.napreden.iskalnik");
	        service.setOAuth2Credentials(credential);
			// dolo�i timeout za service
	        service.setConnectTimeout(300000);
	        service.setReadTimeout(300000);
	        URL spreadsheetUrl = new URL("https://spreadsheets.google.com/feeds/spreadsheets/private/full");  
			
	        SpreadsheetEntry sheetPriljubljene = null;
	        // pridobi vse spreadsheet-e
	        SpreadsheetFeed feed = service.getFeed(spreadsheetUrl, SpreadsheetFeed.class);
	        List<SpreadsheetEntry> spreadsheets = feed.getEntries();
			        
	        // gre �ez vse spreadsheet-e in si zapomni tistega, ki se uporablja za nepremicnine
	        for (SpreadsheetEntry spreadsheet : spreadsheets) {
	        	if (spreadsheet.getTitle().getPlainText().equals("Priljubljene_nepremicnine")) {
	        		sheetPriljubljene = spreadsheet;
	        	}  
	        }
	        
	        // pridobi worksheet za podatke o priljubljenih
	        List<WorksheetEntry> worksheetsNepremicnine = sheetPriljubljene.getWorksheets();   
	        for (WorksheetEntry worksheet : worksheetsNepremicnine) {
	        	if (worksheet.getTitle().getPlainText().equals("Priljubljene")) {
	        		tabelaPriljubljene = worksheet;	
	        	}
	        }
	        
	        return tabelaPriljubljene;
	        
		} catch (AuthenticationException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (ServiceException e) {
			e.printStackTrace();
			return null;
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
}
