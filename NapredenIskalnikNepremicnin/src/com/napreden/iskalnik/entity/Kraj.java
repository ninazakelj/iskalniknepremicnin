package com.napreden.iskalnik.entity;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Kraj {
	@PrimaryKey
	@Persistent private String naziv;
	@Persistent private double latitude;
	@Persistent private double longitude;
	
	public Kraj(String naziv, double latitude, double longitude) {
		this.naziv = naziv;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	/**
	* @return the naziv
	*/
	public String getNaziv() {
		return naziv;
	}
	/**
	* @param naziv the naziv to set
	*/
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	/**
	* @return the latitude
	*/
	public double getLatitude() {
		return latitude;
	}
	/**
	* @param latitude the latitude to set
	*/
	public void setNaslov(double latitude) {
		this.latitude = latitude;
	}
	/**
	* @return the longitude
	*/
	public double getLongitude() {
		return longitude;
	}
	/**
	* @param longitude the longitude to set
	*/
	public void setId(double longitude) {
		this.longitude = longitude;
	}
	
}

